package com.craterzone.ca.common.util;

public interface FilePathImpl {

	// For windows
	/*String TEMP_UPLOAD_FOLDER_PATH = "D:\\cawork\\uploaded\\";
	String UPLOAD_FOLDER_PATH = "D:\\CA\\uploaded\\";
	String UPLOAD_FOLDER_DB_PATH = "D:/CA/uploaded/";
	String XLS_FILE_PATH = "D:\\CA\\excel\\";
	String DOWNLOAD_FOLDER_PATH = "D:\\CA\\excel";
	String PROJECT_FOLDER_PATH = "D:\\CA";
	String ALL_ZIPS_FOLDER_PATH = "D:\\CA\\allzips";*/
	
	String SEPERATOR = "/";
	
	// For linux
	String TEMP_UPLOAD_FOLDER_PATH = "/var/cawork/uploaded/";
	String UPLOAD_FOLDER_PATH = "/var/CA/uploaded/";
	String UPLOAD_FOLDER_DB_PATH = "/var/CA/uploaded/";
	String XLS_FILE_PATH = "/var/CA/excel/";
	String DOWNLOAD_FOLDER_PATH = "/var/CA/excel";
	String PROJECT_FOLDER_PATH = "/var/CA";
	String ALL_ZIPS_FOLDER_PATH = "/var/CA/allzips";
	
	
}
