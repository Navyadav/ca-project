package com.craterzone.ca.common.util;

import com.craterzone.ca.model.RegisterationBean;

public class CustomValidation {

	public static String getValidateFirstForm(RegisterationBean brd) {

		if (brd.getMobileNumber() > 10000000000l
				&& brd.getMobileNumber() < 1000000000l) {
			return "Mobile number is invalid";
		}
		if (!RegularExpressionValidate.isEmailValid(brd.getEmail())) {
			return "Email address is invalid";
		}
		if (brd.getPinCode() < 99999 && brd.getPinCode() > 1000000) {
			return "Pincode is invalid";
		}
		if (brd.getAccountNumber() < 100000000000000l
				&& brd.getAccountNumber() > 10000000000000000l) {
			return "Account number is invalid";
		}
		if (brd.getPanNumber().length < 10) {
			return "Pan number should be of 10 characters long";
		}
/*		if (brd.getPanNumber().length() != 10) {
			return "Pan number must be of 10 characters";
		}
		if (!((brd.getPanNumber().trim().toUpperCase().charAt(3))=='P')) {
			return "Pan number is invalid";
		}*/
		/*if(!RegularExpressionValidate.isNumericValid(""+brd.getBankInterest())) {
			return "Bank Interest must be numeric";
		} else if(brd.getBankInterest() > 100.0) {
			return "Maximum bank interest is 100";
		}*/
		return "";
	}

}
