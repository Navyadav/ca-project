package com.craterzone.ca.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class FileConverterUtil {
	
	private static List<String> _filesListInDir = new ArrayList<String>();

	final static Logger _logger = Logger.getLogger(FileConverterUtil.class.getName());
	
	/**
	 * Zip the single file at location zipFileName(including name of the file ending with '.zip')
	 * 
	 * @param file
	 * @param zipFileName
	 */
	
	public static void zipSingleFile(File file, String zipFileName) {
        try {
            //create ZipOutputStream to write to the zip file
            FileOutputStream fos = new FileOutputStream(zipFileName);
            ZipOutputStream zos = new ZipOutputStream(fos);
            //add a new Zip Entry to the ZipOutputStream
            ZipEntry ze = new ZipEntry(file.getName());
            zos.putNextEntry(ze);
            //read the file and write to ZipOutputStream
            FileInputStream fis = new FileInputStream(file);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = fis.read(buffer)) > 0) {
                zos.write(buffer, 0, len);
            }
             
            //Close the zip entry to write to zip file
            zos.closeEntry();
            //Close resources
            zos.close();
            fis.close();
            fos.close();
            _logger.log(Level.INFO, file.getCanonicalPath()+" is zipped to "+zipFileName);
             
        } catch (IOException e) {
        	_logger.log(Level.SEVERE, "Unable to zipSingleFile in FileConverterUtil" + e);
        }
    }
	
	 /**
     * This method zips the directory
     * @param dir
     * @param zipDirName
     */
    public static void zipDirectory(File dir, String zipDirName) {
        try {
        	_filesListInDir.clear();
        	populateFilesList(dir);
            //now zip files one by one
            //create ZipOutputStream to write to the zip file
            FileOutputStream fos = new FileOutputStream(zipDirName+".zip");
            ZipOutputStream zos = new ZipOutputStream(fos);
            for(String filePath : _filesListInDir) {
            	_logger.log(Level.INFO, "Zipping "+filePath);
                
                //for ZipEntry we need to keep only relative file path, so we used substring on absolute path
                ZipEntry ze = new ZipEntry(filePath.substring(dir.getAbsolutePath().length()+1, filePath.length()));
                zos.putNextEntry(ze);
                
                //read the file and write to ZipOutputStream
                FileInputStream fis = new FileInputStream(filePath);
                byte[] buffer = new byte[1024];
                int len;
                while ((len = fis.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }
                zos.closeEntry();
                fis.close();
            }
            zos.close();
            fos.close();
        } catch (IOException e) {
        	_logger.log(Level.SEVERE, "Unable to zip directory in FileConverterUtil" + e);
        }
    }
    
    /**
     * Demo code for zipping file,
     * Excel file data is hard coded in it.
     */
	
	public static void convertToXls() {
		
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("Sample sheet");
		 
		Map<String, Object[]> data = new HashMap<String, Object[]>();
		data.put("1", new Object[] {"Emp No.", "Name", "Salary"});
		data.put("2", new Object[] {1d, "John", 1500000d});
		data.put("3", new Object[] {2d, "Sam", 800000d});
		data.put("4", new Object[] {3d, "Dean", 700000d});
		 
		Set<String> keyset = data.keySet();
		int rownum = 0;
		for (String key : keyset) {
		    Row row = sheet.createRow(rownum++);
		    Object [] objArr = data.get(key);
		    int cellnum = 0;
		    for (Object obj : objArr) {
		        Cell cell = row.createCell(cellnum++);
		        if(obj instanceof Date)
		            cell.setCellValue((Date)obj);
		        else if(obj instanceof Boolean)
		            cell.setCellValue((Boolean)obj);
		        else if(obj instanceof String)
		            cell.setCellValue(""+obj);
		        else if(obj instanceof Double)
		            cell.setCellValue((Double)obj);
		    }
		}
		 
		try {
			
			// Path can be changed
		    FileOutputStream out = new FileOutputStream(new File("C:\\new.xls"));
		    workbook.write(out);
		    out.close();
		    _logger.log(Level.INFO, "Excel written successfully..");
		     
		} catch (FileNotFoundException e) {
			_logger.log(Level.SEVERE, "Unable to convert to .xls " + e);
		} catch (IOException e) {
			_logger.log(Level.SEVERE, "Unable to convert to .xls" + e);
		}
	}
	
	
	 /**
     * This method populates all the files in a directory to a List
     * @param dir
     * @throws IOException
     */
    private static void populateFilesList(File dir) throws IOException {
        File[] files = dir.listFiles();
        for(File file : files) {
            if(file.isFile()) _filesListInDir.add(file.getAbsolutePath());
            else populateFilesList(file);
        }
    }
    
	/** Generating .xls file from a map data object
	 * 
	 * @param - map of rows
	 * @param - fileName to save with a file name
	 * @param - where to save
	*/
	public static void convertToXls(Map<Integer, Object[]> data, String fileName, String path) {
		
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet(fileName);
		
		Set<Integer> keyset = data.keySet();
		int rownum = 0;
		for (int key : keyset) {
		    Row row = sheet.createRow(rownum++);
		    Object [] objArr = data.get(key);
		    int cellnum = 0;
		    for (Object obj : objArr) {
		        Cell cell = row.createCell(cellnum++);
		        if(obj instanceof Date) {
		            cell.setCellValue(obj.toString());
		        } else if(obj instanceof String) {
		            cell.setCellValue((String)obj);
		        } else if(obj instanceof Double)
		            cell.setCellValue((Double)obj);
		        else if(obj instanceof Integer)
		            cell.setCellValue((Integer)obj);
		        else if(obj instanceof Long)
		            cell.setCellValue((Long)obj);
		    }
		}
		 
		try {
		    FileOutputStream out = new FileOutputStream(new File(path + fileName +".xls"));
		    workbook.write(out);
		    out.close();
		    _logger.log(Level.INFO, "Excel written successfully..");
		     
		} catch (FileNotFoundException e) {
			_logger.log(Level.SEVERE, "Unable to convert to .xls in FileConverterUtil" + e);
		} catch (IOException e) {
			_logger.log(Level.SEVERE, "Unable to convert to .xls in FileConverterUtil" + e);
		}
	}
    
}
