package com.craterzone.ca.common.util;

public class RegularExpressionValidate {

	private static String numericRegx = "^\\d+$";
	private static String emailRegx = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private static String alphaNumericRegx = "^[a-zA-Z0-9]*$";
	private static String alphaCharacterRegx = "([a-zA-Z]+\\s+)*[a-zA-Z]+";

	// private String fileExtentionRegx="([^\\s]+(\\.(?i)(jpg|png|gif|bmp))$)";

	public static boolean isEmailValid(String email) {

		if (email.matches(emailRegx)) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isAlphaNumericValid(String alphaNumeric) {
		if (alphaNumeric.matches(alphaNumericRegx)) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isAlphabeatsValid(String alphaBeats) {
		if (alphaBeats.matches(alphaCharacterRegx)) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isNumericValid(String numeric) {
		if (numeric.matches(numericRegx)) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isNull(String value) {
		if (value.equals("") || value.length()<=0) {
			return true;
		} else {
			return false;
		}
	}
}
