package com.craterzone.ca.common.util;

import java.io.File;

public class FileHandler implements FilePathImpl {

	/**
	 * Move Folder and its contents to pat (newName) from path(oldName) with
	 * folder name (folderName)
	 * 
	 * @param oldName
	 * @param newName
	 * @param folderName
	 * @return
	 */
	
	public static boolean move(String oldName, String newName, String folderName) {
		File file = new File(oldName);
		if (file.renameTo(new File(newName + folderName))) {
			return true;
		}
		return false;
	}

	/**
	 * Delete the folder recursively
	 * 
	 * @param file
	 * @param path
	 */
	
	public static void deleteRestricted(File file, String path) {
		String[] list = file.list();
		for (String name : list) {
			File f = new File(path +SEPERATOR+ name);
			if (f.isDirectory()) {
				deleteRestricted(f, path + name + SEPERATOR);
				f.delete();
			} else {

				f.delete();
			}
		}
	}
}
