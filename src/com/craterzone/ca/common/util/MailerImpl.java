package com.craterzone.ca.common.util;

import java.util.Map;

public interface MailerImpl {
	
	public final int _DEFAULT_MAILER = 1;
	public final int _FEEDBACK_MAIL = 2;
	public final int _EXCEL_MAIL = 3;
	public final int _FORGOT_PASSWORD_MAIL = 4;
	public final int _NOTIFICATION_MAIL = 5;
	public final int _ACCOUNT_VERIFICATION_MAIL = 6;
	public final int _ACCOUNT_VERIFIED_MAIL = 7;
	public final int _DEACTIVATE_ACCOUNT_MAIL = 8;
	public final int _UPDATE_ACCOUNT_MAIL = 9;
	public final int _PASSWORD_CHANGE_MAIL = 10;
	public final int _COST_BREAK_UP_SHEET_MAIL = 11;
	public final int _COST_BREAK_UP_SHEET_WITH_MP_MAIL = 12;
	public final int _INSTANCE_COSTING_WITH_MP_MAIL = 13;
	public final int _INSTANCE_COSTING_MAIL = 14;
	public final int _SIGN_IN_PROBLEM_MAIL = 15;
	public final int _CONTACT_US_NATURAL_BEACH_MAIL = 16;
	public final int _ROOM_RESERVATION_NATURALBEACH_MAIL = 17;

	public final String _CONTENT_TYPE_TEXT_HTML = "text/html";
	public final String ccAdmin = "admin.craterzone@gmail.com";
	
	public int getMailType();
	public String getSubject();
	public void setSubject(String subject);
	public String getReceiver();
	public void setReceiver(String receiver);
	public String getContent();
	public String getContentType();
	public String getTemplateName();
	public Map<String ,String> createDataForTemplate();
	public boolean isContainAttachment();
	public byte[] getAttachment();
	public void setAttachment(byte[] attachement);
	public void setAttachmentName(String attachmentName);
	public String getAttachmentName();
	public void setContent(String content);
	public void setCC(String[] cc);
	public String[] getCC();
	public void setContentType(String contentType);
}
