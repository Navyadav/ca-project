package com.craterzone.ca.common.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionHelper {

	final static Logger _logger = Logger.getLogger(ConnectionHelper.class
			.getName());

	public static void close(Connection connection) {

		try {

			if (connection != null) {

				connection.close();

			}

		} catch (SQLException e) {

			_logger.log(Level.SEVERE, "Unable to Close Connection " + e);

		}

	}

	public static void close(Statement statement) {

		try {

			if (statement != null) {

				statement.close();

			}

		} catch (SQLException e) {

			_logger.log(Level.SEVERE, "Unable to Close Statement " + e);

		}

	}

	public static void close(ResultSet resultSet) {

		try {

			if (resultSet != null) {

				resultSet.close();

			}

		} catch (SQLException e) {

			_logger.log(Level.SEVERE, "Unable to Close Result Set " + e);

		}

	}

	public static void close(PreparedStatement preparedStatement) {
		try {
			if (preparedStatement != null) {
				preparedStatement.close();
			}

		} catch (SQLException e) {

			_logger.log(Level.SEVERE, "Unable to Close preparedStatement " + e);

		}

	}
	public static void rollback(Connection con){
		try {
			if(con != null){
				con.rollback();
			}
		} catch (SQLException e) {
			_logger.log(Level.SEVERE, "Unable to rollback Connection " + e);
		}
	}
}
