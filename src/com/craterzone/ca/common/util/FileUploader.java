package com.craterzone.ca.common.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.springframework.web.multipart.MultipartFile;

public class FileUploader implements FilePathImpl {
	
	// Upload file
	public static void uploadFile(MultipartFile file, String userName) throws IOException {

		// Getting file in inputStream
		InputStream inputStream = null;
		OutputStream outputStream = null;
		
		// Getting original fileName
		String fileName = file.getOriginalFilename();
		
		// Making Directory of user
		File newDirectory = new File(TEMP_UPLOAD_FOLDER_PATH + userName);
		newDirectory.mkdirs();
		
		// Making a new file in specified path
		File newFile = null;
		inputStream = file.getInputStream();
		newFile = new File(TEMP_UPLOAD_FOLDER_PATH + userName + SEPERATOR + fileName);
		
		// if file doesn't exists, making a new file
		if (!newFile.exists()) {
			newFile.createNewFile();
		}
		
		// writing date to file
		outputStream = new FileOutputStream(newFile);
		int read = 0;
		byte[] bytes = new byte[1024];

		while ((read = inputStream.read(bytes)) != -1) {
			outputStream.write(bytes, 0, read);
		}
		
		outputStream.close();

	}

}
