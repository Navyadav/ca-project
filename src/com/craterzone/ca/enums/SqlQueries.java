package com.craterzone.ca.enums;

public class SqlQueries {

	public static StringBuilder userExist() {
		StringBuilder query = new StringBuilder();
		query.append("SELECT count(*) as id from "
				+ Tables.REGISTER_USER.getLabel());
		query.append(" Where " + Tables.RegisterUser.COLUMN_emailId.getLabel()
				+ "='");
		return query;
	}

	public static StringBuilder staffExist() {
		StringBuilder query = new StringBuilder();
		query.append("SELECT count(*) as id from "
				+ Tables.USER_LOGIN.getLabel());
		query.append(" Where " + Tables.UserLogin.COLUMN_userName.getLabel()
				+ "='");
		return query;
	}

	public static StringBuilder addStaff() {
		StringBuilder query = new StringBuilder();
		query.append("Insert into " + Tables.USER_LOGIN.getLabel() + " (");
		query.append(Tables.UserLogin.COLUMN_id.getLabel() + ",");
		query.append(Tables.UserLogin.COLUMN_userName.getLabel() + ",");
		query.append(Tables.UserLogin.COLUMN_password.getLabel() + ",");
		query.append(Tables.UserLogin.COLUMN_userType.getLabel());
		query.append(") values(?,?,?,?)");
		return query;
	}

	public static StringBuilder getInsertUserQuery() {
		StringBuilder query = new StringBuilder();
		query.append("Insert into " + Tables.REGISTER_USER.getLabel() + " (");
		query.append(Tables.RegisterUser.COLUMN_registerUserId.getLabel() + ",");
		query.append(Tables.RegisterUser.COLUMN_firstName.getLabel() + ",");
		query.append(Tables.RegisterUser.COLUMN_middleName.getLabel() + ",");
		query.append(Tables.RegisterUser.COLUMN_lastName.getLabel() + ",");
		query.append(Tables.RegisterUser.COLUMN_fatherFirstName.getLabel()
				+ ",");
		query.append(Tables.RegisterUser.COLUMN_father_middle_name.getLabel()
				+ ",");
		query.append(Tables.RegisterUser.COLUMN_father_last_name.getLabel()
				+ ",");
		query.append(Tables.RegisterUser.COLUMN_dob.getLabel() + ",");
		query.append(Tables.RegisterUser.COLUMN_pan.getLabel() + ",");
		query.append(Tables.RegisterUser.COLUMN_gender.getLabel() + ",");
		query.append(Tables.RegisterUser.COLUMN_mobileNumber.getLabel() + ",");
		query.append(Tables.RegisterUser.COLUMN_emailId.getLabel() + ",");

		query.append(Tables.RegisterUser.COLUMN_addressLine1.getLabel() + ",");
		query.append(Tables.RegisterUser.COLUMN_addressLine2.getLabel() + ",");
		query.append(Tables.RegisterUser.COLUMN_city.getLabel() + ",");
		query.append(Tables.RegisterUser.COLUMN_state.getLabel() + ",");
		query.append(Tables.RegisterUser.COLUMN_pin.getLabel() + ",");
		query.append(Tables.RegisterUser.COLUMN_accountNumber.getLabel() + ",");
		query.append(Tables.RegisterUser.COLUMN_ifscCode.getLabel() + ",");
		query.append(Tables.RegisterUser.COLUMN_bankName.getLabel() + ",");
		query.append(Tables.RegisterUser.COLUMN_bankAddress.getLabel() + ",");
		query.append(Tables.RegisterUser.COLUMN_assessmentYear.getLabel() + ",");
		query.append(Tables.RegisterUser.COLUMN_bankInterest.getLabel() + ",");
		query.append(Tables.RegisterUser.COLUMN_remark.getLabel() + ",");
		query.append(Tables.RegisterUser.COLUMN_dateTime.getLabel());

		query.append(") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		return query;
	}

	public static StringBuilder getInsertFileQuery() {

		StringBuilder query = new StringBuilder();
		query.append("Insert into " + Tables.USER_FILE.getLabel() + " (");
		query.append(Tables.UserFile.COLUMN_registerUserId.getLabel() + ",");

		query.append(Tables.UserFile.COLUMN_form16_first_partA.getLabel() + ",");
		query.append(Tables.UserFile.COLUMN_form16_first_partA_password
				.getLabel() + ",");
		query.append(Tables.UserFile.COLUMN_form16_first_partB.getLabel() + ",");
		query.append(Tables.UserFile.COLUMN_form16_first_partB_password
				.getLabel() + ",");

		query.append(Tables.UserFile.COLUMN_form16_second_partA.getLabel()
				+ ",");
		query.append(Tables.UserFile.COLUMN_form16_second_partA_password
				.getLabel() + ",");
		query.append(Tables.UserFile.COLUMN_form16_second_partB.getLabel()
				+ ",");
		query.append(Tables.UserFile.COLUMN_form16_second_partB_password
				.getLabel() + ",");

		query.append(Tables.UserFile.COLUMN_form16_third_partA.getLabel() + ",");
		query.append(Tables.UserFile.COLUMN_form16_third_partA_password
				.getLabel() + ",");
		query.append(Tables.UserFile.COLUMN_form16_third_partB.getLabel() + ",");
		query.append(Tables.UserFile.COLUMN_form16_third_partB_password
				.getLabel() + ",");

		query.append(Tables.UserFile.COLUMN_form16_fourth_partA.getLabel()
				+ ",");
		query.append(Tables.UserFile.COLUMN_form16_fourth_partA_password
				.getLabel() + ",");
		query.append(Tables.UserFile.COLUMN_form16_fourth_partB.getLabel()
				+ ",");
		query.append(Tables.UserFile.COLUMN_form16_fourth_partB_password
				.getLabel() + ",");

		query.append(Tables.UserFile.COLUMN_form16A_1.getLabel() + ",");
		query.append(Tables.UserFile.COLUMN_form16A_1_password.getLabel() + ",");

		query.append(Tables.UserFile.COLUMN_form16A_2.getLabel() + ",");
		query.append(Tables.UserFile.COLUMN_form16A_2_password.getLabel() + ",");
		query.append(Tables.UserFile.COLUMN_form16A_3.getLabel() + ",");
		query.append(Tables.UserFile.COLUMN_form16A_3_password.getLabel() + ",");
		query.append(Tables.UserFile.COLUMN_form16A_4.getLabel() + ",");
		query.append(Tables.UserFile.COLUMN_form16A_4_password.getLabel() + ",");

		query.append(Tables.UserFile.COLUMN_proof1.getLabel() + ",");
		query.append(Tables.UserFile.COLUMN_proof1_password.getLabel() + ",");
		query.append(Tables.UserFile.COLUMN_proof2.getLabel() + ",");
		query.append(Tables.UserFile.COLUMN_proof2_password.getLabel() + ",");
		query.append(Tables.UserFile.COLUMN_proof3.getLabel() + ",");
		query.append(Tables.UserFile.COLUMN_proof3_password.getLabel() + ",");
		query.append(Tables.UserFile.COLUMN_proof4.getLabel() + ",");
		query.append(Tables.UserFile.COLUMN_proof4_password.getLabel() + ",");
		query.append(Tables.UserFile.COLUMN_income.getLabel());
		query.append(") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

		return query;
	}

	public static StringBuilder getMakingTicketQuery() {
		StringBuilder query = new StringBuilder();

		query.append("Insert into ");
		query.append(Tables.HELP_TICKET.getLabel());
		query.append("(");
		query.append(Tables.HelpTicket.COLUMN_id.getLabel() + ",");
		query.append(Tables.HelpTicket.COLUMN_transactionId.getLabel() + ",");
		query.append(Tables.HelpTicket.COLUMN_dateTime.getLabel() + ",");
		query.append(Tables.HelpTicket.COLUMN_email.getLabel() + ",");
		query.append(Tables.HelpTicket.COLUMN_pan.getLabel() + ",");
		query.append(Tables.HelpTicket.COLUMN_mobile.getLabel() + ",");
		query.append(Tables.HelpTicket.COLUMN_remark.getLabel() + ",");
		query.append(Tables.HelpTicket.COLUMN_status.getLabel());
		query.append(") values(?,?,?,?,?,?,?,?)");
		return query;

	}

	public static StringBuilder getOpenTickets() {
		StringBuilder query = new StringBuilder();

		query.append("Select * from " + Tables.HELP_TICKET.getLabel());
		query.append(" where " + Tables.HelpTicket.COLUMN_status.getLabel()
				+ " ='open'");
		return query;
	}

	public static StringBuilder closeTickets() {

		StringBuilder query = new StringBuilder();
		query.append("Update " + Tables.HELP_TICKET.getLabel() + " Set "
				+ Tables.HelpTicket.COLUMN_status.getLabel() + "=2 WHERE ");
		query.append(Tables.HelpTicket.COLUMN_transactionId.getLabel() + "=");
		return query;

	}

	public static StringBuilder getDiscount() {

		StringBuilder query = new StringBuilder();

		query.append("Select * from " + Tables.DISCOUNT.getLabel());
		query.append(" where " + Tables.Discount.COLUMN_status.getLabel()
				+ " =1");
		return query;

	}

	public static StringBuilder getDeleteDiscount() {

		StringBuilder query = new StringBuilder();

		query.append("Delete from " + Tables.DISCOUNT.getLabel());
		query.append(" where " + Tables.Discount.COLUMN_id.getLabel() + "=");
		return query;

	}

	public static StringBuilder getAddDiscount() {

		StringBuilder query = new StringBuilder();

		query.append("Insert into " + Tables.DISCOUNT.getLabel() + "(");
		query.append(Tables.Discount.COLUMN_id.getLabel() + ",");
		query.append(Tables.Discount.COLUMN_domainName.getLabel() + ",");
		query.append(Tables.Discount.COLUMN_companyName.getLabel() + ",");
		query.append(Tables.Discount.COLUMN_dateTime.getLabel() + ",");
		query.append(Tables.Discount.COLUMN_rate.getLabel() + ",");
		query.append(Tables.Discount.COLUMN_status.getLabel());

		query.append(" ) values(?,?,?,?,?,?) ");
		return query;

	}

	public static StringBuilder updateDiscount() {

		StringBuilder query = new StringBuilder();
		query.append("Update " + Tables.DISCOUNT.getLabel() + " Set "
				+ Tables.Discount.COLUMN_domainName.getLabel() + "=?, ");
		query.append(Tables.Discount.COLUMN_companyName.getLabel() + "=?, ");
		query.append(Tables.Discount.COLUMN_dateTime.getLabel() + "=?, ");
		query.append(Tables.Discount.COLUMN_rate.getLabel() + "=? WHERE ");
		query.append(Tables.Discount.COLUMN_id.getLabel() + "=?");
		return query;

	}

	public static StringBuilder updateRegisteredUserStatus() {
		StringBuilder query = new StringBuilder();
		query.append("Update " + Tables.REGISTER_USER.getLabel());
		query.append(" SET "
				+ Tables.RegisterUser.COLUMN_transactionId.getLabel() + "=?,");
		query.append(Tables.RegisterUser.COLUMN_amount.getLabel() + "=?,");
		query.append(Tables.RegisterUser.COLUMN_status.getLabel() + "=?");
		query.append(" where "
				+ Tables.RegisterUser.COLUMN_registerUserId.getLabel() + "='");
		return query;
	}

}
