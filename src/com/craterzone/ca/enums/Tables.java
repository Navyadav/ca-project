package com.craterzone.ca.enums;

public enum Tables {
	FEEDBACK("feedback"), USER_LOGIN("user_login"), HELP_TICKET("help_ticket"), DISCOUNT(
			"discount"), REGISTER_USER("register_user"), USER_FILE("user_file");

	private String label;

	private Tables(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public enum FeedBack {

		COLUMN_feedbackId("feedback_id"), COLUMN_imgUrl("img_url"), COLUMN_logoUrl(
				"logo_url"), COLUMN_feedBackMessage("feedback_message"), COLUMN_dateTime(
				"datetime"), COLUMN_priority("priority");

		private String label;

		private FeedBack(String label) {
			this.label = label;
		}

		public String getLabel() {
			return label;
		}

	}

	public enum UserLogin {

		COLUMN_id("id"), COLUMN_userName("user_name"), COLUMN_password(
				"password"), COLUMN_userType("user_type");

		private String label;

		private UserLogin(String label) {
			this.label = label;
		}

		public String getLabel() {
			return label;
		}

	}

	public enum HelpTicket {

		COLUMN_id("id"), COLUMN_transactionId("transaction_id"), COLUMN_dateTime(
				"datetime"), COLUMN_pan("pan"), COLUMN_mobile("mobile"), COLUMN_email(
				"email"), COLUMN_remark("remark"), COLUMN_status("status");

		private String label;

		private HelpTicket(String label) {
			this.label = label;
		}

		public String getLabel() {
			return label;
		}
	}

	public enum Discount {

		COLUMN_id("id"), COLUMN_domainName("domain_name"),COLUMN_dateTime("datetime"), COLUMN_companyName(
				"company_name"), COLUMN_rate("rate"), COLUMN_status("status");

		private String label;

		private Discount(String label) {
			this.label = label;
		}

		public String getLabel() {
			return label;
		}
	}

	public enum RegisterUser {

		COLUMN_registerUserId("register_user_id"),
		COLUMN_firstName("first_name"),
		COLUMN_middleName("middle_name"),
		COLUMN_lastName("last_name"),
		COLUMN_fatherFirstName("father_first_name"),
		COLUMN_father_middle_name("father_middle_name"),
		COLUMN_father_last_name("father_last_name"),
		COLUMN_dob("dob"),
		COLUMN_pan("pan"),
		COLUMN_gender("gender"),
		COLUMN_mobileNumber("mobile_number"),
		COLUMN_emailId("email_id"),
		COLUMN_addressLine1("address_line1"),
		COLUMN_addressLine2("address_line2"),
		COLUMN_city("city"),
		COLUMN_state("state"),
		COLUMN_pin("pin"),
		COLUMN_accountNumber("account_number"),
		COLUMN_ifscCode("ifsc_code"),
		COLUMN_bankName("bank_name"),
		COLUMN_bankAddress("bank_address"),
		COLUMN_assessmentYear("assessment_year"),
		COLUMN_bankInterest("bank_interest"),
		COLUMN_remark("remark"),
		COLUMN_transactionId("transactionId"),
		COLUMN_dateTime("date"),
		COLUMN_status("status"),
		COLUMN_amount("amount");
		
		private String label;

		private RegisterUser(String label) {
			this.label = label;
		}

		public String getLabel() {
			return label;
		}
	}

	public enum UserFile {

		
		COLUMN_registerUserId("register_user_id"),
		COLUMN_form16_first_partA("form16_first_partA"),
		COLUMN_form16_first_partA_password("form16_first_partA_password"),
		
		COLUMN_form16_first_partB("form16_first_partB"),
		COLUMN_form16_first_partB_password("form16_first_partB_password"),
		COLUMN_form16_second_partA("form16_second_partA"),
		COLUMN_form16_second_partA_password("form16_second_partA_password"),
		COLUMN_form16_second_partB("form16_second_partB"),
		COLUMN_form16_second_partB_password("form16_second_partB_password"),
		COLUMN_form16_third_partA("form16_third_partA"),
		COLUMN_form16_third_partA_password("form16_third_partA_password"),
		COLUMN_form16_third_partB("form16_third_partB"),
		COLUMN_form16_third_partB_password("form16_third_partB_password"),
		COLUMN_form16_fourth_partA("form16_fourth_partA"),
		COLUMN_form16_fourth_partA_password("form16_fourth_partA_password"),
		COLUMN_form16_fourth_partB("form16_fourth_partB"),
		COLUMN_form16_fourth_partB_password("form16_fourth_partB_password"),
		
		COLUMN_form16A_1("form16A_1"),
		COLUMN_form16A_1_password("form16A_1_password"),
		COLUMN_form16A_2("form16A_2"),
		COLUMN_form16A_2_password("form16A_2_password"),
		COLUMN_form16A_3("form16A_3"),
		COLUMN_form16A_3_password("form16A_3_password"),
		COLUMN_form16A_4("form16A_4"),
		COLUMN_form16A_4_password("form16A_4_password"),
		
		COLUMN_income("income"),
		COLUMN_proof1("Proof1"),
		COLUMN_proof1_password("Proof1_password"),
		COLUMN_proof2("Proof2"),
		COLUMN_proof2_password("Proof2_password"),
		COLUMN_proof3("Proof3"),
		COLUMN_proof3_password("Proof3_password"),
		COLUMN_proof4("Proof4"),
		COLUMN_proof4_password("Proof4_password");
		
		private String label;

		private UserFile(String label) {
			this.label = label;
		}

		public String getLabel() {
			return label;
		}
	}

}
