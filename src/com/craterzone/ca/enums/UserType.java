package com.craterzone.ca.enums;

public enum UserType {

	ADMIN("admin"),
	STAFF("staff");
	
	private String userTypeLabel;
	
	private UserType(String userTypeLabel) {
		this.userTypeLabel = userTypeLabel;
	}
	
	public String getUserType() {
		return userTypeLabel;
	}
	
}
