package com.craterzone.ca.service;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import com.craterzone.ca.common.util.MailerImpl;

public class EmailService {

	final static Logger _logger = Logger
			.getLogger(EmailService.class.getName());

	private static EmailService _instance;

	private Session _senderSession;

	private EmailService() {

		Properties mailprop = getProperties("mail.properties");

		final String username = mailprop.getProperty("mail.username");// "contact@craterzone.com";

		final String password = mailprop.getProperty("mail.password"); // "crat555";

		_senderSession = Session.getInstance(mailprop,

		new javax.mail.Authenticator() {

			protected PasswordAuthentication getPasswordAuthentication() {

				return new PasswordAuthentication(username, password);

			}

		});

	}

	public static synchronized EmailService getInstance() {
		if (_instance == null) {

			_instance = new EmailService();

		}

		return _instance;

	}

	/**
	 * 
	 * This Method load a properties file and provide object of that
	 * 
	 * @param filename
	 * 
	 * @return
	 */

	private Properties getProperties(String filename) {
		Properties prop = new Properties();

		try {
			prop.load(getClass().getResourceAsStream(filename));

		} catch (Exception e) {

			_logger.log(Level.SEVERE, "Unable to execute getProperties in EmailService" + e);

		}
		return prop;
	}

	/**
	 * 
	 * This Method Send Mail
	 * 
	 * 
	 * 
	 * @param mailer
	 *            :
	 * 
	 * @throws Exception
	 */

	public boolean sendMail(MailerImpl mailer, String ccEmail) {
		boolean sendMail = false;

		try {

			if (_senderSession == null) {

				_logger.log(Level.SEVERE,
						"Unable to send email to " + mailer.getReceiver());

			}
			Message message = new MimeMessage(_senderSession);
			/* Set amil Sender */
			message.setFrom(new InternetAddress(_senderSession
					.getProperty("mail.sender")));
			/* Set mail Receiver */
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(mailer.getReceiver()));
			if ((ccEmail != null)) {// &&
									// (!ccEmail.equals("navrattan.craterzone@gmail.com"))
				message.setRecipients(Message.RecipientType.CC,
						InternetAddress.parse(ccEmail));
			}
			/* Message BodyPart */
			BodyPart messageBodyPart = new MimeBodyPart();
			/* MultiPart */
			Multipart multipart = new MimeMultipart();
			/* Set Subject */
			message.setSubject(mailer.getSubject());
			/* Set message Content */

			 messageBodyPart.setContent(mailer.getContent(),
			 mailer.getContentType());
			// Text/plain  
			 //having trouble with setting content. i want to send only text
			// content so i set text as content type
//			messageBodyPart.setText(mailer.getContent());
			/* add message Body Part to multipart */
			multipart.addBodyPart(messageBodyPart);
			/* Check if have Attachment : Attach it to mail if it have. */
			if (mailer.isContainAttachment()) {
				if (mailer.getAttachment() != null) {
					/* Message BodyPart */
					BodyPart messageAttachment = new MimeBodyPart();
					DataSource source = new ByteArrayDataSource(
							mailer.getAttachment(), "application/vnd.ms-excel");
					;
					messageAttachment.setDataHandler(new DataHandler(source));
					messageAttachment.setHeader("Content-Type",
							"application/vnd.ms-excel; charset=\"us-ascii\"; name="
									+ mailer.getAttachmentName());
					multipart.addBodyPart(messageAttachment);
				} else {
					_logger.log(Level.SEVERE,
							"Unable to Send  Attachment with mail for "
									+ mailer.getReceiver());
				}
			}
			/* Add multipart to Message */
			message.setContent(multipart);
			/* send Mail */
			Transport.send(message);
			sendMail = true;
		} catch (MessagingException e) {
			_logger.log(Level.SEVERE,
					"Unable to send email to " + mailer.getReceiver(), e);
		} catch (Exception e) {
			_logger.log(Level.SEVERE,
					"Unable to send email to " + mailer.getReceiver(), e);

		}
		return sendMail;

	}

}
