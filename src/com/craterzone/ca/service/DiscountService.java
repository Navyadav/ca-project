package com.craterzone.ca.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;

import com.craterzone.ca.dao.DiscountDao;
import com.craterzone.ca.model.DiscountBean;


public class DiscountService {

	@Autowired
	DiscountDao _discountDao;
	
	public ArrayList<DiscountBean> getDiscount() {
		ArrayList<DiscountBean> openTickets=_discountDao.getDiscountList();
		return openTickets;
		}
	public boolean deleteDiscount(int id) {
		if(_discountDao.deleteDiscount(id)) {
		return true;
		}
		return false;
	}
	public boolean addDiscount(DiscountBean discountBean) {
		if(_discountDao.addDiscount(discountBean)) {
			return true;
		}
		return false;
	}
	public boolean updateDiscount(DiscountBean discountBean) {
		if(_discountDao.updateDiscount(discountBean)) {
			return true;
		}
		return false;
	}
}
