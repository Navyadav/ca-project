package com.craterzone.ca.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


import org.springframework.beans.factory.annotation.Autowired;
import com.craterzone.ca.common.util.FileConverterUtil;
import com.craterzone.ca.common.util.FilePathImpl;
import com.craterzone.ca.dao.ReportDao;
import com.craterzone.ca.model.Feedback;
import com.craterzone.ca.model.RegisterationBean;

@SuppressWarnings("deprecation")
public class ReportService implements FilePathImpl {
	
	@Autowired
	private ReportDao _reportDao;
	
	final static Logger _logger = Logger.getLogger(ReportService.class.getName());
	
	/**
	 * Zips the folder whose name is userId
	 * @param userId
	 */
	public void zipFolder(String userId) {
		
		if(userId == null) {
			return;
		}
		
		File dir = new File(UPLOAD_FOLDER_DB_PATH + userId);
		FileConverterUtil.zipDirectory(dir, XLS_FILE_PATH + userId);
		
	}
	
	public void zipWholeFolder(String userId) {
		
		if(userId == null) {
			return;
		}
		
		File dir = new File(DOWNLOAD_FOLDER_PATH);
		FileConverterUtil.zipDirectory(dir, ALL_ZIPS_FOLDER_PATH + SEPERATOR + userId);
		
	}
	
	public void generateXls(String userId) {
		
		if(userId == null) {
			return;
		}
		
		RegisterationBean bean = _reportDao.getUserData(userId); 
		Map<Integer, Object[]> data = new HashMap<Integer, Object[]>();
		data.put(1, new Object[] {"UserID", "First Name", "Middle Name",
				"Last Name","Father's First Name","Father's Middle Name", "Father's Last Name",
				"Date of Birth","Pan No","Gender","Mobile Number","Email Id",
				"Address Line 1","Address Line 2","City","State","Pin","Account No","IFSC Code",
				"Bank Name","Bank Address","Assessment Year","Bank Interest","Remarks"});
		data.put(2, new Object[] {bean.getUserId(), bean.getfUserName(), bean.getmUserName(),
				bean.getlUserName(),bean.getfFatherName(),bean.getmFatherName(),bean.getlFatherName(),
				bean.getDOB(),String.valueOf(bean.getPanNumber()),bean.getGender(),bean.getMobileNumber(),bean.getEmail(),
				bean.getAddress1(),bean.getAddress2(),bean.getCity(),bean.getState(),bean.getPinCode(),
				bean.getAccountNumber(),bean.getIfsc(),bean.getBankName(),bean.getBankAddress(),bean.getAssessmentYear(),
				bean.getBankInterest(),bean.getRemark()});
		FileConverterUtil.convertToXls(data, userId, XLS_FILE_PATH);
		
	}

	public List<Feedback> getLatestFeedback() {
		return new ArrayList<Feedback>();
	}

	public List<RegisterationBean> getAdminReport() {
		return _reportDao.getAllUserData();
	}
	
	// Overloaded
	public RegisterationBean getAdminReport(String userId) {
		return _reportDao.getUserData(userId);
	}

	public void downloadAll(String ids) {
		
		if(ids == null) {
			return;
		}
		
		List<RegisterationBean> registerUsersList = _reportDao.getAllUserData(ids);
		
		for(int i = 0;i < registerUsersList.size();i++) {
			this.zipFolder(registerUsersList.get(i).getUserId());
		}
		
		for(int i = 0;i < registerUsersList.size();i++) {
			this.generateXls(registerUsersList.get(i).getUserId());
		}
		zipAllZipsFolder();
		
	}
	
	private void zipAllZipsFolder() {
		
		File dir = new File(DOWNLOAD_FOLDER_PATH);
		
		FileConverterUtil.zipDirectory(dir, PROJECT_FOLDER_PATH + SEPERATOR + "allZips");
		
	}
	
	public void clean() {
		
		try {
			
			new File(ALL_ZIPS_FOLDER_PATH + ".zip").delete();
			
			File[] files = new File(DOWNLOAD_FOLDER_PATH).listFiles();
			for(File fileName : files) {
				fileName.delete();
			}
			
			File[] files2 = new File(ALL_ZIPS_FOLDER_PATH).listFiles();
			for(File fileName : files2) {
				fileName.delete();
			}
	
		} catch (Exception e) {
			_logger.log(Level.SEVERE, "Unable to clean directories after download " + e);
		}
	}
	
	/**
	 * Search the registered users according to the parameters
	 * 
	 * @param dateFrom
	 * @param dateTo
	 * @param keyword
	 * @param searchBy
	 * @return List<RegisterationBean>
	 */
	
	public List<RegisterationBean> search(String dateFrom, String dateTo, String keyword, String searchBy) {

		Date parsedDateFrom = null;
		Date parsedDateTo = null;
		if(!dateFrom.equals("")) {
			parsedDateFrom = new Date(dateFrom);
		}
		if(!dateTo.equals("")) {
			parsedDateTo = new Date(dateTo);
			parsedDateTo.setHours(23);
			parsedDateTo.setMinutes(59);
			parsedDateTo.setSeconds(59);
		}
		
		return _reportDao.search(parsedDateFrom, parsedDateTo, keyword.trim(), searchBy);
	}
	
	/**
	 * Generates admin report in the form of map,
	 * keys are days(today,yesterday,etc),
	 * values are List for the particular key(day)
	 * 
	 * @param dateFrom
	 * @param dateTo
	 * @return Map<String, List<RegisterationBean>>
	 */
	
	 public Map<String, List<RegisterationBean>> getAdminReport(String dateFrom,
	            String dateTo) {
	        Date parsedDateFrom = new Date(dateFrom);
	        Date parsedDateTo = new Date(dateTo);
	        Calendar c = Calendar.getInstance();
	        c.setTime(parsedDateTo);
	        int i = c.get(Calendar.DAY_OF_WEEK) - c.getFirstDayOfWeek();
	        c.add(Calendar.DATE, -i - 7);
	        Date start = c.getTime();
	        c.add(Calendar.DATE, 6);
	        Date end = c.getTime();
	        List<RegisterationBean> registerationBeanlist = _reportDao.search(
	                parsedDateFrom, parsedDateTo, "", "");

	        Map<String, List<RegisterationBean>> map = new HashMap<String, List<RegisterationBean>>();
	        Date now = new Date();
	        for (RegisterationBean registerationBean : registerationBeanlist) {

	            if (registerationBean.getDateTime().getDate() == now.getDate()) {
	                List<RegisterationBean> todayRegisterationBeansList = map
	                        .get("Today");

	                if (todayRegisterationBeansList == null) {
	                    todayRegisterationBeansList = new ArrayList<RegisterationBean>();
	                }
	                todayRegisterationBeansList.add(registerationBean);
	                map.put("Today", todayRegisterationBeansList);
	            } else if (registerationBean.getDateTime().getDate() == (now
	                    .getDate() - 1)) {
	                List<RegisterationBean> todayRegisterationBeansList = map
	                        .get("Yesterday");

	                if (todayRegisterationBeansList == null) {
	                    todayRegisterationBeansList = new ArrayList<RegisterationBean>();
	                }
	                todayRegisterationBeansList.add(registerationBean);
	                map.put("Yesterday", todayRegisterationBeansList);
	            } else if (registerationBean.getDateTime().getDate() <= now
	                    .getDate()
	                    && registerationBean.getDateTime().getDate() >= now
	                            .getDate()-7) {
	                List<RegisterationBean> todayRegisterationBeansList = map
	                        .get("ThisWeek");

	                if (todayRegisterationBeansList == null) {
	                    todayRegisterationBeansList = new ArrayList<RegisterationBean>();
	                }
	                todayRegisterationBeansList.add(registerationBean);
	                map.put("ThisWeek", todayRegisterationBeansList);
	            } else if (registerationBean.getDateTime().getDate() <= end
	                    .getTime()
	                    && registerationBean.getDateTime().getDate() >= start
	                            .getTime()) {
	                List<RegisterationBean> todayRegisterationBeansList = map
	                        .get("LastWeek");

	                if (todayRegisterationBeansList == null) {
	                    todayRegisterationBeansList = new ArrayList<RegisterationBean>();
	                }
	                todayRegisterationBeansList.add(registerationBean);
	                map.put("ThisWeek", todayRegisterationBeansList);
	            }

	        }
	        return map;
	    }
}
