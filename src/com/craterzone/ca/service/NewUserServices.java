package com.craterzone.ca.service;

import java.io.File;
import java.io.IOException;
import java.text.DateFormatSymbols;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import com.craterzone.ca.common.util.FileHandler;
import com.craterzone.ca.common.util.FilePathImpl;
import com.craterzone.ca.common.util.FileUploader;
import com.craterzone.ca.common.util.MailerImpl;
import com.craterzone.ca.dao.DiscountDao;
import com.craterzone.ca.dao.RegisterUserDao;
import com.craterzone.ca.model.RegisterationBean;

@SuppressWarnings("deprecation")
public class NewUserServices implements FilePathImpl {

	final static Logger _logger = Logger.getLogger(NewUserServices.class.getName());
	
	private static int _date;
	private static Integer _counter;
	
	@Autowired
	private RegisterUserDao _registerUser;
	
	@Autowired
	private DiscountDao _discountDao;

	public boolean checkForUserExist(String email) {
		return _registerUser.isUserExists(email);
	}
	
	public boolean checkForStaffExist(String uname) {
		return _registerUser.isStaffExist(uname);
	}

	public boolean addStaff(String uname, String password) {
		if (_registerUser.staffRegister(uname, password)) {
			return true;
		}
		return false;
	}
	
	public String userRegister(RegisterationBean basicRegisteration)
			throws IOException {
		String newUserId = generateRegisterId();
		uploadOneFile(basicRegisteration.getFiles(), newUserId);
		if (_registerUser.userInsert(basicRegisteration, newUserId) > 0) {
			return newUserId;
		}
		_counter--;
		return "";
	}
	
	
	private void uploadOneFile(List<MultipartFile> fileList, String uId) throws IOException {
		for (MultipartFile fileToUpload : fileList) {
			if (fileToUpload.getSize() != 0)
				FileUploader.uploadFile(fileToUpload, uId);
		}
	}
	
	/**
	 * 
	 * Generated id as : 
	 * 
	 * Name of month (first 3 characters)
	 * Year          (next two character)
	 * Day of month  (Next two)
	 * serial number of record for that day in six character
	 * 
	 * examples:
	 * JAN1430000006
	 * JAN1430000007
	 * JAN1431000001
	 * JAN1431000002
	 * 
	 * @return userId - String
	 */
	private static String generateRegisterId() {
		Date today = new Date();
		if(today.getDate()!=_date) {
			_date=today.getDate();
			_counter=0;
		}
		_counter++;
		String monthString = new DateFormatSymbols().getMonths()[today.getMonth()];
		StringBuilder userId = new StringBuilder(monthString.substring(0, 3).toUpperCase());
			userId.append(today.getYear()-100); 
			userId.append(today.getDate());
		byte i =(byte) _counter.toString().length();
		while(i < 6){
			userId.append("0");
			i++;
		}
		return userId.toString()+_counter;
	}
	
	public double getPaidAmountCalculate(List<MultipartFile> fileList) {
		double amount = 0;
		for (MultipartFile fileToUpload : fileList) {
			if (fileToUpload.getSize() != 0) {
				amount += 50;
			}
		}
		return amount;

	}
	
	public boolean updateAfterPaymentSuccess(final RegisterationBean registerationBean,int status) {
		try {
		// folder creation if not present
		File tempFolder = new File(PROJECT_FOLDER_PATH);
		if(!tempFolder.exists()) {
			tempFolder.mkdirs();
			new File(PROJECT_FOLDER_PATH).mkdirs();
			new File(DOWNLOAD_FOLDER_PATH).mkdirs();
			new File(UPLOAD_FOLDER_PATH).mkdirs();
			new File(ALL_ZIPS_FOLDER_PATH).mkdirs();
		}
		if (_registerUser.registeredUserUpdate(registerationBean,status)) {
			if (status == 1) {
				if(FileHandler.move(TEMP_UPLOAD_FOLDER_PATH + registerationBean.getUserId(), UPLOAD_FOLDER_PATH, registerationBean.getUserId())){
						new Thread("SendingRegistrationtMail") {
							public void run() {
								MailerImpl mailService = new CaMailService();
								mailService.setReceiver(registerationBean.getEmail());
								mailService.setContentType("text/html");
								mailService.setSubject("Registration Confirmation Mail");
								String str = CaMailService._registration_mail_content;
								str = str.replace(":ORDERID",registerationBean.getTransactionId());
								mailService.setContent(str);
								if (EmailService.getInstance().sendMail(mailService, null)) {
									_logger.log(Level.FINE, "mail sent to user ");
								} else {
									_logger.log(Level.WARNING, "mail not sent to user ");
								}
							}
						}.start();	
					}
				}
		} else {
			File file = new File(TEMP_UPLOAD_FOLDER_PATH + registerationBean.getUserId());
			FileHandler.deleteRestricted(file, file.getPath());
			file.delete();
		}
		return true;
		} catch (SecurityException e) {
			_logger.log(Level.SEVERE, "Unable to move/delele folder in folder: " + TEMP_UPLOAD_FOLDER_PATH + e);
		} catch (NullPointerException e) {
			_logger.log(Level.SEVERE, "Unable to perform updateAfterPaymentSuccess process " + e);
		}
		return false;
	}
	
	
	public double discount(String email, double paidAmount) {
		
		return (((100-_discountDao.getDiscountForDomain(email.split("@")[1])) * paidAmount)/100);
		
	}
}
