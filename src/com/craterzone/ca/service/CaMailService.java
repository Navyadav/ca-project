package com.craterzone.ca.service;

import java.util.Map;
import java.util.logging.Logger;

import com.craterzone.ca.common.util.MailerImpl;

public class CaMailService implements MailerImpl {

	
	private String subject;
	private String receiver;
	private String content;
	private String[] cc; 
	private String contentType;
	public static String _registration_mail_content = "<html><body><div style='font-family:Bauhaus 93;font-size:14px'><b>Filemyreturn.co.in </b></div><div style='font-family:Brush Script MT;font-size:14px'>A venture of Rai Tax Consultants</div><br><br><div style='font-family:Times New Roman;font-size:12px'>Dear Sir/Madam,<br>" +
			"<br>Welcome to the Rai�s Family,<br><p>Thanks for providing us an opportunity to file your Income Tax Return. Please find enclosed your payment Receipt. We request you to retain the same for future correspondence/support. Further, we would like to inform you that you can track the status of your return as well by logging on to filemyreturn.co.in and entering your details.</p>" +
			"<p>You will be receiving your Income Tax Return Acknowledgement (ITR-V) from �donotreply@incometaxindiaefiling.gov.in� in a password protected PDF file. The password to open the same is your PAN No. (in lower case) followed by your Date of Birth in DDMMYYYY format.</p>" +
			"<p>After receiving your Income Tax Return Acknowledgement on your email id provided at the time of filing, kindly save and print the same. Kindly send a signed copy of your ITR-V on the below mentioned postal address:</p>" +
			"<br>Centralised Processing Center<br>Post Bag No. 1, Electronic City Post Office, Bengaluru, Karnataka-560100." +
			"<br>" +
			"Your OrderID is as follows: :ORDERID </div>" +
			"</body></html>";
	final static Logger _logger = Logger.getLogger(CaMailService.class
			.getName());

	public static final int _MAIL_TYPE = _CONTACT_US_NATURAL_BEACH_MAIL;
	

	public final boolean _CONTAIN_ATTACHMENT = false;
	@Override
	public int getMailType() {
		return _MAIL_TYPE;
	}

	@Override
	public String getSubject() {
		return subject;
	}

	@Override
	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Override
	public String getReceiver() {
		return receiver;
	}

	@Override
	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	@Override
	public String getContent() {
		return content;
	}
	
	
	public void setContent(String content) {
		this.content=content;
	}
	@Override
	public String getContentType() {
		return contentType;
	}

	@Override
	public String getTemplateName() {
		return null;
	}

	@Override
	public Map<String, String> createDataForTemplate() {
		return null;
	}

	@Override
	public boolean isContainAttachment() {
		return false;
	}

	@Override
	public byte[] getAttachment() {
		return null;
	}

	@Override
	public void setAttachment(byte[] attachement) {

	}

	@Override
	public void setAttachmentName(String attachmentName) {

	}

	@Override
	public String getAttachmentName() {
		return null;
	}

	@Override
	public void setCC(String[] cc) {
		this.cc = cc;
	}
	@Override
	public String[] getCC(){
		return this.cc;
	}
	public void setContentType(String contentType){
		this.contentType = contentType;
	}
	
	
}
