package com.craterzone.ca.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.craterzone.ca.dao.UserLoginDao;
import com.craterzone.ca.model.LoginUser;

public class UserLoginService {

	@Autowired
	private UserLoginDao _userLoginDao;
	
	public boolean validUser(LoginUser loginUser) {
		
		if(loginUser == null) {
			return false;
		}
		
		if(_userLoginDao.isValidUser(loginUser)) {
			return true;
		}
		
		return false;
	}

}
