package com.craterzone.ca.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;

import com.craterzone.ca.common.util.RegularExpressionValidate;
import com.craterzone.ca.dao.TicketDao;
import com.craterzone.ca.model.TicketBean;

public class TicketService {

	@Autowired
	TicketDao _ticketDao;

	public int makeTicket(TicketBean ticketBean) {
		return _ticketDao.insertTicket(ticketBean);

	}

	public ArrayList<TicketBean> getAllOpenTickets() {
		ArrayList<TicketBean> openTickets = _ticketDao.getOpenTicket();
		return openTickets;
	}

	public boolean closeTicket(String transactionId) {
		if (_ticketDao.getTicketsClose(transactionId) > 0) {

			return true;
		}
		return false;
	}

	public String customValidation(String email, long mobile) {

		if (!RegularExpressionValidate.isEmailValid(email)) {
			return "Email Id is Not Valid";
		}
		if (mobile > 10000000000l || mobile < 1000000000l) {
			return "Mobile Number is Not valid";
		}
		return "";
	}

}
