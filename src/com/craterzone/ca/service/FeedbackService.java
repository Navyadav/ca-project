package com.craterzone.ca.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.craterzone.ca.dao.FeedbackDao;
import com.craterzone.ca.model.Feedback;

public class FeedbackService {
	
	@Autowired
	private FeedbackDao _feedbackDao;

	public int saveFeedback(Feedback feedback) {
		try {
			int updates = _feedbackDao.addFeedback(feedback);
			if(updates > 0) {
				return updates; 
			}
		} catch (Exception e) {
			return 0;
		}
		return 0;
	}
	
	public Feedback getFeedbackDetails(int feedbackId) {
		
		return _feedbackDao.getFeedback(feedbackId);
		
	}
	
	public int deleteFeedback(int feedbackId) {
		return _feedbackDao.deleteFeedback(feedbackId);
	}
	
	public List<Feedback> getAllFeedbacks() {
		return _feedbackDao.getAllFeedbacks();
	}
	
	public int savePreferredFeedback(Feedback feedback) {
		try {
			int updates = _feedbackDao.addPreferredFeedback(feedback);
			if(updates > 0) {
				return updates; 
			}
		} catch (Exception e) {
			return 0;
		}
		return 0;
	}

	public List<Feedback> getPreferredFeedbacks() {
		return _feedbackDao.getPreferredFeedbacks();
	}
	
}
