package com.craterzone.ca.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.craterzone.ca.model.RegisterationBean;

public class FormValidator implements Validator {

	String _className;
	Object _obj;
	
	
	public FormValidator(String name,Object obj) {
		_className=name;
		_obj=obj;
	}
	@Override
	public boolean supports(Class<?> clazz) {
		
		return _obj.getClass().isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object object, Errors errors) {
		
		if(_className.equals("RegisterationBean")) {
			
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fUserName", "fUserName.required","User First Name Invalid");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lUserName", "lUserName.required","User Last Name Invalid");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fFatherName", "fFatherName.required","User Father first Name Invalid");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lFatherName", "lFatherName.required","User Father Last Name Invalid");

		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "DOB", "DOB.required","User Date Of Birth Invalid");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "panNumber", "panNumber.required","User Pan Card number Invalid");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mobileNumber", "mobileNumber.required","User Mobile Number Invalid");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "email.required","User Email id Invalid");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address1", "address1.required","User Address Invalid");
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "city", "city.required","User City Invalid");		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "state", "state.required","User state Invalid");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pinCode", "pinCode.required","User Pincode Invalid");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "accountNumber", "accountNumber.required","User AccountNumber Invalid");
		
		if(((RegisterationBean)object).getIfsc().equalsIgnoreCase("")) {
			if(((RegisterationBean)object).getBankName().equalsIgnoreCase("")) {
				errors.rejectValue("bankName", "bankName.required", "Bank Name is required");
			}
			if(((RegisterationBean)object).getBankAddress().equalsIgnoreCase("")) {
				errors.rejectValue("bankAddress", "bankAddress.required", "Bank Name is required");
			}
		}
		
		} else if(_className.equals("TicketBean")) {
			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "transactionId", "transactionId.required","Transaction Id is Required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "email.required","Email id is Required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "panNumber", "panNumber.required","Pan Card Number is Required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mobile", "mobile.required","Mobile Number is Required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "remarks", "remarks.required","Remarks are Required");
		}
	}
	

}
