package com.craterzone.ca.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.craterzone.ca.model.Feedback;

public class FeedbackValidator implements Validator {

		public boolean supports(Class<?> arg0) {
			return Feedback.class.isAssignableFrom(arg0);
		}

		public void validate(Object target, Errors errors) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors,"imgUrl","imgUrl.required","Enter Image URL");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors,"logoUrl","logoUrl.required","Enter Logo URL");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors,"feedbackMessage","feedbackMessage.required","Enter Feedback Message");
		}
	}
