package com.craterzone.ca.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.craterzone.ca.common.util.MailerImpl;
import com.craterzone.ca.model.TicketBean;
import com.craterzone.ca.service.CaMailService;
import com.craterzone.ca.service.EmailService;
import com.craterzone.ca.service.TicketService;
import com.craterzone.ca.validation.FormValidator;

@Controller
@SessionAttributes("ticket")
public class TicketController {

	final static Logger _logger = Logger.getLogger(TicketController.class.getName());
	
	@Autowired
	TicketService _ticketService;
	

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, true));
	}

		/**
		 * 
		 * @param  Showing Ticket Opening page to user for making new ticket
		 * @return
		 */
	@RequestMapping("/ticket.htm")
	public ModelAndView getTicketPage(HttpServletRequest request,
			HttpServletResponse response) {

		ModelAndView model = new ModelAndView();
		model.addObject("ticket", new TicketBean());
		model.setViewName("ticket_opening");
		return model;
	}
		
	/*
	 * 
	 * Map this function When user submit the new ticket
	 * validation is performing
	 * Sending mail after making new Ticket
	 */

	@RequestMapping("/submitTicket.htm")
	public ModelAndView submitTicket(
			@ModelAttribute("ticket") final TicketBean ticket,
			BindingResult bindingResult, HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView model = new ModelAndView();
		FormValidator formValidator=new FormValidator("TicketBean", ticket);
		formValidator.validate(ticket, bindingResult);
		if (bindingResult.hasErrors()) {
			model.addObject("error", "Required Fields are invalid");
			model.setViewName("ticket_opening");
			return model;
		}
		String errorMsg=_ticketService.customValidation(ticket.getEmail(), ticket.getMobile());
		if(!errorMsg.equals("")) {
			model.addObject("error", errorMsg);
			model.setViewName("ticket_opening");
			return model;
		}
		final int generateKey=_ticketService.makeTicket(ticket);
		if (generateKey>0) {
			model.setViewName("submitTicket");
			try {
				new Thread("SendingTicketMail"){
					public void run() {
						MailerImpl mailService = new CaMailService();
						mailService.setReceiver(ticket.getEmail());
						mailService.setContentType("text/plain");
						mailService.setSubject("Ticket Confirmation Mail");
						mailService.setContent("Dear "+ticket.getEmail().split("@")[0]+",\n\n\n\t\t Your ticket id is Help-Ticket-id "+generateKey);
						if (EmailService.getInstance().sendMail(mailService, mailService.ccAdmin)) {
							_logger.log(Level.INFO, "mail sent to user");
						} else {
							_logger.log(Level.INFO, "mail not sent");
						}
					}
					
				}.start();	
			} catch(Exception e) {
				_logger.log(Level.INFO, "Excption");
				model.setViewName("ticket_opening");
				model.addObject("error", "Error in sending email");
			}
			
			return model;
			
		}
		model.setViewName("ticket_opening");
		model.addObject("error", "error in data base insertion");
		return model;
		
	}

		/**
		 * 
		 * @param 	Showing all Ticket to admin
		 * @return
		 */
	@RequestMapping("/alltickets.htm")
	public ModelAndView getAllTickets(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView model = new ModelAndView();
		ArrayList<TicketBean> openTicket = _ticketService.getAllOpenTickets();
		if (openTicket != null) {
			model.setViewName("ticket_admin");
			model.addObject("Tickets", openTicket);
			model.addObject("TicketsReply", new TicketBean());
			return model;
		}
		model.setViewName("ticket_admin");
		model.addObject("error", "No Open Tickets");
		return model;
	}

			/**
			 * 
			 *  @param ticketBean contain New Ticket bean with new Remarks that admin puts
			 * @Mailing sending remarks as mail body to user
			 */
	@RequestMapping("/ticketreply.htm")
	public ModelAndView replyTicket(HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("TicketsReply") final TicketBean ticketBean) {
		ModelAndView model = new ModelAndView();
		 
		
			try{
			new Thread("HelpTicket") {
				public void run() {
					MailerImpl mailService = new CaMailService();
					mailService.setReceiver(ticketBean.getEmail());
					mailService.setSubject("Mail Reply");
					mailService.setContent(ticketBean.getRemarks());
					mailService.setContentType("text/plain");
					/* Tickets close and mail sent to user */
					if (EmailService.getInstance().sendMail(mailService, null)) {
						_logger.log(Level.INFO, "mail sent to user");
					} else {
						_logger.log(Level.INFO, "mail not sent");
					}
				}
			}.start();
			} catch(Exception e){
				_logger.log(Level.SEVERE, "Unable to process replyTicket request" + e);
			}
			if (_ticketService.closeTicket(ticketBean.getTransactionId())) {
			model.setViewName("redirect:/alltickets.htm");
			return model;
		}
		model.setViewName("ticket_admin");
		model.addObject("error", "Mail sent error");
		return model;
	}

}
