package com.craterzone.ca.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.craterzone.ca.model.LoginUser;
import com.craterzone.ca.service.UserLoginService;

@Controller
@SessionAttributes
public class UserLoginController {

	@Autowired
	private UserLoginService _userLoginService;
	
	@RequestMapping("/login.htm")
	public ModelAndView viewLoginForm(ModelAndView mav) {
		
		LoginUser loginUser = new LoginUser();
		mav.addObject("LoginUser",loginUser);
		mav.setViewName("loginForm");
		return mav;
	}
	
	@RequestMapping(value="/doLogin.htm",method=RequestMethod.POST)
	public ModelAndView loginCheck(@ModelAttribute("LoginUser") LoginUser loginUser, ModelAndView mav, HttpServletRequest request) {
		
		if(!_userLoginService.validUser(loginUser)) {
			mav.addObject("error","User does not exists");
			mav.setViewName("loginForm");
			return mav;
		}
		
		HttpSession session = request.getSession();
		session.setAttribute("username", loginUser.getUsername());
		mav.setViewName("redirect:/reportGenerate.htm");
		return mav;
	}
	
	@RequestMapping("/logout.htm")
	public ModelAndView logout(HttpServletRequest request, ModelAndView mav) {
		HttpSession session =request.getSession(false); 
		//session.removeAttribute("username");
		if(session!=null) {
			session.removeAttribute("username");
			session.invalidate();
		}
		mav.setViewName("redirect:/login.htm");
		return mav;
	}
	
}
