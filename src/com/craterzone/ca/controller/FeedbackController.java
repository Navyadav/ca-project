package com.craterzone.ca.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.craterzone.ca.model.Feedback;
import com.craterzone.ca.service.FeedbackService;
import com.craterzone.ca.validation.FeedbackValidator;

@Controller
public class FeedbackController {

	@Autowired
	private FeedbackService _feedbackService;
	
	@Autowired
	private FeedbackValidator _feedbackValidator;

	/**
	 * Save the new feedback from the user and redirect to allFeedbacks page,
	 * 
	 * @param feedback
	 * @param result
	 * @param button
	 * @param mav
	 * @return ModelAndView, returns feedbackError message if result is not inserted successfully
	 */
	
	@RequestMapping(value="/saveFeedback", method=RequestMethod.POST)
	public ModelAndView setFeedback(@ModelAttribute("feedback") Feedback feedback, BindingResult result,@RequestParam("button") String button, ModelAndView mav) {
		
		// validation
		if(feedback == null) {
			mav.setViewName("feedback");
			return mav;
		}
		
		_feedbackValidator.validate(feedback, result);
		if(result.hasErrors()) {
			List<Feedback> listOfFeedbacks = _feedbackService.getAllFeedbacks();
			mav.addObject("feedback", listOfFeedbacks);
			//Feedback newfeedback = new Feedback();
			mav.addObject("NewFeedback", feedback);
			mav.setViewName("feedback");
			return mav;
		}
		
		int status = 0;
		if(button.equalsIgnoreCase("Add on top")) {
			status = _feedbackService.savePreferredFeedback(feedback);
		} else if(button.equalsIgnoreCase("Add")) {
			status = _feedbackService.saveFeedback(feedback);
		}
		
		if(status <= 0) {
			mav.addObject("feedbackError", "No result updated");
			mav.setViewName("redirect:/allFeedbacks.htm");
			return mav;
		}
		mav.addObject("feedback", feedback);
		mav.setViewName("redirect:/allFeedbacks.htm");
		return mav;
		
	}
	
	/**
	 * Deletes the particular feedback from DB with feedbackID,
	 * and redirect to the allFeedbacks page.
	 * 
	 * @param feedbackID
	 * @param mav
	 * @return ModelAndView
	 */
	
	@RequestMapping(value="/deleteFeedback", method=RequestMethod.GET)
	public ModelAndView deleteFeedback(@RequestParam("feedbackID") int feedbackID, ModelAndView mav) {
		
		_feedbackService.deleteFeedback(feedbackID);
		mav.setViewName("redirect:/allFeedbacks.htm");
		return mav;
		
	}
	
	/**
	 * Display all feedback details on the feedback page.
	 * Return ModelAndView object with a list of all feedbacks and Binding bean 
	 * for the new feedback form.
	 * 
	 * @param mav
	 * @return ModelAndView 
	 */
	
	@RequestMapping("/allFeedbacks")
	public ModelAndView getAllFeedbacks(ModelAndView mav) {
		
		List<Feedback> listOfFeedbacks = _feedbackService.getAllFeedbacks();
		mav.addObject("feedback", listOfFeedbacks);
		Feedback feedback = new Feedback();
		mav.addObject("NewFeedback", feedback);
		mav.setViewName("feedback");
		return mav;
	}
	
	/**
	 * Displays latest feedbacks to the index page.
	 * Returns the latest feedbacks list to the view.
	 * 
	 * @param mav
	 * @return ModelAndVew
	 */
	
	@RequestMapping("/index")
	public ModelAndView latestFeedbacks(ModelAndView mav) {
		
		List<Feedback> feedbackList = _feedbackService.getPreferredFeedbacks();
		mav.addObject("listOfFeedbacks",feedbackList);
		mav.setViewName("index");
		return mav;
		
	}
	
	
}
