package com.craterzone.ca.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.craterzone.ca.common.util.CustomValidation;
import com.craterzone.ca.model.RegisterationBean;
import com.craterzone.ca.service.NewUserServices;
import com.craterzone.ca.validation.FormValidator;

@Controller
@SessionAttributes({"NewUserInformation","Amount"})
public class RegisterationController {

	@Autowired
	private NewUserServices _newUserService;

	/**
	 * Binding the date in the View with format MM/dd/yyyy
	 * 
	 * @param binder
	 */

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, true));
	}
	
	

	@RequestMapping("/addUser.htm")
	public ModelAndView addUser(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView model = new ModelAndView("addUser");
		HttpSession session = request.getSession(false);
		
		if(session == null) {
			model.setViewName("redirect:/login.htm");
			return model;
		}
		
		if(session.getAttribute("username") == null) {
			model.setViewName("redirect:/login.htm");
			return model;
		}
		
		if(!((String)session.getAttribute("username")).equalsIgnoreCase("admin")) {
			
			model.addObject("error","You are not authorized to see this page");
			model.setViewName("redirect:/login.htm");
			return model;
		}
		return model;
	}

	@RequestMapping(value = "/submitUser.htm", method = RequestMethod.POST)
	public ModelAndView submitUser(HttpServletRequest request,
			HttpServletResponse response) {
		String uname = request.getParameter("username");
		String password = request.getParameter("password");
		ModelAndView model = new ModelAndView("addUser");
		if (_newUserService.checkForStaffExist(uname)) {
			if (!_newUserService.addStaff(uname, password)) {
				model.addObject("error", "Data Base Insertion error");
				return model;
			}
			model.addObject("error", "User Added sucessfully");
			return model;
		}
		model.addObject("error", "User with same User Name Exist");
		return model;
	}

	@RequestMapping("/firststep.htm")
	public ModelAndView getFirstStep(@RequestParam("step") String stepType,HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ModelAndView model = new ModelAndView();
		if(stepType.equals("simple")) {
			model.addObject("Amount", "150");
		} else if(stepType.equals("consult")) {
			model.addObject("Amount", "450");
		} else {
			model.setViewName("redirect:/index.htm");
			return model;
		}
		
		model.addObject("NewUserInformation", new RegisterationBean());
		model.setViewName("client_detail");
		return model;
	}

	@RequestMapping("/submitRegisterationfirst.htm")
	public ModelAndView getSecondStep(
			@ModelAttribute("NewUserInformation") RegisterationBean basicRegisteration,
			BindingResult bindingResult, HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView model = new ModelAndView();
		FormValidator formValidator = new FormValidator(
				"BasicRegisterationDetails", basicRegisteration);
		formValidator.validate(basicRegisteration, bindingResult);

		if (bindingResult.hasErrors()) {
			model.setViewName("client_detail");
			model.addObject("error", "Please complete the form");
			return model;
		}
		String error = CustomValidation
				.getValidateFirstForm(basicRegisteration);
		if (!error.equals("")) {
			model.setViewName("client_detail");
			model.addObject("error", error);
			return model;
		}
		if (_newUserService.checkForUserExist(basicRegisteration.getEmail())) {
			model.addObject("NewUserInformation", basicRegisteration);
			model.addObject("error",
					"User with same email id has been registered");
			model.setViewName("client_detail");
			return model;
		}
		model.addObject("NewUserInformation", basicRegisteration);
		model.setViewName("uploads");
		return model;
	}

	

	/**
	 * Action called when second step submit button is clicked
	 * 
	 * @param basicRegisteration
	 * @param request
	 * @param response
	 * @return ModelAndView
	 */
	@RequestMapping("/submitRegisterationSecond.htm")
	public ModelAndView submitSecond(
			@ModelAttribute("NewUserInformation") RegisterationBean basicRegisteration,@ModelAttribute("Amount") String stepAmount,
			HttpServletRequest request, HttpServletResponse response) {

		ModelAndView model = new ModelAndView();
		
		// checking for file present
		if (basicRegisteration.getFiles().get(0).getSize() == 0) {
			model.addObject("error", "Upload at Part A one file");
			model.setViewName("uploads");
			return model;
		}
		try {
			double amount=Double.parseDouble(stepAmount);
			String newUserId = _newUserService.userRegister(basicRegisteration);
			if (!newUserId.equals("")) {
				basicRegisteration.setUserId(newUserId);
				double paidAmount =1;// _newUserService.getPaidAmountCalculate(basicRegisteration.getFiles()) + amount;
				paidAmount = _newUserService.discount(basicRegisteration.getEmail(),paidAmount);
				model.addObject("amount", paidAmount);
				model.addObject("NewUserInformation", basicRegisteration);
				model.setViewName("submitRegisteration");
			} else {
				model.setViewName("uploads");
				model.addObject("error",
						"error in registration");
			}
			return model;

		} catch (IOException e) {
			model.setViewName("uploads");
			model.addObject("error", "error in File Uploading");
			model.addObject("NewUserInformation", basicRegisteration);
			return model;
		}

	}

	/**************** ............Online Payment Ca Work Flow /Piyush.............. ***********************/
	@RequestMapping("/afterPayment.htm")
	public ModelAndView afterPayment(
			@ModelAttribute("NewUserInformation") RegisterationBean basicRegisteration,
			HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		String status = "";
		try {
			
			String statusCode = request.getParameter("AuthDesc");
			basicRegisteration.setAmount(Double.parseDouble(request
					.getParameter("Amount")));
			final String orderId = request.getParameter("Order_Id");
			basicRegisteration.setTransactionId(orderId);
			if (statusCode != null) {
				int statusId = 3;
				if (statusCode.equals("Y")) {
					statusId = 1;
				} else if (statusCode.equals("B")) {
					statusId = 2;
					status = "Your transation is panding, please contact to support@corrucostos.com";
				} else {
					status = "Unable to recharge account, please contact to support@corrucostos.com";
				}
				if (_newUserService.updateAfterPaymentSuccess(
						basicRegisteration, statusId)) {
					status = "User has been Sucessfully Registered";
					model.addObject("OrderId", orderId);
				} else {
					status = "Error in Backend Process";
					model.addObject("OrderId", "");
				}
			}
		} catch (Exception e) {
			model.setViewName("receipt_page");
			model.addObject("status", "Unable to Continue");
			model.addObject("OrderId", "");
			return model;
		}
		model.setViewName("receipt_page");
		model.addObject("status", status);
		HttpSession session = request.getSession(false);
		if(session!=null) {
			session.removeAttribute("NewUserInformation");
		}
		return model;
	}
	
}
