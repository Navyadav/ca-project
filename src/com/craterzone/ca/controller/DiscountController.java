package com.craterzone.ca.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.craterzone.ca.model.DiscountBean;
import com.craterzone.ca.service.DiscountService;

@Controller
public class DiscountController {

	@Autowired
	private DiscountService _discountService;
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, true));
	}

		/**
		 *  showing all discounts to user
		 * 
		 */
	@RequestMapping("/showdiscount.htm")
	public ModelAndView showDiscount(HttpServletRequest request,
			HttpServletResponse response) {

		ModelAndView model = new ModelAndView();
		ArrayList<DiscountBean> openDiscount = _discountService.getDiscount();
		model.addObject("NewDiscount", new DiscountBean());
		model.setViewName("discount");
		if (openDiscount != null) {
			model.addObject("discount", openDiscount);
			return model;
		}
		model.addObject("error", "No Data Found for Discount");
		return model;

	}
	
		/**
		 * 
		 * @param  If User wants to delete any discount
		 * @return discount after deleting the particular discount
		 */
	@RequestMapping("/deleteDiscount")
	public ModelAndView deleteDiscount(@RequestParam("id") int deleteId) {
		ModelAndView model = new ModelAndView();
		if (_discountService.deleteDiscount(deleteId)) {
			model.setViewName("redirect:/showdiscount.htm");
			return model;
		}
		model.setViewName("discount");
		model.addObject("error", "Delet can not perform");
		return model;
	}
		/**
		 * 
		 * @param discountBean contain data of Discount Bean
		 * @param bindingResult binding result for checking server side validation
		 * @param request 	Checking for user wants to update of add new Discount
		 * @param response
		 * @return 
		 */
	@RequestMapping(value = "addnew.htm", method = RequestMethod.POST)
	public ModelAndView addDiscount(
			@ModelAttribute("NewDiscount") DiscountBean discountBean,BindingResult bindingResult,
			HttpServletRequest request, HttpServletResponse response) {
		ModelAndView model=new ModelAndView("redirect:/showdiscount.htm");
		// checking for user choice is Update or Adding new discount
		String operationType = request.getParameter("submit");
		if (operationType.equals("Update")) {	// If user wants to update existing discount
			if(!_discountService.updateDiscount(discountBean)) {
				model.addObject("error", "This Discount Can't Update");
			}
			return model;
		} else {
			if (!_discountService.addDiscount(discountBean)) {
				model.addObject("error", "Cant Add new Discount");
			}
			return model;
		}
	}

}
