package com.craterzone.ca.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.craterzone.ca.model.RegisterationBean;
import com.craterzone.ca.service.ReportService;

@Controller
public class AdminDashBoardController {

	@Autowired
	private ReportService _reportService;
	
	@RequestMapping("/adminDashboard.htm")
	public ModelAndView getDashBoardData(HttpServletRequest request) {
		
		ModelAndView model = new ModelAndView();
		if(!((String)request.getSession(false).getAttribute("username")).equalsIgnoreCase("admin")) {
			
			model.addObject("error","You are not authorized to see this page");
			model.setViewName("admin_report");
			return model;
		}
		
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		Date dateTo = new Date();
		
		Calendar cal = Calendar.getInstance();
		int i = cal.get(Calendar.DAY_OF_WEEK) - cal.getFirstDayOfWeek();
		cal.add(Calendar.DATE, -i - 6);
		Date dateFrom = cal.getTime();
		Map<String,List<RegisterationBean>> map=_reportService.getAdminReport(dateFormat.format(dateFrom), dateFormat.format(dateTo));
		model.addObject("adminReport", map);
		model.setViewName("admin_report");
		return model;
		
	}
}
