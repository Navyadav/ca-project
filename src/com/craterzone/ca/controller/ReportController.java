package com.craterzone.ca.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.craterzone.ca.common.util.FilePathImpl;
import com.craterzone.ca.model.RegisterationBean;
import com.craterzone.ca.service.NewUserServices;
import com.craterzone.ca.service.ReportService;

@Controller
public class ReportController implements FilePathImpl {
	
	final static Logger _logger = Logger.getLogger(ReportController.class.getName());
	
	@Autowired
	private NewUserServices newUserService;
	
	@Autowired
	private ReportService _reportService;
	
	@RequestMapping(value="/download", method=RequestMethod.GET)
	public void downloadExcel(@RequestParam("userId") String userId,HttpServletRequest request, HttpServletResponse response, OutputStream outStream) {

		if(userId.equals("")) {
			return;
		}

		FileInputStream inputStream = null;
		try {
			_reportService.zipFolder(userId);

			_reportService.generateXls(userId);

			_reportService.zipWholeFolder(userId);

			// get absolute path of the application
			ServletContext context = request.getServletContext();
			File downloadFile = new File(ALL_ZIPS_FOLDER_PATH + SEPERATOR + userId + ".zip");
			inputStream = new FileInputStream(downloadFile);

			// get MIME type of the file
			String mimeType = context
					.getMimeType(ALL_ZIPS_FOLDER_PATH + SEPERATOR + userId + ".zip");
			if (mimeType == null) {
				// set to binary type if MIME mapping not found
				mimeType = "application/octet-stream";
			}
			_logger.log(Level.INFO, "MIME type: " + mimeType);

			// set content attributes for the response
			response.setContentType(mimeType);
			response.setContentLength((int) downloadFile.length());

			// set headers for the response
			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"",
					downloadFile.getName());
			response.setHeader(headerKey, headerValue);

			byte[] buffer = new byte[4096];
			int bytesRead = -1;

			// write bytes read from the input stream into the output stream
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}
			inputStream.close();
			outStream.close();
			
			_reportService.clean();
			
			} catch (IOException e) {
				
			}
	}
	
	@RequestMapping("/reportGenerate")
	public ModelAndView generateReport(ModelAndView mav ,HttpServletRequest request, HttpServletResponse response) {
		List<RegisterationBean> users = _reportService.getAdminReport();
		mav.addObject("registeredUsersList",users);
		mav.setViewName("reports");
		return mav;
	}
 

	@RequestMapping("/downloadAll")
	public void downloadAll(@RequestParam("ids") String ids,ModelAndView mav, HttpServletRequest request, HttpServletResponse response, OutputStream outStream) {
		
		if(ids.equals("")) {
			return;
		}
		
		_reportService.downloadAll(ids);
		
		
		// Code for download 
		FileInputStream inputStream = null;
		try {

			// get absolute path of the application
			ServletContext context = request.getServletContext();
			File downloadFile = new File(PROJECT_FOLDER_PATH + SEPERATOR + "allZips" + ".zip");
			inputStream = new FileInputStream(downloadFile);

			// get MIME type of the file
			String mimeType = context
					.getMimeType(PROJECT_FOLDER_PATH + SEPERATOR + "allZips" + ".zip");
			if (mimeType == null) {
				// set to binary type if MIME mapping not found
				mimeType = "application/octet-stream";
			}
			_logger.log(Level.INFO, "MIME type: " + mimeType);

			// set content attributes for the response
			response.setContentType(mimeType);
			response.setContentLength((int) downloadFile.length());

			// set headers for the response
			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"",
					downloadFile.getName());
			response.setHeader(headerKey, headerValue);

			byte[] buffer = new byte[4096];
			int bytesRead = -1;

			// write bytes read from the input stream into the output stream
			while ((bytesRead = inputStream.read(buffer)) != -1) {
					outStream.write(buffer, 0, bytesRead);
				}
			inputStream.close();
			outStream.close();
			_reportService.clean();
			
			} catch (IOException e) {
				_logger.log(Level.SEVERE, "Unable to process downloadAll request" + e);
			} finally {
				_logger.log(Level.INFO, "Download Success");
		}
		
	}
	
	@RequestMapping("/search") 
	public ModelAndView search(@RequestParam("dateFrom") String dateFrom, @RequestParam("dateTo") String dateTo, @RequestParam("keyword") String keyword, @RequestParam("searchBy") String searchBy, ModelAndView mav) {
		
		if(dateFrom == null || dateTo == null || keyword == null) {
			mav.addObject("error","Search keywords required");
			mav.setViewName("reports");
			return mav;
		}
		
		List<RegisterationBean> searchResult = _reportService.search(dateFrom, dateTo, keyword, searchBy);
		mav.addObject("registeredUsersList",searchResult);
		mav.setViewName("reports");
		return mav;
	}
	
	@RequestMapping("/userDetails")
	public ModelAndView getDetails(@RequestParam("userId") String userId) {
		RegisterationBean userData=_reportService.getAdminReport(userId);
		ModelAndView model=new ModelAndView();
		model.addObject("userData",userData);
		model.setViewName("details");
		return model;
	}
	@RequestMapping("/userDetailsPrint")
	public ModelAndView getDetailsPrint(@RequestParam("userId") String userId) {
		RegisterationBean userData=_reportService.getAdminReport(userId);
		ModelAndView model=new ModelAndView();
		model.addObject("userData",userData);
		model.setViewName("detailsPrint");
		return model;
	}
	
	
	@RequestMapping("/faqs")
	public String getFaqsPage() {
		return "faqs";
	}
	
	@RequestMapping("/itrFiling")
	public String getitrFilingPage() {
		return "itrFiling";
	}
	
	@RequestMapping("/privacyPolicy")
	public String getPrivacyPolicyPage() {
		return "privacypolicy";
	}
	
	@RequestMapping("/tandc")
	public String getTermsAndConditionsPage() {
		return "terms";
	}
	
	@RequestMapping("/contactus")
	public String getContactUsPage() {
		return "contactus";
	}
}
