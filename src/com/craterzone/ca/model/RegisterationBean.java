package com.craterzone.ca.model;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

public class RegisterationBean {

	private String userId;
	private String fUserName;
	private String mUserName;
	private String lUserName;
	private String fFatherName;
	private String mFatherName;
	private String lFatherName;
	private Date DOB;
	private char[] panNumber;
	private String gender;
	private long mobileNumber;
	private String email;
	private String address1;
	private String address2;
	private String city;
	private String State;
	private int pinCode;
	private long accountNumber;
	private String ifsc;
	private String bankAddress;
	private int assessmentYear;
	private double bankInterest;
	private String remark;
	private String bankName;
	private ArrayList<MultipartFile> files;
	private ArrayList<String> passFiles;
	private double income;
	private Date dateTime;
	private String transactionId;
	private String status;
	private double amount;

	public RegisterationBean() {

	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getfUserName() {
		return fUserName;
	}

	public void setfUserName(String fUserName) {
		this.fUserName = fUserName;
	}

	public String getmUserName() {
		return mUserName;
	}

	public void setmUserName(String mUserName) {
		this.mUserName = mUserName;
	}

	public String getlUserName() {
		return lUserName;
	}

	public void setlUserName(String lUserName) {
		this.lUserName = lUserName;
	}

	public String getfFatherName() {
		return fFatherName;
	}

	public void setfFatherName(String fFatherName) {
		this.fFatherName = fFatherName;
	}

	public String getmFatherName() {
		return mFatherName;
	}

	public void setmFatherName(String mFatherName) {
		this.mFatherName = mFatherName;
	}

	public String getlFatherName() {
		return lFatherName;
	}

	public void setlFatherName(String lFatherName) {
		this.lFatherName = lFatherName;
	}

	public Date getDOB() {
		return DOB;
	}

	public void setDOB(Date dOB) {
		DOB = dOB;
	}

	public char[] getPanNumber() {
		return panNumber;
	}
	
	public void setPanNumber(char[] panNumber) {
		this.panNumber = panNumber;
	}

	public long getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(long mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress1() {
		return address1;
	}


	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}

	public int getPinCode() {
		return pinCode;
	}

	public void setPinCode(int pinCode) {
		this.pinCode = pinCode;
	}

	public String getIfsc() {
		return ifsc;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public String getBankAddress() {
		return bankAddress;
	}

	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	public double getIncome() {
		return income;
	}

	public void setIncome(double income) {
		this.income = income;
	}

	public ArrayList<MultipartFile> getFiles() {
		return files;
	}

	public void setFiles(ArrayList<MultipartFile> files) {
		this.files = files;
	}

	public int getAssessmentYear() {
		return assessmentYear;
	}

	public void setAssessmentYear(int assessmentYear) {
		this.assessmentYear = assessmentYear;
	}

	public double getBankInterest() {
		return bankInterest;
	}

	public void setBankInterest(double bankInterest) {
		this.bankInterest = bankInterest;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public ArrayList<String> getPassFiles() {
		return passFiles;
	}

	public void setPassFiles(ArrayList<String> passFiles) {
		this.passFiles = passFiles;
	}
	

}
