package com.craterzone.ca.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

import com.craterzone.ca.common.util.ConnectionHelper;
import com.craterzone.ca.common.util.FilePathImpl;
import com.craterzone.ca.enums.SqlQueries;
import com.craterzone.ca.enums.Tables;
import com.craterzone.ca.model.RegisterationBean;

public class RegisterUserDao implements FilePathImpl {

	@Autowired
	private DataSource _dataSource;
	
	final static Logger _logger = Logger.getLogger(RegisterUserDao.class.getName());

	public boolean isUserExists(String email) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			StringBuilder query = new StringBuilder("SELECT ");
			query.append(Tables.RegisterUser.COLUMN_firstName.getLabel());
			query.append(" from ");
			query.append(Tables.REGISTER_USER.getLabel());
			query.append(" where ");
			query.append(Tables.RegisterUser.COLUMN_emailId.getLabel());
			query.append("=?");
			con = _dataSource.getConnection();
			ps = con.prepareStatement(query.toString());
			ps.setString(1, email);
			rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}
		} catch (Exception e) {
			_logger.log(Level.SEVERE, "Unable to execute isUserExists Statement " + e);
		} finally {
			ConnectionHelper.close(rs);
			ConnectionHelper.close(ps);
			ConnectionHelper.close(con);
		}
		return false;
	}

	public int userInsert(RegisterationBean basicRegisteration,	String newUserId) {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			
			con = _dataSource.getConnection();
			con.setAutoCommit(false);
			ps = con.prepareStatement(SqlQueries.getInsertUserQuery()
					.toString());
			ps.setString(1, newUserId);
			ps.setString(2, basicRegisteration.getfUserName());
			ps.setString(3, basicRegisteration.getmUserName());
			ps.setString(4, basicRegisteration.getlUserName());
			ps.setString(5, basicRegisteration.getfFatherName());
			ps.setString(6, basicRegisteration.getmFatherName());
			ps.setString(7, basicRegisteration.getlFatherName());
			ps.setDate(8, new java.sql.Date(basicRegisteration.getDOB().getTime()));			
			ps.setString(9, String.valueOf(basicRegisteration.getPanNumber()));
			ps.setString(10, basicRegisteration.getGender());
			ps.setLong(11, basicRegisteration.getMobileNumber());
			ps.setString(12, basicRegisteration.getEmail());
			ps.setString(13, basicRegisteration.getAddress1());
			ps.setString(14, basicRegisteration.getAddress2());
			ps.setString(15, basicRegisteration.getCity());
			ps.setString(16, basicRegisteration.getState());
			ps.setInt(17, basicRegisteration.getPinCode());
			ps.setLong(18, basicRegisteration.getAccountNumber());
			ps.setString(19, basicRegisteration.getIfsc());
			ps.setString(20, basicRegisteration.getBankName());
			ps.setString(21, basicRegisteration.getBankAddress());
			ps.setInt(22, basicRegisteration.getAssessmentYear());
			ps.setDouble(23, basicRegisteration.getBankInterest());
			ps.setString(24, basicRegisteration.getRemark());
			ps.setTimestamp(25, new java.sql.Timestamp(new java.util.Date().getTime()));
			int count = ps.executeUpdate();
			con.commit();
			return count;
		} catch (Exception e) {
			_logger.log(Level.SEVERE, "Unable to execute userInsert Statement " + e);
		} finally {
			ConnectionHelper.close(ps);
			ConnectionHelper.rollback(con);
			ConnectionHelper.close(con);
		}
		return 0;
	}
	
	public boolean registeredUserUpdate(RegisterationBean basicRegisteration,int status) {
		String query = SqlQueries.updateRegisteredUserStatus().toString();
		query=query+basicRegisteration.getUserId()+"'";
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = _dataSource.getConnection();
			con.setAutoCommit(false);
			ps = con.prepareStatement(query);
			ps.setString(1,basicRegisteration.getTransactionId());
			ps.setDouble(2, basicRegisteration.getAmount());
			ps.setInt(3, status);
			if (ps.executeUpdate() > 0) {
				
				// File path insertion in Data Base
				
				if(status==1) {
					ps = con.prepareStatement(SqlQueries.getInsertFileQuery()
							.toString());			
					ps.setString(1, basicRegisteration.getUserId());
					int index = 2;

					for (int i = 0; i < basicRegisteration.getFiles().size(); i++) {
						String path = UPLOAD_FOLDER_PATH
								+ basicRegisteration.getUserId()
								+ "/"
								+ basicRegisteration.getFiles().get(i)
										.getOriginalFilename();
						if (basicRegisteration.getFiles().get(i).getSize() != 0) {
							ps.setString(index++, path);
							ps.setString(index++, basicRegisteration.getPassFiles().get(i));
						} else {
							ps.setString(index++, "");
							ps.setString(index++, "");
						}
					}
					ps.setDouble(34, basicRegisteration.getIncome());
					ps.executeUpdate();
					con.commit();
					return true;
				}
			}
		} catch (Exception e) {
			_logger.log(Level.SEVERE, "Unable to execute registeredUserUpdate Statement " + e);
		} finally {
			ConnectionHelper.close(ps);
			ConnectionHelper.close(con);
		}
		return false;
	}
	
	public boolean isStaffExist(String uname) {
		String query=SqlQueries.staffExist().append(uname+"'").toString();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = _dataSource.getConnection();
			ps = con.prepareStatement(query);
			rs = ps.executeQuery(query);
			while (rs.next()) {
				if(rs.getInt(1) > 0) {
					return false;
				}
			}
		} catch (Exception e) {
			_logger.log(Level.SEVERE, "Unable to execute isStaffExist Statement " + e);
		} finally {
			ConnectionHelper.close(rs);
			ConnectionHelper.close(ps);
			ConnectionHelper.close(con);
		}
		return true;
	}

	public boolean staffRegister(String uname,String password) {
		
		String query=SqlQueries.addStaff().toString();
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = _dataSource.getConnection();
			ps = con.prepareStatement(query);
			ps.setInt(1, 0);
			ps.setString(2, uname);
			ps.setString(3, password);
			ps.setString(4, "staff");
			if (ps.executeUpdate() > 0) {
				return true;
			}
		} catch (Exception e) {
			_logger.log(Level.SEVERE, "Unable to execute isStaffExist Statement " + e);
		} finally {
			ConnectionHelper.close(ps);
			ConnectionHelper.close(con);
		}
		return false;
	}
}
