package com.craterzone.ca.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

import com.craterzone.ca.common.util.ConnectionHelper;
import com.craterzone.ca.enums.Tables;
import com.craterzone.ca.model.LoginUser;

public class UserLoginDao {

	@Autowired
	private DataSource _dataSource;

	final static Logger _logger = Logger.getLogger(UserLoginDao.class.getName());

	public boolean isValidUser(LoginUser loginUser) {

		if (loginUser == null) {
			return false;
		}

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = _dataSource.getConnection();
			StringBuilder query = new StringBuilder("select id from ");
			query.append(Tables.USER_LOGIN.getLabel());
			query.append(" ");
			query.append("where ");
			query.append(Tables.UserLogin.COLUMN_userName.getLabel());
			query.append("=");
			query.append("'" + loginUser.getUsername() + "' AND ");
			query.append(Tables.UserLogin.COLUMN_password.getLabel());
			query.append("=");
			query.append("'" + loginUser.getPassword() + "'");

			ps = con.prepareStatement(query.toString());
			rs = ps.executeQuery();

			if (rs.next()) {
				return true;
			}
			
		} catch (Exception e) {
			_logger.log(Level.SEVERE, "Unable to Close Statement " + e);
		} finally {
			ConnectionHelper.close(ps);
			ConnectionHelper.close(con);
		}
		return false;
	}

}
