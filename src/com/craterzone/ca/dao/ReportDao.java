package com.craterzone.ca.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

import com.craterzone.ca.common.util.ConnectionHelper;
import com.craterzone.ca.enums.Tables;
import com.craterzone.ca.model.RegisterationBean;

public class ReportDao {
	
	@Autowired
	private DataSource _dataSource;
	
	final static Logger _logger = Logger.getLogger(ReportDao.class.getName());

	/** Fetches data for a particular user from DB 
	 * 
	 * @param userId
	 * @return RegisterationBean
	 */
	
	public RegisterationBean getUserData(String userId) {
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = _dataSource.getConnection();
			StringBuilder query = new StringBuilder("SELECT * from ");
			query.append(Tables.REGISTER_USER.getLabel() );
			query.append(" where ");
			query.append(Tables.RegisterUser.COLUMN_registerUserId.getLabel() + "=");
			query.append("'"+userId+"'");
			query.append(" and ");
			query.append(Tables.RegisterUser.COLUMN_status.getLabel());
			query.append("=?");
			ps = con.prepareStatement(query.toString());
			ps.setString(1, "paid");
			rs = ps.executeQuery();
			
			RegisterationBean bean = new RegisterationBean();
			while(rs.next()) {
				
				bean.setUserId(rs.getString(Tables.RegisterUser.COLUMN_registerUserId.getLabel()));
				bean.setfUserName(rs.getString(Tables.RegisterUser.COLUMN_firstName.getLabel()));
				bean.setmUserName(rs.getString(Tables.RegisterUser.COLUMN_middleName.getLabel()));
				bean.setlUserName(rs.getString(Tables.RegisterUser.COLUMN_lastName.getLabel()));
				bean.setfFatherName(rs.getString(Tables.RegisterUser.COLUMN_fatherFirstName.getLabel()));
				bean.setmFatherName(rs.getString(Tables.RegisterUser.COLUMN_father_middle_name.getLabel()));
				bean.setlFatherName(rs.getString(Tables.RegisterUser.COLUMN_father_last_name.getLabel()));
				bean.setDOB(rs.getDate(Tables.RegisterUser.COLUMN_dob.getLabel()));
				bean.setPanNumber((rs.getString(Tables.RegisterUser.COLUMN_pan.getLabel())).toCharArray());
				bean.setGender(rs.getString(Tables.RegisterUser.COLUMN_gender.getLabel()));
				bean.setMobileNumber(rs.getLong(Tables.RegisterUser.COLUMN_mobileNumber.getLabel()));
				bean.setEmail(rs.getString(Tables.RegisterUser.COLUMN_emailId.getLabel()));
				bean.setAddress1(rs.getString(Tables.RegisterUser.COLUMN_addressLine1.getLabel()));
				bean.setAddress2(rs.getString(Tables.RegisterUser.COLUMN_addressLine2.getLabel()));
				bean.setCity(rs.getString(Tables.RegisterUser.COLUMN_city.getLabel()));
				bean.setState(rs.getString(Tables.RegisterUser.COLUMN_state.getLabel()));
				bean.setPinCode(rs.getInt(Tables.RegisterUser.COLUMN_pin.getLabel()));
				bean.setAccountNumber(rs.getLong(Tables.RegisterUser.COLUMN_accountNumber.getLabel()));
				bean.setIfsc(rs.getString(Tables.RegisterUser.COLUMN_ifscCode.getLabel()));
				bean.setBankName(rs.getString(Tables.RegisterUser.COLUMN_bankName.getLabel()));
				bean.setBankAddress(rs.getString(Tables.RegisterUser.COLUMN_bankAddress.getLabel()));
				bean.setAssessmentYear(rs.getInt(Tables.RegisterUser.COLUMN_assessmentYear.getLabel()));
				bean.setBankInterest(rs.getDouble(Tables.RegisterUser.COLUMN_bankInterest.getLabel()));
				bean.setAmount(rs.getDouble(Tables.RegisterUser.COLUMN_amount.getLabel()));
				bean.setRemark(rs.getString(Tables.RegisterUser.COLUMN_remark.getLabel()));
				
			}
			return bean;
			
		} catch (Exception e) {
			_logger.log(Level.SEVERE, "Unable to execute getUserDate Statement " + e);
		} finally {
			ConnectionHelper.close(rs);
			ConnectionHelper.close(con);
			ConnectionHelper.close(ps);
		}
		return null;
	}
	
	/** Fetches all DB entries from register_user table for some ids
	 * 
	 * @param ids - String
	 * @return ArrayList of RegisterationBean type 
	 */
	
	public List<RegisterationBean> getAllUserData(String ids) {
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = _dataSource.getConnection();
			StringBuilder query = new StringBuilder("SELECT * from ");
			query.append(Tables.REGISTER_USER.getLabel());
			query.append(" WHERE ");
			query.append(Tables.RegisterUser.COLUMN_registerUserId.getLabel());
			query.append(" in ");
			query.append("(");
			for(String temp:ids.split(",")){
				query.append("'");
				query.append(temp);
				query.append("'");
				query.append(",");
			}
			query.setCharAt(query.length()-1,' ');	
			query.append(")");
			query.append(" and ");
			query.append(Tables.RegisterUser.COLUMN_status.getLabel());
			query.append("=?");
			
			ps = con.prepareStatement(query.toString());
			ps.setString(1, "paid");
			rs = ps.executeQuery();
			
			List<RegisterationBean> listOfAllUsers = new ArrayList<RegisterationBean>();
			
			while(rs.next()) {
				RegisterationBean bean = new RegisterationBean();
				bean.setUserId(rs.getString(Tables.RegisterUser.COLUMN_registerUserId.getLabel()));
				bean.setfUserName(rs.getString(Tables.RegisterUser.COLUMN_firstName.getLabel()));
				bean.setmUserName(rs.getString(Tables.RegisterUser.COLUMN_middleName.getLabel()));
				bean.setlUserName(rs.getString(Tables.RegisterUser.COLUMN_lastName.getLabel()));
				bean.setfFatherName(rs.getString(Tables.RegisterUser.COLUMN_fatherFirstName.getLabel()));
				bean.setmFatherName(rs.getString(Tables.RegisterUser.COLUMN_father_middle_name.getLabel()));
				bean.setlFatherName(rs.getString(Tables.RegisterUser.COLUMN_father_last_name.getLabel()));
				bean.setDOB(rs.getDate(Tables.RegisterUser.COLUMN_dob.getLabel()));
				bean.setPanNumber((rs.getString(Tables.RegisterUser.COLUMN_pan.getLabel())).toCharArray());
				bean.setGender(rs.getString(Tables.RegisterUser.COLUMN_gender.getLabel()));
				bean.setMobileNumber(rs.getLong(Tables.RegisterUser.COLUMN_mobileNumber.getLabel()));
				bean.setEmail(rs.getString(Tables.RegisterUser.COLUMN_emailId.getLabel()));
				bean.setAddress1(rs.getString(Tables.RegisterUser.COLUMN_addressLine1.getLabel()));
				bean.setAddress2(rs.getString(Tables.RegisterUser.COLUMN_addressLine2.getLabel()));
				bean.setCity(rs.getString(Tables.RegisterUser.COLUMN_city.getLabel()));
				bean.setState(rs.getString(Tables.RegisterUser.COLUMN_state.getLabel()));
				bean.setPinCode(rs.getInt(Tables.RegisterUser.COLUMN_pin.getLabel()));
				bean.setAccountNumber(rs.getLong(Tables.RegisterUser.COLUMN_accountNumber.getLabel()));
				bean.setIfsc(rs.getString(Tables.RegisterUser.COLUMN_ifscCode.getLabel()));
				bean.setBankName(rs.getString(Tables.RegisterUser.COLUMN_bankName.getLabel()));
				bean.setBankAddress(rs.getString(Tables.RegisterUser.COLUMN_bankAddress.getLabel()));
				bean.setAssessmentYear(rs.getInt(Tables.RegisterUser.COLUMN_assessmentYear.getLabel()));
				bean.setBankInterest(rs.getDouble(Tables.RegisterUser.COLUMN_bankInterest.getLabel()));
				bean.setDateTime(new java.util.Date(rs.getTimestamp(Tables.RegisterUser.COLUMN_dateTime.getLabel()).getTime()));
				bean.setRemark(rs.getString(Tables.RegisterUser.COLUMN_remark.getLabel()));
				bean.setAmount(rs.getDouble(Tables.RegisterUser.COLUMN_amount.getLabel()));
			
				listOfAllUsers.add(bean);
			}
			return listOfAllUsers;
			
		} catch (Exception e) {
			_logger.log(Level.SEVERE, "Unable to execute getAllUserData Statement " + e);
		} finally {
			ConnectionHelper.close(rs);
			ConnectionHelper.close(con);
			ConnectionHelper.close(ps);
		}
		return null;
	}

	/** Fetches all DB entries from register_user table
	 * 
	 * @return ArrayList of RegisterationBean type 
	 */
	
	public List<RegisterationBean> getAllUserData() {
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = _dataSource.getConnection();
			StringBuilder query = new StringBuilder("SELECT * from ");
			query.append(Tables.REGISTER_USER.getLabel());
			query.append(" WHERE ");
			query.append(Tables.RegisterUser.COLUMN_status.getLabel());
			query.append("=?");
			ps = con.prepareStatement(query.toString());
			ps.setString(1, "paid");
			rs = ps.executeQuery();
			
			List<RegisterationBean> listOfAllUsers = new ArrayList<RegisterationBean>();
			
			while(rs.next()) {
				RegisterationBean bean = new RegisterationBean();
				bean.setUserId(rs.getString(Tables.RegisterUser.COLUMN_registerUserId.getLabel()));
				bean.setfUserName(rs.getString(Tables.RegisterUser.COLUMN_firstName.getLabel()));
				bean.setmUserName(rs.getString(Tables.RegisterUser.COLUMN_middleName.getLabel()));
				bean.setlUserName(rs.getString(Tables.RegisterUser.COLUMN_lastName.getLabel()));
				bean.setfFatherName(rs.getString(Tables.RegisterUser.COLUMN_fatherFirstName.getLabel()));
				bean.setmFatherName(rs.getString(Tables.RegisterUser.COLUMN_father_middle_name.getLabel()));
				bean.setlFatherName(rs.getString(Tables.RegisterUser.COLUMN_father_last_name.getLabel()));
				bean.setDOB(rs.getDate(Tables.RegisterUser.COLUMN_dob.getLabel()));
				bean.setPanNumber((rs.getString(Tables.RegisterUser.COLUMN_pan.getLabel())).toCharArray());
				bean.setGender(rs.getString(Tables.RegisterUser.COLUMN_gender.getLabel()));
				bean.setMobileNumber(rs.getLong(Tables.RegisterUser.COLUMN_mobileNumber.getLabel()));
				bean.setEmail(rs.getString(Tables.RegisterUser.COLUMN_emailId.getLabel()));
				bean.setAddress1(rs.getString(Tables.RegisterUser.COLUMN_addressLine1.getLabel()));
				bean.setAddress2(rs.getString(Tables.RegisterUser.COLUMN_addressLine2.getLabel()));
				bean.setCity(rs.getString(Tables.RegisterUser.COLUMN_city.getLabel()));
				bean.setState(rs.getString(Tables.RegisterUser.COLUMN_state.getLabel()));
				bean.setPinCode(rs.getInt(Tables.RegisterUser.COLUMN_pin.getLabel()));
				bean.setAccountNumber(rs.getLong(Tables.RegisterUser.COLUMN_accountNumber.getLabel()));
				bean.setIfsc(rs.getString(Tables.RegisterUser.COLUMN_ifscCode.getLabel()));
				bean.setBankName(rs.getString(Tables.RegisterUser.COLUMN_bankName.getLabel()));
				bean.setBankAddress(rs.getString(Tables.RegisterUser.COLUMN_bankAddress.getLabel()));
				bean.setAssessmentYear(rs.getInt(Tables.RegisterUser.COLUMN_assessmentYear.getLabel()));
				bean.setBankInterest(rs.getDouble(Tables.RegisterUser.COLUMN_bankInterest.getLabel()));
				bean.setDateTime(new java.util.Date(rs.getTimestamp(Tables.RegisterUser.COLUMN_dateTime.getLabel()).getTime()));
				bean.setTransactionId(rs.getString(Tables.RegisterUser.COLUMN_transactionId.getLabel()));
				bean.setRemark(rs.getString(Tables.RegisterUser.COLUMN_remark.getLabel()));
				bean.setAmount(rs.getDouble(Tables.RegisterUser.COLUMN_amount.getLabel()));
				listOfAllUsers.add(bean);
			}
			return listOfAllUsers;
			
		} catch (Exception e) {
			_logger.log(Level.SEVERE, "Unable to execute getAllUserData Statement " + e);
		} finally {
			ConnectionHelper.close(rs);
			ConnectionHelper.close(con);
			ConnectionHelper.close(ps);
		}
		return null;
	}
	
	/**
	 * Searches the data in DB table using required parameters
	 * 
	 * @param dateFrom
	 * @param dateTo
	 * @param keyword
	 * @param searchBy
	 * @return ArrayList of RegisterationBean type
	 */

	public List<RegisterationBean> search(Date dateFrom, Date dateTo, String keyword, String searchBy) {
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			boolean dateFromFlag = false;
			StringBuilder query = new StringBuilder("SELECT * FROM ");
			query.append(Tables.REGISTER_USER.getLabel());
			query.append(" WHERE ");
			if(dateFrom!=null) {
				query.append(Tables.RegisterUser.COLUMN_dateTime.getLabel());
				query.append(" >= ");
				query.append("'" + new java.sql.Timestamp(dateFrom.getTime()) + "'");
				dateFromFlag = true;
			}
			
			if(dateTo!=null) {
				if(dateFromFlag){
				   query.append(" AND ");
				}
				query.append(Tables.RegisterUser.COLUMN_dateTime.getLabel());
				query.append(" <= ");
				query.append("'" + new java.sql.Timestamp(dateTo.getTime()) + "'");
				dateFromFlag = true;
			}
			
			if(searchBy.equals("pan")) {
				if(!keyword.equals("")) {
					if(dateFromFlag){
						   query.append(" AND ");
						}
					query.append(Tables.RegisterUser.COLUMN_pan.getLabel());
					query.append(" = ");
					query.append("'" + keyword + "'");
					dateFromFlag = true;
				}
			} else if(searchBy.equals("transactionId")) {
				if(!keyword.equals("")) {
					if(dateFromFlag){
						   query.append(" AND ");
					}
					query.append(Tables.RegisterUser.COLUMN_transactionId.getLabel());
					query.append(" = ");
					query.append("'" + keyword + "'");
					dateFromFlag = true;
				}
			}
			
			if(dateFromFlag) {
				 query.append(" AND ");
			}
			query.append(" status=?");
			
			con = _dataSource.getConnection();
			ps = con.prepareStatement(query.toString());
			ps.setString(1, "paid");
			
			rs = ps.executeQuery();
			
			List<RegisterationBean> searchResultList = new ArrayList<RegisterationBean>();
			
			while(rs.next()) {
				RegisterationBean bean = new RegisterationBean();
				bean.setUserId(rs.getString(Tables.RegisterUser.COLUMN_registerUserId.getLabel()));
				bean.setfUserName(rs.getString(Tables.RegisterUser.COLUMN_firstName.getLabel()));
				bean.setmUserName(rs.getString(Tables.RegisterUser.COLUMN_middleName.getLabel()));
				bean.setlUserName(rs.getString(Tables.RegisterUser.COLUMN_lastName.getLabel()));
				bean.setfFatherName(rs.getString(Tables.RegisterUser.COLUMN_fatherFirstName.getLabel()));
				bean.setmFatherName(rs.getString(Tables.RegisterUser.COLUMN_father_middle_name.getLabel()));
				bean.setlFatherName(rs.getString(Tables.RegisterUser.COLUMN_father_last_name.getLabel()));
				bean.setDOB(rs.getDate(Tables.RegisterUser.COLUMN_dob.getLabel()));
				bean.setPanNumber((rs.getString(Tables.RegisterUser.COLUMN_pan.getLabel())).toCharArray());
				bean.setGender(rs.getString(Tables.RegisterUser.COLUMN_gender.getLabel()));
				bean.setMobileNumber(rs.getLong(Tables.RegisterUser.COLUMN_mobileNumber.getLabel()));
				bean.setEmail(rs.getString(Tables.RegisterUser.COLUMN_emailId.getLabel()));
				bean.setAddress1(rs.getString(Tables.RegisterUser.COLUMN_addressLine1.getLabel()));
				bean.setAddress2(rs.getString(Tables.RegisterUser.COLUMN_addressLine2.getLabel()));
				bean.setCity(rs.getString(Tables.RegisterUser.COLUMN_city.getLabel()));
				bean.setState(rs.getString(Tables.RegisterUser.COLUMN_state.getLabel()));
				bean.setPinCode(rs.getInt(Tables.RegisterUser.COLUMN_pin.getLabel()));
				bean.setAccountNumber(rs.getLong(Tables.RegisterUser.COLUMN_accountNumber.getLabel()));
				bean.setIfsc(rs.getString(Tables.RegisterUser.COLUMN_ifscCode.getLabel()));
				bean.setBankName(rs.getString(Tables.RegisterUser.COLUMN_bankName.getLabel()));
				bean.setBankAddress(rs.getString(Tables.RegisterUser.COLUMN_bankAddress.getLabel()));
				bean.setAssessmentYear(rs.getInt(Tables.RegisterUser.COLUMN_assessmentYear.getLabel()));
				bean.setBankInterest(rs.getDouble(Tables.RegisterUser.COLUMN_bankInterest.getLabel()));
				bean.setTransactionId(rs.getString(Tables.RegisterUser.COLUMN_transactionId.getLabel()));
				bean.setDateTime(new java.util.Date(rs.getTimestamp(Tables.RegisterUser.COLUMN_dateTime.getLabel()).getTime()));
				bean.setRemark(rs.getString(Tables.RegisterUser.COLUMN_remark.getLabel()));
				bean.setAmount(rs.getDouble(Tables.RegisterUser.COLUMN_amount.getLabel()));
				searchResultList.add(bean);
			}
			return searchResultList;
			
		} catch (Exception e) {
			_logger.log(Level.SEVERE, "Unable to execute search Statement " + e);
		} finally {
			ConnectionHelper.close(rs);
			ConnectionHelper.close(con);
			ConnectionHelper.close(ps);
		}
		return null;
	}
	
}
