package com.craterzone.ca.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

import com.craterzone.ca.common.util.ConnectionHelper;
import com.craterzone.ca.enums.SqlQueries;
import com.craterzone.ca.enums.Tables;
import com.craterzone.ca.model.TicketBean;

public class TicketDao {

	final static Logger _logger = Logger.getLogger(TicketDao.class.getName());
	
	@Autowired
	DataSource _dataSource;

	// Adding new Ticket
	public int insertTicket(TicketBean ticketBean) {
		StringBuilder query = SqlQueries.getMakingTicketQuery();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs=null;
		try {
			con = _dataSource.getConnection();
			ps = con.prepareStatement(query.toString(),Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, 0);
			ps.setString(2, ticketBean.getTransactionId());
			ps.setTimestamp(3, new java.sql.Timestamp(new java.util.Date().getTime()));
			ps.setString(4, ticketBean.getEmail());
			ps.setString(5, ticketBean.getPanNumber());
			ps.setLong(6, ticketBean.getMobile());
			ps.setString(7, ticketBean.getRemarks());
			ps.setString(8, "open");
			
			if (ps.executeUpdate() > 0) {
				rs=ps.getGeneratedKeys();
				 rs.next();
				 return rs.getInt(1);
			} else {
				return 0;
			}

		} catch (Exception e) {
			_logger.log(Level.SEVERE, "Unable to execute insertTicket in TicketDAO" + e);
		} finally {
			ConnectionHelper.close(ps);
			ConnectionHelper.close(con);
		}
		return 0;
	}

		// Putting all tickets in list and showing to user
	public ArrayList<TicketBean> getOpenTicket() {
		StringBuilder query = SqlQueries.getOpenTickets();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = _dataSource.getConnection();
			ps = con.prepareStatement(query.toString());
			rs = ps.executeQuery();
			if(rs==null) {
				return null;
			}
			ArrayList<TicketBean> openTickets=new ArrayList<TicketBean>();
			TicketBean ticketBean=null;
			
			while(rs.next()) {
				ticketBean=new TicketBean();
				ticketBean.setEmail(rs.getString(Tables.HelpTicket.COLUMN_email.getLabel()));
				ticketBean.setTransactionId(rs.getString(Tables.HelpTicket.COLUMN_transactionId.getLabel()));
				ticketBean.setMobile(rs.getLong(Tables.HelpTicket.COLUMN_mobile.getLabel()));
				ticketBean.setPanNumber(rs.getString(Tables.HelpTicket.COLUMN_pan.getLabel()));
				ticketBean.setDateTime(new java.util.Date(rs.getDate(Tables.HelpTicket.COLUMN_dateTime.getLabel()).getTime()));
				ticketBean.setRemarks(rs.getString(Tables.HelpTicket.COLUMN_remark.getLabel()));
				openTickets.add(ticketBean);
			}
			return openTickets;
		} catch (Exception e) {

			_logger.log(Level.SEVERE, "Unable to execute getOpenTicket in TicketDAO" + e);
			return null;
		} finally {
			ConnectionHelper.close(ps);
			ConnectionHelper.close(con);
			ConnectionHelper.close(rs);
		}

	}
	
		// Close the ticket after admin reply with remarks
	public int getTicketsClose(String transactionId) {
		
		StringBuilder query = SqlQueries.closeTickets();
		query.append(transactionId);
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = _dataSource.getConnection();
			ps = con.prepareStatement(query.toString());
			int count=ps.executeUpdate();
		return count;
		} catch(Exception e) {
			_logger.log(Level.SEVERE, "Unable to execute getTicketClose in TicketDAO" + e);
		} finally {
			ConnectionHelper.close(ps);
			ConnectionHelper.close(con);
		} 
		return 0;
	}
	
}
