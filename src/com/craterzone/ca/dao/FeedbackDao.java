package com.craterzone.ca.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

import com.craterzone.ca.common.util.ConnectionHelper;
import com.craterzone.ca.enums.Tables;
import com.craterzone.ca.model.Feedback;

public class FeedbackDao {
	
	@Autowired
	private DataSource _dataSource;
	
	final static Logger _logger = Logger.getLogger(FeedbackDao.class.getName());
	
	public int addFeedback(Feedback feedback) {
		
		if(feedback == null) {
			return 0;
		}
		
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = _dataSource.getConnection();
			con.setAutoCommit(false);
			StringBuilder query = new StringBuilder("insert into " + Tables.FEEDBACK.getLabel() + "(");
			
			query.append(Tables.FeedBack.COLUMN_dateTime.getLabel());
			query.append(",");
			query.append(Tables.FeedBack.COLUMN_feedbackId.getLabel());
			query.append(",");
			query.append(Tables.FeedBack.COLUMN_feedBackMessage.getLabel());
			query.append(",");
			query.append(Tables.FeedBack.COLUMN_imgUrl.getLabel());
			query.append(",");
			query.append(Tables.FeedBack.COLUMN_logoUrl.getLabel());
			query.append(",");
			query.append(Tables.FeedBack.COLUMN_priority.getLabel());
			query.append(")");
			
			query.append(" values(?,?,?,?,?,?)");
			ps = con.prepareStatement(query.toString());
			ps.setTimestamp(1, new java.sql.Timestamp(new Date().getTime()));
			ps.setInt(2, 0);
			ps.setString(3, feedback.getFeedbackMessage());
			ps.setString(4, feedback.getImgUrl());
			ps.setString(5, feedback.getLogoUrl());
			ps.setInt(6, getLowestPriority() + 1);
			
			int recordsUpdated = ps.executeUpdate();
			con.commit();
			return recordsUpdated;
			
		} catch (Exception e) {
			_logger.log(Level.SEVERE, "Unable to execute addFeedback Statement " + e);
		} finally {
			ConnectionHelper.close(ps);
			ConnectionHelper.close(con);
		}
		
		return 0;
	}
	
	public int addPreferredFeedback(Feedback feedback) {
		
		if(feedback == null) {
			return 0;
		}
		
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = _dataSource.getConnection();
			con.setAutoCommit(false);
			
			// Updating all priority values
			StringBuilder query = new StringBuilder("update ");
			query.append(Tables.FEEDBACK.getLabel());
			query.append(" SET ");
			query.append(Tables.FeedBack.COLUMN_priority.getLabel());
			query.append("=");
			query.append(Tables.FeedBack.COLUMN_priority.getLabel());
			query.append("+");
			query.append("1");
			ps = con.prepareStatement(query.toString());
			ps.executeUpdate();
			
			// Inserting the feedback with priority 1 (highest priority)
			StringBuilder query1 = new StringBuilder("insert into " + Tables.FEEDBACK.getLabel() + "(");
			
			query1.append(Tables.FeedBack.COLUMN_dateTime.getLabel());
			query1.append(",");
			query1.append(Tables.FeedBack.COLUMN_feedbackId.getLabel());
			query1.append(",");
			query1.append(Tables.FeedBack.COLUMN_feedBackMessage.getLabel());
			query1.append(",");
			query1.append(Tables.FeedBack.COLUMN_imgUrl.getLabel());
			query1.append(",");
			query1.append(Tables.FeedBack.COLUMN_logoUrl.getLabel());
			query1.append(",");
			query1.append(Tables.FeedBack.COLUMN_priority.getLabel());
			query1.append(")");
			
			query1.append(" values(?,?,?,?,?,?)");
			ps = con.prepareStatement(query1.toString());
			ps.setTimestamp(1, new java.sql.Timestamp(new Date().getTime()));
			ps.setInt(2, 0);
			ps.setString(3, feedback.getFeedbackMessage());
			ps.setString(4, feedback.getImgUrl());
			ps.setString(5, feedback.getLogoUrl());
			ps.setInt(6, 1);
			
			int recordsUpdated = ps.executeUpdate();
			con.commit();
			return recordsUpdated;
			
		} catch (Exception e) {
			_logger.log(Level.SEVERE, "Unable to execute addPreferredFeedback Statement " + e);
		} finally {
			ConnectionHelper.close(ps);
			ConnectionHelper.close(con);
		}
		return 0;
	}
	
	public Feedback getFeedback(int feedbackId) {
		
		if(feedbackId <= 0) {
			return null;
		}
		
		Connection con = null;
		PreparedStatement ps =null;
		ResultSet rs = null;
		try {
			
			String query = "select * from " + Tables.FEEDBACK.getLabel() + " where " + Tables.FeedBack.COLUMN_feedbackId.getLabel() + 
					"=" + feedbackId;
			con = _dataSource.getConnection();
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			
			Feedback feedback = new Feedback();
			while(rs.next()) {
				feedback.setDateTime(new Date(rs.getDate(
						Tables.FeedBack.COLUMN_dateTime.getLabel()).getTime()));
				feedback.setFeedbackId(rs
						.getInt(Tables.FeedBack.COLUMN_feedbackId.getLabel()));
				feedback.setFeedbackMessage(rs
						.getString(Tables.FeedBack.COLUMN_feedBackMessage
								.getLabel()));
				feedback.setImgUrl(rs.getString(Tables.FeedBack.COLUMN_imgUrl
						.getLabel()));
				feedback.setLogoUrl(rs.getString(Tables.FeedBack.COLUMN_logoUrl
						.getLabel()));
				feedback.setPriority(rs.getInt(Tables.FeedBack.COLUMN_priority
						.getLabel()));
			}
			return feedback;
			
		} catch (Exception e) {
			_logger.log(Level.SEVERE, "Unable to execute getFeedback Statement " + e);
			return null;
		} finally {
			ConnectionHelper.close(rs);
			ConnectionHelper.close(ps);
			ConnectionHelper.close(con);
		}
		
	}

	public int deleteFeedback(int feedbackId) {
		
		if(feedbackId <= 0) {
			return 0;
		}
		
		Connection con = null;
		PreparedStatement ps =null;
		
		String query = "delete from " + Tables.FEEDBACK.getLabel() + " where " + Tables.FeedBack.COLUMN_feedbackId.getLabel() + 
				"=" + feedbackId;
		try {
			con = _dataSource.getConnection();
			ps = con.prepareStatement(query);
			int status = ps.executeUpdate();
			return status;
		} catch (SQLException e) {
			_logger.log(Level.SEVERE, "Unable to execute deleteFeedback Statement " + e);
		} finally {
			ConnectionHelper.close(ps);
			ConnectionHelper.close(con);
		}
		return 0;
	}
	
	// Returning all feedbacks in an array list
	public List<Feedback> getAllFeedbacks() {
		
		Connection con = null;
		PreparedStatement ps =null;
		ResultSet rs = null;
		List<Feedback> feedbackList = null;
		try {
			
			StringBuilder query = new StringBuilder("select * from ");
			query.append(Tables.FEEDBACK.getLabel());
			query.append(" ORDER BY ");
			query.append(Tables.FeedBack.COLUMN_priority.getLabel());
			
			con = _dataSource.getConnection();
			ps = con.prepareStatement(query.toString());
			rs = ps.executeQuery();
			feedbackList = new ArrayList<Feedback>();
			
			while(rs.next()) {
				Feedback feedback = new Feedback();
				feedback.setDateTime(new Date(rs.getDate(Tables.FeedBack.COLUMN_dateTime.getLabel()).getTime()));
				feedback.setFeedbackId(rs.getInt(Tables.FeedBack.COLUMN_feedbackId.getLabel()));
				feedback.setFeedbackMessage(rs.getString(Tables.FeedBack.COLUMN_feedBackMessage.getLabel()));
				feedback.setImgUrl(rs.getString(Tables.FeedBack.COLUMN_imgUrl.getLabel()));
				feedback.setLogoUrl(rs.getString(Tables.FeedBack.COLUMN_logoUrl.getLabel()));
				feedback.setPriority(rs.getInt(Tables.FeedBack.COLUMN_priority.getLabel()));
				feedbackList.add(feedback);
			}
			return feedbackList;
			
		} catch (Exception e) {
			_logger.log(Level.SEVERE, "Unable to execute getAllFeedbacks Statement " + e);
		} finally {
			ConnectionHelper.close(rs);
			ConnectionHelper.close(ps);
			ConnectionHelper.close(con);
		}
		return null;
	}
	
	private int getLowestPriority() {
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			
			StringBuilder query = new StringBuilder("select ");
			query.append("MAX(");
			query.append(Tables.FeedBack.COLUMN_priority.getLabel());
			query.append(") from ");
			query.append(Tables.FEEDBACK.getLabel());
			
			con = _dataSource.getConnection();
			ps = con.prepareStatement(query.toString());
			rs = ps.executeQuery();
			
			if(rs.next()) {
				return rs.getInt(1);
			}
			return 0;
			
		}catch (Exception e) {
			_logger.log(Level.SEVERE, "Unable to execute getLowestPriority Statement " + e);
		} finally {
			ConnectionHelper.close(rs);
			ConnectionHelper.close(ps);
			ConnectionHelper.close(con);
		}
		return 0;
	}

	public List<Feedback> getPreferredFeedbacks() {
		Connection con = null;
		PreparedStatement ps =null;
		ResultSet rs = null;
		List<Feedback> feedbackList = null;
		try {
			
			StringBuilder query = new StringBuilder("select * from ");
			query.append(Tables.FEEDBACK.getLabel());
			query.append(" ORDER BY ");
			query.append(Tables.FeedBack.COLUMN_priority.getLabel());
			query.append(" LIMIT ");
			query.append(4);
			
			con = _dataSource.getConnection();
			ps = con.prepareStatement(query.toString());
			rs = ps.executeQuery();
			feedbackList = new ArrayList<Feedback>();
			
			while(rs.next()) {
				Feedback feedback = new Feedback();
				feedback.setDateTime(new Date(rs.getDate(Tables.FeedBack.COLUMN_dateTime.getLabel()).getTime()));
				feedback.setFeedbackId(rs.getInt(Tables.FeedBack.COLUMN_feedbackId.getLabel()));
				feedback.setFeedbackMessage(rs.getString(Tables.FeedBack.COLUMN_feedBackMessage.getLabel()));
				feedback.setImgUrl(rs.getString(Tables.FeedBack.COLUMN_imgUrl.getLabel()));
				feedback.setLogoUrl(rs.getString(Tables.FeedBack.COLUMN_logoUrl.getLabel()));
				feedback.setPriority(rs.getInt(Tables.FeedBack.COLUMN_priority.getLabel()));
				feedbackList.add(feedback);
			}
			return feedbackList;
			
		} catch (Exception e) {
			_logger.log(Level.SEVERE, "Unable to execute getAllFeedbacks Statement " + e);
		} finally {
			ConnectionHelper.close(rs);
			ConnectionHelper.close(ps);
			ConnectionHelper.close(con);
		}
		return null;
	}
	
	
}
