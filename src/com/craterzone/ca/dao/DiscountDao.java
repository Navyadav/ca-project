package com.craterzone.ca.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

import com.craterzone.ca.common.util.ConnectionHelper;
import com.craterzone.ca.enums.SqlQueries;
import com.craterzone.ca.enums.Tables;
import com.craterzone.ca.model.DiscountBean;

public class DiscountDao {

	@Autowired
	private DataSource _dataSource;
	
	final static Logger _logger = Logger.getLogger(DiscountDao.class.getName());

	// Getting all Discounts and puting in List
	
	public ArrayList<DiscountBean> getDiscountList() {
		StringBuilder query = SqlQueries.getDiscount();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = _dataSource.getConnection();
			ps = con.prepareStatement(query.toString());
			rs = ps.executeQuery();
			if(rs == null) {
				return null;
			}
			ArrayList<DiscountBean> discountList=new ArrayList<DiscountBean>();
			while(rs.next()) {
				DiscountBean discountBean=new DiscountBean();
				discountBean.setId(rs.getInt(Tables.Discount.COLUMN_id.getLabel()));
				discountBean.setDomainName(rs.getString(Tables.Discount.COLUMN_domainName.getLabel()));
				discountBean.setCompanyName(rs.getString(Tables.Discount.COLUMN_companyName.getLabel()));
				discountBean.setDatetime(new java.util.Date(rs.getDate(Tables.Discount.COLUMN_dateTime.getLabel()).getTime()));
				discountBean.setPercentage(rs.getDouble(Tables.Discount.COLUMN_rate.getLabel()));
				discountList.add(discountBean);
			}
			return discountList;
		} catch (Exception e) {
			_logger.log(Level.SEVERE, "Unable to execute getDiscountList in DiscountDAO" + e);
		} finally {
			ConnectionHelper.close(ps);
			ConnectionHelper.close(con);
			ConnectionHelper.close(rs);
		}
		return null;
	}
	
	// Delete the discount
	
	public boolean deleteDiscount(int id) {
		
		StringBuilder query = SqlQueries.getDeleteDiscount();
		Connection con = null;
		PreparedStatement ps = null;
		try {
			query.append(id);
			con = _dataSource.getConnection();
			ps = con.prepareStatement(query.toString());
			if(ps.executeUpdate() > 0) {
				return true;
			}
		} catch(Exception e) {
			_logger.log(Level.SEVERE, "Unable to execute deleteDiscount in DiscountDAO" + e);
		}
		finally {
			ConnectionHelper.close(ps);
			ConnectionHelper.close(con);
		}
		return false;
	}
	
	// Adding new Discount
	public boolean addDiscount(DiscountBean discountbean) {
		StringBuilder query = SqlQueries.getAddDiscount();
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = _dataSource.getConnection();
			ps = con.prepareStatement(query.toString());
			ps.setInt(1, 0);
			ps.setString(2, discountbean.getDomainName());
			ps.setString(3, discountbean.getCompanyName());
			ps.setTimestamp(4, new java.sql.Timestamp(new java.util.Date().getTime()));
			ps.setDouble(5, discountbean.getPercentage());
			ps.setInt(6,1);
			if(ps.executeUpdate()>0) {
				return true;
			} else {
				return false;
			}
		} catch(Exception e) {
			_logger.log(Level.SEVERE, "Unable to execute addDiscount in DiscountDAO" + e);
			return false;
		}
		finally {
			ConnectionHelper.close(ps);
			ConnectionHelper.close(con);
			
		}
	}
	
	// Updating existed discount
	public boolean updateDiscount(DiscountBean discountBean) {
		
		StringBuilder query = SqlQueries.updateDiscount();
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = _dataSource.getConnection();
			ps = con.prepareStatement(query.toString());
			ps.setString(1, discountBean.getDomainName());
			ps.setString(2, discountBean.getCompanyName());
			ps.setDate(3, new java.sql.Date(new java.util.Date().getTime()));
			ps.setDouble(4, discountBean.getPercentage());
			ps.setInt(5, discountBean.getId());
			if(ps.executeUpdate()>0) {
				return true;
			} else {
				return false;
			}
		} catch(Exception e) {
			_logger.log(Level.SEVERE, "Unable to execute updateDiscount Statement " + e);
			return false;
		}
		finally {
			ConnectionHelper.close(ps);
			ConnectionHelper.close(con);
		}
	}
	
	/**
	 * Getting discount for a particular Domain
	 * @param domain
	 * @return Discount - double
	 */
	
	public double getDiscountForDomain(String domain) {
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			con = _dataSource.getConnection();
			StringBuilder query = new StringBuilder("SELECT ");
			query.append(Tables.Discount.COLUMN_rate.getLabel());
			query.append(" FROM ");
			query.append(Tables.DISCOUNT.getLabel());
			query.append(" WHERE ");
			query.append(Tables.Discount.COLUMN_domainName.getLabel());
			query.append("=");
			query.append("'"+domain+"'");
			ps = con.prepareStatement(query.toString());
			rs = ps.executeQuery();
			if(rs.next()) {
				return rs.getDouble(1);
			}
			
		} catch (Exception e) {
			_logger.log(Level.SEVERE, "Unable to execute getDiscountForDomain Statement " + e);
		} finally {
			ConnectionHelper.close(rs);
			ConnectionHelper.close(ps);
			ConnectionHelper.close(con);
		}
		return 0;
	}


}
