<%@ page language="java" contentType="text/html; charset=utf-8"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="step"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<!-- Set latest rendering mode for IE -->
<title>Uploads</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<% 
	String path=request.getContextPath(); 
%>
<script src="<%=path %>/js/jquery-latest.js"></script>
<script src="<%=path%>/js/validate.js"></script>
<script type="text/javascript">
$(function() {
	$(".file_browse").change(function() {
		$(this).parent().addClass("upload_btn1");
	});
});

$(function() {
$('.popup').click(function(event) {
    event.preventDefault();
    window.open($(this).attr("href"), "popupWindow", "width=1100,height=600,scrollbars=yes");
});
});
</script>
</head>
<body>

    <!-- Form Start -->
    <section class="container">
    	<div class="form upload">
        
           <h2>Uploads</h2>
           	<h5 style="color:red;" id="error">${error}</h5>
           
           <step:form method="POST" action="submitRegisterationSecond.htm"
			commandName="NewUserInformation" enctype="multipart/form-data" onsubmit="return onSubmitSecond();">
           <!-- section 1 -->
           <table cellpadding="8" cellspacing="0">
           
           		<tr>
                	<td>1.</td>
                    <td>Assessment Year <span class="required">*</span></td>
                    <td width="400px"><step:select path="assessmentYear" >
                        <step:option path="assessmentYear" value="2013" id="assessmentYear">2013</step:option>
                        <step:option path="assessmentYear" value="2012" id="assessmentYear">2012</step:option>
                    </step:select></td>
                </tr>
           
           </table>
           <!-- section 1 End -->
           
           <!-- section 2 -->
           <table cellpadding="5" cellspacing="0">
           	<tr>
            	<td>2.</td>
                <td colspan="2">Income from Salaries/ Professional/Consultant - Form 16/ Form 16A. <span class="required">*</span></td>
            </tr>
            
            <tr class="style_1">
            	<td></td>
                <td><table cellpadding="5" cellspacing="0">
                	<tr>
                    	<td>I.</td>
                        <td>Part A<span class="required">*</span></td>
                        <td><div class='upload_btn'>
                        <input type='file' class='file_browse' name="files[0]" id="file" /></div></td>
                        <td>
                        <input placeholder="Password(Optional)" title="If required" type="password" name="passFiles[0]" id="passFiles[]" /></td>
                    </tr>
                    <tr>
                    	<td>II.</td>
                        <td>Part A</td>
                        <td><div class='upload_btn'><input type='file' class='file_browse' name="files[1]" id="file" /></div></td>
                        <td><input placeholder="Password(Optional)" title="If required" type='password' name="passFiles[1]" id="passFiles[]"/></td>
                    </tr>
                    <tr>
                    	<td>III.</td>
                        <td>Part A</td>
                         <td><div class='upload_btn'><input type='file' class='file_browse' name="files[2]" id="file" /></div></td>
                        <td><input placeholder="Password(Optional)" title="If required" type="password" name="passFiles[2]" id="passFiles[]"/></td>
                    </tr>
                    <tr>
                    	<td>IV.</td>
                        <td>Part A</td>
                        <td><div class='upload_btn'><input type='file' class='file_browse' name="files[3]" id="files[]" /></div></td>
                        <td><input placeholder="Password(Optional)" title="If required" type="password" name="passFiles[3]" id="passFiles[]"/></td>
                    </tr>
                	</table>
                </td>
                
                
                <td><table cellpadding="5" cellspacing="0" align="right">
                	<tr>
                        <td>Part B</td>
                        <td><div class='upload_btn'><input type='file' class='file_browse' name="files[4]" id="files[]" /></div></td>
                        <td><input placeholder="Password(Optional)" title="If required" type="password" name="passFiles[4]" id="passFiles[]"/></td>
                    </tr>
                    <tr>
                        <td>Part B</td>
                        <td><div class='upload_btn'><input type='file' class='file_browse' name="files[5]" id="files[]" /></div></td>
                        <td><input placeholder="Password(Optional)" title="If required" type="password" name="passFiles[5]" id="passFiles[]"/></td>
                    </tr>
                    <tr>
                        <td>Part B</td>
                        <td><div class='upload_btn'><input type='file' class='file_browse' name="files[6]" id="files[]" /></div></td>
                        <td><input placeholder="Password(Optional)" title="If required" type="password" name="passFiles[6]" id="passFiles[]"/></td>
                    </tr>
                    <tr>
                        <td>Part B</td>
                        <td><div class='upload_btn'><input type='file' class='file_browse' name="files[7]" id="files[]" /></div></td>
                        <td><input placeholder="Password(Optional)" title="If required" type="password" name="passFiles[7]" id="passFiles[]"/></td>
                    </tr>
                	</table>
                </td>
                
            </tr>
           </table>
           <!-- section 2 End -->
           
           
           <!-- section 3 -->
           <table cellpadding="5" cellspacing="0">
           		<tr>
                	<td valign="top">3.</td>
                    <td>Income from Bank Interest &#45; All Savings Account Interest (From 1st April to 31st March).</td>
                </tr>
                <tr>
                	<td></td>
                    <td>Rs. <step:input name="" type="text" path="income" id="income" onkeypress="return isNumber(event)" /></td>
                </tr>
           </table>
           <!-- section 3 End -->
           
           <!-- Section 4 -->
           <table cellpadding="5" cellspacing="0">
           	<tr>
            	<td valign="top">4.</td>
                <td colspan="2">Interest on FD's/ Any other Income - If TDS has been deducted then Form 16A or 26AS report, otherwise only figure. </td>
            </tr>
            
            <tr class="style_1">
            	<td></td>
                <td><table cellpadding="5" cellspacing="0">
                	<tr>
                    	<td>I.</td>
                        <td>Form 16</td>
                         <td><div class='upload_btn'><input type='file' class='file_browse' name="files[8]" id="files[]" /></div></td>
                        <td><input placeholder="Password(Optional)" title="If required" type="password" name="passFiles[8]" id="passFiles[]"/></td>
                    </tr>
                    <tr>
                    	<td>II.</td>
                        <td>Form 16</td>
                        <td><div class='upload_btn'><input type='file' class='file_browse' name="files[9]" id="files[]" /></div></td>
                        <td><input placeholder="Password(Optional)" title="If required" type="password" name="passFiles[9]" id="passFiles[]"/></td>
                    </tr>
                    <tr>
                    	<td>III.</td>
                        <td>Form 16</td>
                         <td><div class='upload_btn'><input type='file' class='file_browse' name="files[10]" id="files[]" /></div></td>
                        <td><input placeholder="Password(Optional)" title="If required" type="password" name="passFiles[10]" id="passFiles[]"/></td>
                    </tr>
                    <tr>
                    	<td>IV.</td>
                        <td>Form 16</td>
                        <td><div class='upload_btn'><input type='file' class='file_browse' name="files[11]" id="files[]" /></div></td>
                        <td><input placeholder="Password(Optional)" title="If required" type="password" name="passFiles[11]" id="passFiles[]"/></td>
                    </tr>
                    <tr>
                    	<td>Rate</td>
                        <td colspan="3"><step:input name="" maxlength="3" type="text" path="bankInterest" id="bankInterest" onkeypress="return isNumber(event)" /></td>
                    </tr>
                	</table>
                </td>
            </tr>
           </table>
           <!-- Section 4 End -->
           
           
            <!-- Section 5 -->
           <table cellpadding="5" cellspacing="0">
           	<tr>
            	<td valign="top">5.</td>
                <td colspan="2">Additional Proofs (If not considered in Form - 16) - 80C/80E/80D/ Home Loan-(Bank Certificate and possession letter)/ 80G- Donation (Donee's Receipt and exemption certificate).</td>
            </tr>
            
            <tr class="style_1">
            	<td></td>
                <td><table cellpadding="5" cellspacing="0">
                	<tr>
                    	<td>I.</td>
                        <td>Proof 1</td>
                        <td><div class='upload_btn'><input type='file' class='file_browse' name="files[12]" id="files[]" /></div></td>
                        <td><input placeholder="Password(Optional)" title="If required" type="password" name="passFiles[12]" id="passFiles[]"/></td>
                    </tr>
                    <tr>
                    	<td>II.</td>
                        <td>Proof 2</td>
                         <td><div class='upload_btn'><input type='file' class='file_browse' name="files[13]" id="files[]" /></div></td>
                        <td><input placeholder="Password(Optional)" title="If required" type="password" name="passFiles[13]" id="passFiles[]"/></td>
                    </tr>
                    <tr>
                    	<td>III.</td>
                        <td>Proof 3</td>
                        <td><div class='upload_btn'><input type='file' class='file_browse' name="files[14]" id="files[]" /></div></td>
                        <td><input placeholder="Password(Optional)" title="If required" type="password" name="passFiles[14]" id="passFiles[]"/></td>
                    </tr>
                    <tr>
                    	<td>IV.</td>
                        <td>Proof 4</td>
                         <td><div class='upload_btn'><input type='file' class='file_browse' name="files[15]" id="files[]" /></div></td>
                        <td><input placeholder="Password(Optional)" title="If required" type="password" name="passFiles[15]" id="passFiles[]"/></td>
                    </tr>
                	</table>
                </td>
            </tr>
           </table>
           <!-- Section 5 End -->
           
           <!-- Section 5 -->
           <table cellpadding="5" cellspacing="0">
           		<tr>
                	<td>6.</td>
                    <td>Remarks</td>
                </tr>
                <tr>
                	<td></td>
                    <td><step:textarea name="" cols="" rows="" path="remark"></step:textarea></td>
                </tr>
           </table>
           <!-- Section 5 End -->
           
           <table cellpadding="5" cellspacing="0" style="margin-left:20px; font-size:13px">
           	<tr>
            	<td><input name="" type="checkbox" value="" id="checkbox1"></td>
                <td>I accept all the <a class="popup" href="tandc.htm" >terms and conditions.</a></td>
            </tr>
            <tr>
            	<td valign="top"><input name="" type="checkbox" value="" id="checkbox2"></td>
                <td>I confirm that all the information and documents provided by me are true and correct.</td>
            </tr>
            <tr>
            	<td colspan="2"><input name="" type="submit" value="Proceed to Pay"></td>
            </tr>
           </table>
          
          </step:form> 
        </div>
    
    
    </section>
    <!-- Form End -->

</body>
</html>
