<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Terms and Conditions</title>
<style type="text/css">
p,div,h2,h3,h5,h4{
margin:0px;
padding:0px;
}

</style>
</head>
<body>

<div style="background-color:#E6E6E6">
<h2>Terms of Use</h2>
<p>We advise you to read the terms and conditions for use of our website prior to usage.</p><br>
<p>By using this website, you agree to the following:</p><br>
<h3>TERMS & CONDITIONS</h3>
<h3>Disclaimer</h3>
<p>Our disclaimer should be read along with our Privacy Policy.</p><br>
<p>Filemyreturn is the software service provided by Rai Tax Consultants.</p><br>
<p>Acceptance of Terms & Conditions given below is required to avail the services of this website.</p><br>
<p>Every effort has been made to avoid errors or omissions. In spite of this, errors may creep in. Any mistake, error or discrepancy noted may be brought to our notice which shall be taken care of. It is notified that neither Filemyreturn nor the authors will be responsible for any damage or loss of action to any one, of any kind, in any manner, there from. It is suggested that to avoid any doubt, the reader should cross-check all the facts, law and contents of the material on web with original Government publications or notifications.</p><br><br>
<p>The material contained on this site and on the associated web pages is general information and is not intended to be advice on any particular matter. Users and readers should seek appropriate professional advice before acting on the basis of any information contained herein or using any Software provided herein. Filemyreturn, its directors, employees, agents, representatives and the authors expressly disclaim any and all liability to any person, whether a subscriber or not, in respect of anything and of the consequences of anything done or omitted to be done by any such person in reliance upon the contents of this site or use of software provided in this site and associated web pages.</p><br>
<p>In no event shall Filemyreturn be liable for any direct, indirect, punitive, incidental, special, consequential damages or any damages whatsoever including, without limitation, damages for loss of use, data or profits, arising out of the use or performance or inability to use the Filemyreturn web site or related services.</p><br>
<p>We do not warrant that the functions contained in the material and software on this website will be uninterrupted or error free, that defects will be corrected, or that this site or the servers that make it available are free of viruses or represent the full functionality, accuracy or reliability of the materials. In no event will we be liable for any loss or damage, or any loss or damages whatsoever arising from the use, or loss of use, of data, or profits arising out of or in connection with the use of this website or the software provided therein.</p><br>
<p>Information in the many web pages that are linked to Filemyreturn's Website comes from a variety of sources. Some of this information comes from official Filemyreturn licensees, but much of it comes from unofficial or unaffiliated organizations and individuals, both internal and external to Filemyreturn. Filemyreturn does not author, edit, or monitor these unofficial pages or links. You acknowledge and agree that Filemyreturn and its Website Co-branding Providers shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on such external sites or resources.</p><br>
<p>By using the www.filemyreturn.co.in  web site, including any software, and content contained therein, you agree that use of the site is entirely at your own risk. The site is provided "as is," without warranty of any kind, either express or implied, including without limitation, any warranty for information, data, services, uninterrupted access, or products provided through or in connection with the site. Specifically, Filemyreturn disclaims any and all warranties, including, but not limited to: (1) any warranties concerning the security, availability, accuracy, usefulness, or content of information, products or services and (2) any warranties of title, warranty of non-infringement, warranties of merchantability or fitness for a particular purpose. This disclaimer of liability applies to any damages or injury caused by any failure of performance, error, omission, interruption, deletion, defect, delay in operation or transmission, computer virus, communication line failure, theft or destruction or unauthorized access to, alteration of, or use of record, whether for breach of contract, tortuous behavior, negligence, or under any other cause of action.</p><br>
<p>Neither Filemyreturn nor any of its employees, agents, successors, assigns, affiliates, website co-branding providers or content or service providers shall be liable to you or other third party for any direct, indirect, incidental, special or consequential damages arising out of use of service or inability to gain access to or use the service or out of any breach of any warranty. </p><br>
<p>At any stage if you are dissatisfied with any products, services, software and related material purchased or any portion of the Filemyreturn web site, or with any of these terms of use, your sole and exclusive remedy is to discontinue the use of Filemyreturn web site.</p><br>
<h4>Using Filemyreturn website</h4>
<p>Our website is maintained for your personal use and viewing. Access and use by you of our site constitutes your acceptance of these terms and conditions. They take effect from the date on which you first use this website. We may at any time revise these terms and conditions without notice, and therefore they should be regularly checked. The continued use of the site after a change has been made is your acceptance of the change.
</p><br>
<p>You agree to use this site only for lawful purposes, and in a manner that does not infringe the rights of, or restrict or inhibit the use and enjoyment of this site by any third party. Such restrictions or inhibitions includes, without limitation, conduct which is unlawful, or which may harass or cause distress or inconvenience to any person, and the transmission of obscene or offensive content or disruption to the normal flow of dialogue within this site.</p><br>
<p>Our responsibilities and liabilities to you in providing this site and any associated services are limited as set out in our disclaimer.</p><br>
<h5>Online services</h5>
<p>A number of our online services have specific terms and conditions related to their use. You will be asked to confirm acceptance of these when using them for the first time.</p><br>
<h5>Software Usage</h5>
<p>As long as you comply with the terms of usage as stated herein and have paid the payment fee as specified on this website, Filemyreturn allows you to provide your information/documents for Income  Tax Return Filing. You are not allowed to copy this software on any computer machine.</p><br>
<p>&#8216;&#8216;Software&#8217;&#8217; means (a) all of the contents displayed on the web site, including but not limited to (i) software; (ii) digital images, clip art, sounds or other artistic works (iii) related explanatory written materials or files; and (iv) fonts; and (b) upgrades, modified versions, updates, additions, and copies of the Software, if any.</p><br>
<p>You shall not copy the Software except as set forth in the terms hereof and shall not modify, adapt or translate the Software. You shall not reverse engineer, decompile, disassemble or otherwise attempt to discover the source code of the Software.</p><br>
<p>Filemyreturn owns all intellectual property in the Software. Filemyreturn permits you to use the Software only in accordance with the terms of this Agreement.</p><br>
<p>The Software and any authorized copies that you make are the intellectual property of and are owned by Filemyreturn. The structure, organization and code of the Software are the valuable trade secrets and confidential information of Filemyreturn. This Agreement does not grant you any intellectual property rights in the Software and all rights not expressly granted are reserved by Filemyreturn.</p><br>
<h5>Links to external websites</h5>
<p>This has been extensively explained in our Privacy Policy
</p><br>
<h5>Links to external websites</h5>
<p>This has been extensively explained in our Privacy Policy
</p><br>
<h5>Limited Liability</h5><p>
Under no circumstances will Filemyreturn or any of its affiliates or assigns be liable to you, or any other person or entity acting on behalf of you, for any loss of use, revenue or profit, lost or damaged data, or other commercial or economic loss or for any direct, indirect, incidental, special, statutory, punitive, exemplary or consequential damages whatsoever related to your use or reliance upon software or services on this website, even if advised of the possibility of such damages or if such damages are foreseeable.
</p><br>
<h5>Accurate Calculation: </h5>
<p>We diligently work to ensure the accuracy of the calculations done by us on the basis of your inputs.  In case of your failure to enter all required information accurately, willful or fraudulent omission or inclusion of information on your tax return, misclassification of information to avoid or reduce an applicable penalty/interest then you must contact us on the details mentioned on the website. </p><br>
<h5>Other Information </h5>
<p>Filemyreturn is taking reasonable and appropriate measures, to ensure that your personal information is disclosed only to those specified by you. However, the Internet is an open system and we cannot and do not guarantee that the personal information you have entered will not be intercepted by others and decrypted.</p><br>
<p>If you wish to use the Site, you may be asked to supply certain information, including credit card or other payment mechanism information. You agree not to hold Filemyreturn liable for any loss or damage of any sort incurred as a result of any such dealings with any merchant or information or service provider through the Site. You agree that all information you provide through the Site for purposes of availing services will be accurate, complete and current. You agree to pay all charges incurred by users of your account and credit card or other payment mechanism at the prices in effect when such charges are incurred. You also will be responsible for paying any applicable taxes relating to services availed through the Site.</p><br>
<h5>General Provisions</h5>
<p>If any part of this agreement is found void and unenforceable, it will not affect the validity of the balance of this agreement, which shall remain valid and enforceable according to its terms. This agreement may only be modified by a writing signed by an officer of Filemyreturn. This is the entire agreement between Filemyreturn and you relating to the use of the Software and the services provided through this website and it supersedes any prior representations, discussions, undertakings, communications or advertisements relating to the Software and the services.

Filemyreturn reserves the right at any time to modify, alter, or update these terms of use and Disclaimer without prior notice and you agree that you shall be bound by such modifications, alterations, or updates.
</p><br>
<p>The failure of Filemyreturn to exercise or enforce any right or provision of the Terms of Service shall not constitute a waiver of such right or provision. If any provision of the Terms of Service is found by a court of competent jurisdiction to be invalid, the parties nevertheless agree that the court should endeavor to give effect to the parties' intentions as reflected in the provision, and the other provisions of the Terms of Service remain in full force and effect.</p><br>
<h5>Privacy Policy</h5>
<p>Our Privacy Policy explains how we protect and respect the data we collect from you. Your use of this site constitutes acceptance of this policy.</p><br>
<h5>Governing law</h5>
<p>These terms and conditions shall be governed by and construed in accordance with the laws of India. Any dispute arising under these terms and conditions shall be subject to the exclusive jurisdiction of the courts of New Delhi, India.</p><br>
<h5>TERMS & CONDITIONS (ITR-V Deposit Facility)</h5>
<p>If you avail any filemyreturn service then you are liable for sending a signed copy of ITR-V (to be received on the email provided by you) to the  </p><br>
<p style="background-color:#FFFFFF">Income Tax Department - CPC, Post Box No. 1, Electronic City Post Office, Bangalore - 560100, Karnataka.</p><br>
</div>
</body>
</html>