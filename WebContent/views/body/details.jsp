<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<body>

        <div class="ticket">
     
<table cellpadding="15" cellspacing="5" width="100%">
	<h2>User Details</h2>
	
	<tr>
	<td>Transaction Id</td>
	<td>${userData.transactionId }</td>
	</tr>
	<tr>
		<td>Name</td>
		<td>${userData.fUserName } &nbsp;${userData.mUserName
			}&nbsp;${userData.lUserName }</td>

		<td>Father's Name</td>
		<td>${userData.lUserName } &nbsp;${userData.fFatherName
			}&nbsp;${userData.lFatherName }</td>

	</tr>
	<tr>
		<td>Date of Birth</td>
		<td>${userData.DOB }</td>

		<td>Gender</td>
		<td>${userData.gender }</td>

	</tr>	
	<tr>
		<td>Mobile No.</td>
		<td>${userData.mobileNumber }</td>

		<td>Email-id</td>
		<td>${userData.email }</td>

	</tr>
	<tr>
		<td>Address</td>
		<td>${userData.address1 }</td>

	</tr>

	<tr>
		<td></td>
		<td>${userData.address2 }</td>
	</tr>
	<tr>
	<td>State/City</td>
		<td>${userData.state }&nbsp;&nbsp;${userData.city }&nbsp;&nbsp;${userData.pinCode }</td>
		
	</tr>
	<tr>
		<td>Pan Number</td>
		<td><c:forEach items="${userData.panNumber}" var="panArray" >${panArray}</c:forEach></td>
		<td>Account No.</td>
		<td>${userData.accountNumber }</td>

	</tr>
	<tr>
		<td>Bank Name</td>
		<td>${userData.bankName }</td>
		<td>Bank Address</td>
		<td>${userData.bankAddress }</td>

	</tr>
		<tr>
		<td>Remarks</td>
		<td>${userData.remark }</td>
		<td>Income</td>
		<td>${userData.income }</td>

	</tr>
</table>
</div>
</body>