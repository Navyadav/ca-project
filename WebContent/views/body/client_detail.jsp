<%@ taglib uri="http://www.springframework.org/tags/form" prefix="step"%>
<head>
<%
	String path = request.getContextPath();
%>
<script src="<%=path%>/js/validate.js"></script>
 <script>
$(function() {
$( "#datepicker" ).datepicker({changeMonth:true,changeYear:true, yearRange: "1960:2016", maxDate: new Date()});
});

</script>
<script>
$(function() {
$(".inputs").keyup(function () {
    if (this.value.length == this.maxLength) {
      $(this).next('.inputs').focus();
    }
});
});
</script>
<script>
$(function() {
	$(".inputs[type=text]").change(function(){
		  $(this).val( $(this).val().toUpperCase() );
		});
});
</script>
</head>

<body>
		<div class="form">

			<h1>Client Detail</h1>
			</td>
			<p>Lorem Ipsum is simply dummy text of the printing and
				typesetting industry. Lorem Ipsum has been the industry's standard
				dummy text ever since the 1500s</p>

			<div class="divider"></div>

			<h4 style="color: red;" id="error">${error}</h4>

			<table cellpadding="8" cellspacing="0">

				<step:form method="POST" action="submitRegisterationfirst.htm"
					commandName="NewUserInformation" onsubmit="return formSubmit();">
					<tr>
						<td colspan="2"><h2>Please fill all the important
								details here.</h2></td>
					</tr>
					<tr>
						<td colspan="2"><h3>Please fill 1st Four Details as per
								PAN CARD</h3></td>
					</tr>
					<tr>
						<td width="25%">User Name <span class="required">*</span></td>
						<td width="75%" class="three_field">
						<step:input name="" type="text" placeholder="First" path="fUserName" id="fUserName"  onkeypress="return onlyAlphabets(event,this)"/>
							<step:input name="" type="text" placeholder="Middle" onkeypress="return onlyAlphabets(event,this)"
								path="mUserName" id="mUserName" /> 
							<step:input name=""	type="text" placeholder="Last" path="lUserName" id="lUserName" onkeypress="return onlyAlphabets(event,this)"
								style="margin-right: 0; " /> </br> <step:errors path="fUserName" />
							<step:errors path="lUserName" /></td>
					</tr>
					<tr>
						<td>Father Name <span class="required">*</span></td>
						<td class="three_field"><step:input name="" type="text" onkeypress="return onlyAlphabets(event,this)"
								placeholder="First" path="fFatherName" id="fFatherName" /> 
						<step:input
								name="" type="text" placeholder="Middle" path="mFatherName" onkeypress="return onlyAlphabets(event,this)"
								id="mFatherName" /> 
						<step:input name="" type="text"
								placeholder="Last" style="margin-right: 0;" path="lFatherName" onkeypress="return onlyAlphabets(event,this)"
								id="lFatherName" /> <br> <step:errors path="fFatherName" />
							<step:errors path="lFatherName" /></td>
					</tr>
					<tr class="three_field">
						<td>Date of Birth <span class="required">*</span></td>
						<td><step:input placeholder="MM/DD/YYYY" type="text" path="DOB" id="datepicker" readonly="true" />
						<br>
						<step:errors path="DOB" />
						</td>
					</tr>

					<tr>
						<td>PAN Card Number <span class="required">*</span></td>
						<td class="userPanNum">
						<input class="inputs" type="text" name="panNumber" maxlength="1" onkeypress="return onlyAlphabets(event,this)">
						<input class="inputs" type="text" name="panNumber" maxlength="1" onkeypress="return onlyAlphabets(event,this)">
						<input class="inputs" type="text" name="panNumber" maxlength="1" onkeypress="return onlyAlphabets(event,this)">
						<input class="inputs" type="text" name="panNumber" maxlength="1" value="P" readonly="readonly">
						<input class="inputs" type="text" name="panNumber" maxlength="1" onkeypress="return onlyAlphabets(event,this)">
						<input class="inputs" type="text" name="panNumber" maxlength="1" onkeypress="return isNumber(event)">
						<input class="inputs" type="text" name="panNumber" maxlength="1" onkeypress="return isNumber(event)">
						<input class="inputs" type="text" name="panNumber" maxlength="1" onkeypress="return isNumber(event)">
						<input class="inputs" type="text" name="panNumber" maxlength="1" onkeypress="return isNumber(event)">
						<input class="inputs" type="text" name="panNumber" maxlength="1" onkeypress="return onlyAlphabets(event,this)">
						
						
						<%-- <step:input name="" type="text" path="panNumber" onkeypress="return checkAlphaNumeric(event)"
								id="panNumber" maxlength="10" /> <br> <step:errors path="panNumber" /> --%></td>
					</tr>

					<tr>
						<td>Gender <span class="required">*</span></td>
						<td><step:input name="" type="radio" path="gender"
								checked="true" value="male" /> Male <step:input name=""
								type="radio" path="gender" value="Female" /> Female <br> <step:errors
								path="gender" /></td>

					</tr>

					<tr>
						<td>
							Mobile Number <span class="required">*</span>
						</td>
						<td><step:input placeholder="0987654321" name="" type="text" path="mobileNumber" onkeypress="return isNumber(event)"
								maxlength="10" id="mobileNumber" /> <br> <step:errors
								path="mobileNumber" /></td>
					</tr>

					<tr>
						<td>Email ID <span class="required">*</span></td>
						<td><step:input placeholder="example@domain.com" name="" type="text" path="email" id="email" />
							<br> <step:errors path="email" /></td>
					</tr>

					<tr>
						<td>Address <span class="required">*</span></td>
						<td><step:input placeholder="address line 1" name="" type="text" path="address1"
								id="address1" /> <br> <step:errors path="address1" /></td>
					</tr>
					<tr>
						<td></td>
						<td><step:input placeholder="address line 2" name="" type="text" path="address2"
								id="address2" /> <br> <step:errors path="address2" /></td>
					</tr>
					<tr>
						<td></td>
						<td><table cellpadding="5" cellspacing="0">
								<tr>
									<td>City<span class="required">*</span></td>
									<td>State<span class="required">*</span></td>
									<td>Pin<span class="required">*</span></td>
								</tr>
								<tr>
									<td><step:input placeholder="City" name="" type="text" path="city" id="city" onkeypress="return onlyAlphabetsWithSpace(event,this)" />
										<br> <step:errors path="city" /></td>
									<td><step:input placeholder="State" name="" type="text" path="state" onkeypress="return onlyAlphabetsWithSpace(event,this)"
											id="state" /> <br> <step:errors path="state" /></td>
									<td><step:input placeholder="Pin Code" name="" type="text" path="pinCode" onkeypress="return isNumber(event)"
											id="pinCode" maxlength="6" /> <br> <step:errors
											path="pinCode" /></td>
								</tr>
							</table></td>
					</tr>

					<tr>
						<td colspan="2"><h2>Bank Information</h2></td>
					</tr>

					<tr>
						<td>Account Number <span class="required">*</span></td>
						<td><step:input placeholder="Account Number" name="" type="text" path="accountNumber" maxlength="16" onkeypress="return isNumber(event)"
								id="accountNumber" /> <br> <step:errors
								path="accountNumber" /></td>
					</tr>
					<tr>
						<td>IFSC Code <span class="required">*</span></td>
						<td><step:input placeholder="IFSC Code" name="" type="text" path="ifsc" id="ifsc" maxlength="11"/>
							<br> <step:errors path="ifsc" /></td>
					</tr>
					<tr>
						<td></td>
						<td align="center" style="font-size: 24px;">Or</td>
					</tr>
					<tr>
						<td>Bank Name <span class="required">*</span></td>
						<td><step:input placeholder="Bank Name" name="" type="text" path="bankName" onkeypress="return onlyAlphabetsWithSpace(event,this)"
								id="bankName" /> <br> <step:errors path="bankName" /></td>
					</tr>
					<tr>
						<td>Bank Address <span class="required">*</span></td>
						<td><step:input placeholder="Bank Address" name="" type="text" path="bankAddress"
								id="bankAddress" /> <br> <step:errors path="bankAddress" />
						</td>
					</tr>

					<tr>
						<td></td>
						<td><input name="" type="submit" value="Proceed to Upload"></td>
					</tr>
				</step:form>
			</table>
		</div>


	
	<!-- Form End -->





</body>
</html>
