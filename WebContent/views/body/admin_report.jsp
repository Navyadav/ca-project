<%@page import="java.util.Arrays"%>
<%@page import="com.craterzone.ca.model.RegisterationBean"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="step"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%
	String path = request.getContextPath();
%>

	<script src="<%=path %>/js/jquery.js" type="text/javascript"></script>
	<script src="<%=path %>/js/jquery.treeview.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=path %>/js/demo.js"></script>


<body>

        <div class="ticket">
        <h2>Admin Report Summary</h2>
	 
	${error }
	<ul id="navigation">
	<c:set var="countApplication" value="0" scope="request"></c:set>
		<c:forEach items="${adminReport}" var="datafeed" varStatus="loopCounter" >
			<li><a href="?${loopCounter.count }">${datafeed.key }</a>
			 <ul>
        	<table cellpadding="10" cellspacing="0" width="100%">
            	<tr align="left" bgcolor="#666666" style="color:#fff">
                	<th width="10%">Sr. No.</th>
                    <th width="15%">Date</th>
                    <th>Name</th>
                    <th>PAN</th>
                    <th>Transaction ID </th>
                    <th>Remakrs</th>
                </tr>
			
		<c:forEach items="${datafeed.value}" var="datafeed" varStatus="loopCounter">
	<c:set  var="countApplication" value="${countApplication+1}" />
		 <tr>
                	<td>${loopCounter.count}</td>
                	<td><fmt:formatDate value="${datafeed.dateTime}" pattern="MMM-dd-yyyy" /></td>
                    <td>${datafeed.fUserName}</td>
                    <td><c:forEach items="${datafeed.panNumber}" var="panArray" >${panArray}</c:forEach></td>
                    <td>${datafeed.transactionId}</td>
                    <td>${datafeed.remark}</td>
                </tr>
		</c:forEach>
	 </table>
        </ul>
			
		</li>
		</c:forEach>
        
		
	</ul>
		<div style=" font-family:Arial, Helvetica, sans-serif; margin-top:30px" id="application">
            Number of Application  <strong>${countApplication}</strong>
        </div>

        </div>
    <div class="clearfix"></div>    

</body>
</html>
