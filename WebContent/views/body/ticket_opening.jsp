<%@ taglib uri="http://www.springframework.org/tags/form" prefix="step"%>

<%
	String path = request.getContextPath();
%>

	<script src="<%=path%>/js/ticket.js"></script>
</head>

<body>

    <!-- Form Start -->
 
    	<div class="form" style="min-height:500px;">
        	
            <h2>Ticket Opening</h2>
                <h4 style="color: red;" id="error">${error}</h4>
            
            <br><br>
            <step:form method="POST" action="submitTicket.htm" commandName="ticket" onsubmit="return addTicket();">
            
            <table cellpadding="8" cellspacing="0" width="100%">
            	<tr>
                	<td width="20%">Transaction ID <span class="required">*</span></td>
                    <td><step:input name="" type="text" path="transactionId" id="transid" /></td>
                </tr>
                <tr>
                	<td>PAN<span class="required">*</span></td>
                    <td><step:input maxlength="10" name="" type="text" path="panNumber" id="pan" onkeypress="checkAlphaNumeric" /></td>
                </tr>
                <tr>
                	<td>Mobile <span class="required">*</span></td>
                    <td><step:input maxlength="10" name="" type="text" path="mobile" id="mobile" onkeypress="return isNumber(event)"/></td>
                </tr>
                <tr>
                	<td>Email <span class="required">*</span></td>
                    <td><step:input name="" type="text" path="email" id="email" /></td>
                </tr>
                <tr>
                	<td valign="top">Remarks</td>
                    <td><step:textarea name="" cols="" rows="" style="height:120px;" path="remarks" id="remarks"></step:textarea></td>
                </tr>
                <tr>
                	<td></td>
                    <td><input name="" type="submit" value="Send"></td>
                </tr>
                
            </table>
			
            </step:form>
            
        </div>
    <!-- Form End -->
</body>
</html>
