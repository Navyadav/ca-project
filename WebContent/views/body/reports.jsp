<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="step"%>
<%@ page import="java.text.SimpleDateFormat;" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<!-- Set latest rendering mode for IE -->
<%
	String path = request.getContextPath();
%>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Client Detail</title>
<script type="text/javascript" src="<%=path %>/js/jquery.dataTables.min.js"></script>
<link href="<%=path %>/css/jquery.dataTables.css" rel="stylesheet"
	type="text/css" />
<script>
$(function() {
$( "#datepicker" ).datepicker({changeMonth:true,
	changeYear:true,
	yearRange: "2012:2016",
	maxDate : 0,
	 onSelect: function() {
		 var selectedDate = $('#datepicker').datepicker("getDate");
	        $("#datepicker2").datepicker('option', 'minDate', selectedDate);
	    }
	});
$( "#datepicker2" ).datepicker({
	changeMonth:true,
	changeYear:true,
	minDate : new Date(2012, 1 - 1, 1),
	maxDate:0,
	
	});

});

$(document).ready(function() {
	$("#abc").dataTable({"bFilter": false ,"bLengthChange": false
		/* "aoColumnDefs": [
        {"bSortable": false,
        	"aTargets": [ 5 ]} */
    });
} );
function getUserId(){
	var ids = "";
	$('.userId')
	.each(
			function(index, val){
				ids = ids+($(this).html())+",";
			}); 
	ids = ids.substring(0,ids.length-1);
	var url = "downloadAll.htm?ids=" + ids; 
	$("#fileDownloadSimpleRichExperience").prop('href',url);
	$.get(url);
}
$(document).on("click", "a.fileDownloadSimpleRichExperience", function () {
 $.fileDownload($(this).prop('href'), {
     preparingMessageHtml: "We are preparing your report, please wait...",
     failMessageHtml: "There was a problem generating your report, please try again."
 });
 return false; //this is critical to stop the click event which will trigger a normal file download!
});


</script>
</head>
<body>

	<!-- Form Start -->
	<section class="container">

	<div class="ticket">
		${error}
		<h2>Reports</h2>
		<br>
		<form action="search.htm">
			<table cellpadding="10" cellspacing="0" width="100%"
				style="border-bottom: 1px solid #CCC;">
				<tr>
					<td>From <input name="dateFrom" type="text"
						readonly="readonly" id="datepicker" placeholder="Choose Date"
						class="filter"> To <input name="dateTo" type="text"
						readonly="readonly" id="datepicker2" placeholder="Choose Date"
						class="filter">
					</td>
					<td><input name="keyword" type="text"></td>
					<td><select name="searchBy">
							<option value="pan">PAN Number</option>
							<option value="transactionId">Transaction ID</option>
					</select></td>
					<td><input type="submit" value="Search" class="downloadButton"></td>
				</tr>
			</table>
		</form>


		<table id="abc" cellpadding="10" cellspacing="0" width="100%"
			style="font-family: Arial, Helvetica, sans-serif;">
			<thead>
				<tr align="left" bgcolor="#666666" style="color: #fff">
					<th width="10%" height="28px">Sr. No.</th>
					<th width="15%" height="28px">Date</th>
					<th>Name</th>
					<th>PAN</th>
					<th>Transaction ID</th>
					<c:if test="${username=='admin'}">
							<td>Amount</td>
							<td>Email</td>
							<td>Mobile No.</td>
						</c:if>
					<th style="background: none;">Remakrs</th>
				</tr>

			</thead>
			<tbody>
				<c:forEach items="${registeredUsersList}" var="user"
					varStatus="count">
					<tr>
						<td class="userId">${user.userId }</td>
						<td><fmt:formatDate value="${user.dateTime}" pattern="MMM-dd-yyyy" /></td>
						<td>${user.fUserName }</td>
						<td><c:forEach items="${user.panNumber}" var="panArray" >${panArray}</c:forEach></td>
						<td>${user.transactionId }</td>
						<c:if test="${username=='admin'}">
							<td>${user.amount}</td>
							<td>${user.email}</td>
							<td>${user.mobileNumber}</td>
						</c:if>
						<td style="width: auto" valign="middle">${user.remark }
							<div style="padding-top: 15px;width: 290px">

								<a href="userDetails.htm?userId=${user.userId}"
									class="downloadButton">Details</a>
								<a href="userDetailsPrint.htm?userId=${user.userId}"
									class="downloadButton">Print</a>
								<a href="download.htm?userId=${user.userId }"
									class="downloadButton">Download</a>
							</div>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<c:if test="${fn:length(registeredUsersList) != 0}">
		<div align="right" style="padding-top: 40px">
			<a id="fileDownloadSimpleRichExperience" onclick="getUserId();"
				href="javaScript:void(0);" class="btn_yellow">Download All</a>
		</div>
	</c:if>
	</div>
	<div class="clearfix"></div>
	</section>
	<!-- Form End -->

</body>

</html>