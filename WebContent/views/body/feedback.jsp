<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="step"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<%
	String path = request.getContextPath();
%>
<!-- Set latest rendering mode for IE -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Client Detail</title>
</head>

<body>

    <!-- Form Start -->
    <section class="container">
        
        ${feedbackError}
        <div class="ticket feedback">
        <h2>Feedback</h2>
            <br>
            <table cellpadding="10" cellspacing="0" width="100%" style="font-family:Arial, Helvetica, sans-serif;">
            	<tr align="left" bgcolor="#666666" style="color:#fff">
                	<th>Image URL</th>
                    <th>Logo URL</th>
                    <th>Feedback</th>
                    <th></th>
                </tr>
                <c:forEach var="currentFeedback" items="${feedback}">
                <tr>
                	<td>${currentFeedback.imgUrl}</td>
                    <td>${currentFeedback.logoUrl}</td>
                    <td>${currentFeedback.feedbackMessage}</td>
                    <td><a href="deleteFeedback.htm?feedbackID=${currentFeedback.feedbackId}" 
                     class="btn_delete linkButton">Delete</a></td>
                </tr>
               </c:forEach>
            </table>
            
            <br><br>
            <div class="add_feedback">
            <step:form commandName="NewFeedback" method="POST" action="saveFeedback.htm">
            <table cellpadding="10" cellspacing="0" width="100%">
            	<tr>
                	<td colspan="10"><h2>Add New Feedback</h2></td>
                </tr>
                <tr>
                	<td>Image URL<br><step:input placeholder="http://example.com/example.img" path="imgUrl"/></td>
                    <td>Logo URL<br><step:input placeholder="http://example.com/logo.img" path="logoUrl"/></td>
                    <td>Feedback<br><step:input placeholder="feedback message" path="feedbackMessage"/></td>
                </tr>
                <tr align="right">
                	<td colspan="2"></td>
                	
                	<td>
                		<table width="100%">
                			<tr>
                				<td><input onMouseOver="this.style.color='#000000'" onMouseOut="this.style.color='#FFFFFF'" type="submit" name="button" value="Add on top"/></td>
                				<td><input onMouseOver="this.style.color='#000000'" onMouseOut="this.style.color='#FFFFFF'" type="submit" name="button" value="Add"/></td>
                			</tr>
                			
                		</table>
                	</td>
                	
                </tr>
            </table>
            </step:form>
            </div>
            
        </div>
   
   
    <div class="clearfix"></div>    
    </section>
    <!-- Form End -->

</body>
</html>