<%@ page language="java" contentType="text/html; charset=utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<!-- Set latest rendering mode for IE -->
<title>Faqs</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
</head>
<body style="font-family: 'Times New Roman',Georgia,Serif; font-size: 18px;">
<h2 align="center">FAQ's</h2>
<p>

<ol>
<li><b>In case of ITR-V copy is not received?</b><br>Just drop a mail on support@filemyreturn.co.in with Reference No., date of uploading, Name, Pan No., mobile no. and Date of Birth. </li>
<li>In case of intimations received from Income Tax department:-<br>Various Intimations:-<br>
	<ol type="a">
	<li><b>Intimation regarding &#8216;&#8216; "Non-Receipt" of ITR V for AY XXXX-XX at Income Tax Department - CPC, Bangalore - your case - Reg Reminder&#8217;&#8217; from mail id &#8216;&#8216;intimations@cpc.gov.in&#8217;&#8217;.?</b><br><br><b>Ans. </b>It is a general reminder and not a notice, as it takes time for the Income Tax department to update there records. We request you to check after 30days on the link &#8216;&#8216;https://incometaxindiaefiling.gov.in/e-Filing/Services/ITRVStatusLink.html&#8217;&#8217;. If it still shows that it has not been received kindly drop a mail on support@filemyreturn.co.in. </li>
	<li><b>Intimation regarding &#8216;&#8216;Intimation U/S 143(1) for PAN AAAxxxxx6N AY:20XX-XX&#8217;&#8217; or &#8216;&#8216;Intimation U/S 245 for PAN AAxxxxx6N of Income Tax Act, 1961&#8217;&#8217; or &#8216;&#8216;Intimation U/S XXX of Income Tax Act, 1961&#8217;&#8217; from mail id &#8216;&#8216;intimations@cpc.gov.in&#8217;&#8217;?</b><br><br><b>Ans. </b>It is a notice and should not be ignored. These intimations are password protected pdf files. The password to open the file is your PAN No. in lower case followed by date of birth as per pan card in DDMMYYYY.  We request you to consult professional for the same. If you need our assistance(we provide services which are chargeable). You can contact:- 1.) CA Qimat Rai Garg - 9810518507, 2.) Amber Rai Garg - 9810483187.</li>
	</ol>
</li>
</ol>
<br>
<p style="margin:0px 60px;font-size:18px;"><b>NOTE:- You can contact us for any query on below contact details:-<br>
Email Id:- support@filemyreturn.co.in<br>
Phone No.:- 011-43046886, 9910011642<br>
</b>
</p>
</p>
</body>
</html>