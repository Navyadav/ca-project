<%@ taglib uri="http://www.springframework.org/tags/form" prefix="step"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%
	String path = request.getContextPath();
%>
<title>Discount</title>
<script src="<%=path%>/js/discount.js"></script>
<script src="<%=path%>/js/validate.js"></script>
<body>
          <div class="ticket feedback discount">
        	<table cellpadding="5" cellspacing="0" width="100%" style="border-bottom:1px solid #CCC;">
            	<tr>
                	<td><h2>Discount</h2></td>
                	
                    <td align="right"><button class="add-new button" style="background: url('<%=path %>/img/green_btn.jpg') repeat scroll 0 0 rgba(0, 0, 0, 0);color:#fff;"  onMouseOver="this.style.color='#000'" onMouseOut="this.style.color='#fff'">Add New</button></td>
                </tr>
            </table>
            	<h6 style="color: red;" id="error">${error}</h6>
            
            <table cellpadding="10" cellspacing="0" width="100%" style="font-family:Arial, Helvetica, sans-serif;">
            	<tr align="left" bgcolor="#666666" style="color:#fff">
                	<th>Domain</th>
                    <th>Company Name</th>
                    <th>Percentage </th>
                    <th colspan="3">Date</th>
                </tr>
                
                <c:forEach items="${discount}" var="datafeed">
                <tr>
                <td id="dname-${datafeed.id}">${datafeed.domainName}</td>
				<td id="cname-${datafeed.id}">${datafeed.companyName}</td>
				<td id="rate-${datafeed.id}">${datafeed.percentage}</td>
				<td><fmt:formatDate value="${datafeed.datetime}" pattern="MMM-dd-yyyy" /></td>
			
                    <td align="right"><a  href="deleteDiscount.htm?id=${datafeed.id}" class="btn_delete linkButton">Delete</a>
    <button class="btn_yellow edit" style="background: url('<%=path%>/images/btn_yellow_bg.jpg') repeat scroll center center rgba(0, 0, 0, 0);
    color: #000000;" onMouseOver="this.style.color='#666666'" onMouseOut="this.style.color='#000000'" id="${datafeed.id}">Edit</button></td>
                </tr>
                </c:forEach>               
               
            </table>
            
            
        </div>
      	<div id="dialog-form" title="Discount">
		<p class="validateTips" id="model-title"></p>
		<h6 style="color: red;" id="discounterror"></h6>
		<step:form modelAttribute="NewDiscount" action="addnew.htm" method="POST" onsubmit="return discountValidation();">
			<fieldset>
			<step:input type="hidden" path="id" value="" id="discountId"/>
				<label for="name">Domain Name</label>
				<step:input placeholder="example.com, example.co.in" type="text"  id="dname" onkeypress="return checkAlphaNumeric(event)"
					class="text ui-widget-content ui-corner-all" path="domainName" />
				<label for="name">Company Name</label>
				<step:input placeholder="company name" type="text"  id="cname" onkeypress="return checkAlphaNumeric(event)"
					class="text ui-widget-content ui-corner-all" path="companyName" />
					
				<label for="name">Rate</label>
				<step:input placeholder="domain discount" type="text" id="rate" onkeypress="return isNumber(event)"
					class="text ui-widget-content ui-corner-all" path="percentage" /><br>
				<input type="Submit" value="Add New Discount" id="submit" name="submit"/>
			</fieldset>
		</step:form>
	</div>
   
    <div class="clearfix"></div>    


</body>
</html>
