<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="step"%>
<%@ page
	import="java.io.*,java.util.*, com.craterzone.ca.ccavenue.security.AesCryptUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="user_email" value="${NewUserInformation.email }" />
<c:set var="amount" value="${amount }" />
<%@ include file="./libFunctions.jsp" %>
<%
	//String userEmail = pageContext.getAttribute("user_email").toString();

	String userEmail = pageContext.getAttribute("user_email")
			.toString();
	String Merchant_Id = "M_tan25765_25765";
	String Order_Id = getOrderId(userEmail);
	String Redirect_Url = "http://54.193.35.113:8080/CA/afterPayment.htm";
	String WorkingKey = "zc6j1eawfaawhov38pqbb3ft0r22yoww";
	String Amount = pageContext.getAttribute("amount").toString();
	System.out.print("Amount is "  + Amount);
	String Checksum = getChecksum(Merchant_Id, Order_Id, Amount,
			Redirect_Url, WorkingKey);//amount
	Enumeration enumeration = request.getParameterNames();
	String ccaRequest = "", pname = "", pvalue = "";
	while (enumeration.hasMoreElements()) {
		pname = "" + enumeration.nextElement();
		pvalue = request.getParameter(pname);
		ccaRequest = ccaRequest + pname + "=" + pvalue + "&";
	}
	ccaRequest = ccaRequest + "Checksum=" + Checksum;
	AesCryptUtil aesUtil = new AesCryptUtil(WorkingKey);
	String encRequest = aesUtil.encrypt(ccaRequest);
	String delivery_cust_name = "CraterZone";
	String delivery_cust_address = "Faridabad";
	String delivery_cust_country = "India";
	String delivery_cust_state = "Haryana";
	String delivery_cust_tel = "7503030472";
	String delivery_city = "Faridabad";
	String delivery_zip = "121005";
%>
<html>
<body>
<div style="height: 475px;vertical-align: middle;">
	<form action="http://www.ccavenue.com/shopzone/cc_details.jsp"
		method="post" name="online_form" id="online_form"
		novalidate="novalidate">
		<input type="hidden" name=Redirect_Url value="<%=Redirect_Url%>">
		<input type="hidden" name="Amount" value="<%=Amount%>"> 
		<input type="hidden" name="delivery_cust_name" value="<%=delivery_cust_name%>" /> 
		<input type="hidden" name="Merchant_Id" value="<%=Merchant_Id%>" />
		<input type="hidden" name="delivery_cust_address" value="<%=delivery_cust_address%>" />
		<input type="hidden" name="delivery_cust_country" value="<%=delivery_cust_country%>" /> 
		<input type="hidden" name="delivery_cust_state" value="<%=delivery_cust_state%>" />
		<input type="hidden" name="delivery_cust_tel" value="<%=delivery_cust_tel%>" />
		<input type="hidden" name="delivery_cust_city" value="<%=delivery_city%>" />
		<input type="hidden" name="delivery_zip_code" value="<%=delivery_zip%>" />
		<input type="hidden" name="Checksum" value="<%=Checksum%>" />
		<input type="hidden" name="Order_Id" value="<%=Order_Id%>" /> 
		
		<input type="hidden" name="billing_cust_email" value="<%=userEmail%>" />
		<input type="hidden" name="billing_cust_name" value="${NewUserInformation.fUserName } ${NewUserInformation.lUserName }" />
		<input type="hidden" name="billing_cust_city" value="${NewUserInformation.city }" />
		<input type="hidden" name="billing_cust_state" value=" ${NewUserInformation.state }" />
		<input type="hidden" name="billing_cust_country" value="India" />
		<input type="hidden" name="billing_cust_tel" value=" ${NewUserInformation.mobileNumber }" />
		<input type="hidden" name="billing_zip_code" value=" ${NewUserInformation.pinCode }" />
		<input type="hidden" name="billing_cust_address" value="${NewUserInformation.address1 }" />

<div style="width: 50%;" align="center">
		<table width="87%" border="0" cellspacing="0" cellpadding="0"
			class="options">
			<tr>
				<td colspan="3" height="5"></td>
			</tr>
			<tr>
				<td align="center" width="6%"><input name="paymentOption"
					type="radio" value="" checked="checked"></td>
				<td width="29%"><img
					src="<c:out value="${context}"/>/
                        <c:out value="${resUrl}"/>
                        /images/net-banking.png"
					alt="Netbanking" /></td>
				<td width="65%">Netbanking</td>
			</tr>
			<tr>
				<td colspan="3" height="5"></td>
			</tr>
			<tr>
				<td colspan="3" height="1" bgcolor="#A98E4E"></td>
			</tr>
			<tr>
				<td colspan="3" height="5"></td>
			</tr>
			<tr>
				<td align="center"><input name="paymentOption" type="radio"
					value=""></td>
				<td><img
					src="<c:out value="${context}"/>/
                        <c:out value="${resUrl}"/>
                        /images/dedit_card_visa.png"
					alt="Debit Cards" /></td>
				<td>Debit Cards</td>
			</tr>
			<tr>
				<td colspan="3" height="1" bgcolor="#A98E4E"></td>
			</tr>
			<tr>
				<td colspan="3" height="5"></td>
			</tr>
			<tr>
				<td align="center"><input name="paymentOption" type="radio"
					value=""></td>
				<td><img
					src="<%=request.getContextPath() %>/images/visamastercard.png"
					align="Visa / MasterCards Credit Cards" /></td>
				<td>Visa / MasterCards Credit Cards</td>
			</tr>
			<tr>
				<td colspan="3" height="3"></td>
			</tr>
		</table>
		<table width="80%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="10"></td>
			</tr>
			<tr>
				<td align="center"><input style="width: 130px;" type="submit" class="btn-signup"
					value="Pay Online"></td>
			</tr>
		</table>
		</div>
	</form>
	</div>
</body>
</html>