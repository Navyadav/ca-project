<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="step"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<!-- Set latest rendering mode for IE -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Client Detail</title>

<link href="css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />
<link href="css/style1.css" rel="stylesheet" type="text/css" />
<link href="css/slider_style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.flexisel.js"></script>
<script type="text/javascript" src="js/cycle.js"></script>
	<script>
        $('#s3').cycle({ 
        fx:     'fade', 
        speed:   0, 
        timeout: 1000, 
        next:   '#s3', 
        pause:   0, 
    });
    </script>


</head>
<body>
<body class="index">


    
    <div class="container">
    
        <div class="tag_line"><span class="green_text">S</span>imple & <span class="green_text">S</span>ecure way to e-file <span class="green_text">Returns</span> in <span class="green_text">India!</span></div>

        
        <!-- Testimonials Start -->
        <div class="testimonial">
        
        	<!-- Testimonial 1 -->
        	<c:forEach var="currentFeedback" items="${listOfFeedbacks}">
            <div class="testimonial_box">
            	<div class="test_img">
                    <div class="avtar_img">
                    <img src="${currentFeedback.imgUrl}" alt="Image" width="120" />
                    </div>
                    <div class="test_logo">
                        <img src="${currentFeedback.logoUrl}" alt="Logo" width="100" />
                    </div>
                </div>
                <div class="test_img_cont"><p>${currentFeedback.feedbackMessage}</p>
                <!-- <h1>Person Name</h1> -->
                </div>
            <div class="clearfix"></div>
            </div>
            <!-- /Testimonial 1 End-->
            
            
            <div class="divider"></div>
            </c:forEach>
        </div>
        <!-- Testimonials End -->
        
        
        <!-- About Section Start -->
        <div class="about_cont">
        
        		<div class="slider_bg">
                    <div id="s3" class="pics">
                        <img src="img/slider_img01.png"/>
                        <img src="img/slider_img02.png"/>
                        <img src="img/slider_img03.png"/>
                        <img src="img/slider_img04.png"/>
                        <img src="img/slider_img05.png"/>
                        <img src="img/slider_img06.png"/>
                        <img src="img/slider_img07.png"/>
                    </div>
                </div>
        
                <div class="offer_btn_bg">
                    <h1>Start Here!! <br/>Click to Upload</h1>
                    <a href="firststep.htm?step=simple"><div class="offer_btn">Salaried <br/>Rs. 200/-*</div></a>
                    <a href="firststep.htm?step=consult"><div class="offer_btn" style="margin-right:0;">Non Salaried Rs. 500/-*</div></a>
                <div class="clearfix"></div>
                </div>
        
        </div>
        <!-- About Section End -->
        
        
        <!-- Section Company Trust Start -->
        <div class="trut_comp_logo">
        <h1><span class="green_text">C</span>ompany <span class="green_text">T</span>rust <span class="green_text">U</span>s</h1>
        
        <ul id="flexiselDemo3">
            <li><img src="img/logo01.jpg" /></li>
            <li><img src="img/logo02.jpg" /></li>
            <li><img src="img/logo03.jpg" /></li>
            <li><img src="img/logo04.jpg" /></li>                                                 
        </ul>   
        </div>
        <!-- Section Company Trust End -->
   <br><br>
   <div class="clearfix"></div> 
	</div>
</body>
    <script>
        $("#flexiselDemo3").flexisel({
        visibleItems: 5,
        animationSpeed: 1000,
        autoPlay: true,
        autoPlaySpeed: 2000,            
        pauseOnHover: true,
        enableResponsiveBreakpoints: true,
        responsiveBreakpoints: { 
            portrait: { 
                changePoint:480,
                visibleItems: 1
            }, 
            landscape: { 
                changePoint:640,
                visibleItems: 2
            },
            tablet: { 
                changePoint:768,
                visibleItems: 3
            }
        }
    });
</script>

<script type="text/javascript" src="js/jquery.mCustomScrollbar.js"></script>
<script>
		(function($){
			$(window).load(function(){
				/* custom scrollbar fn call */
				$(".testimonial").mCustomScrollbar({
					scrollInertia:600,
					autoDraggerLength:false
				});
			});
		})(jQuery);
	</script>

</html>