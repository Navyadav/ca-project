<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="step"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<%
	String path=request.getContextPath(); 
%>
<!-- Set latest rendering mode for IE -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Client Detail</title>
</head>

<body>
    
    <!-- Form Start -->
    <section class="container">
    <step:form commandName="LoginUser" method="POST" action="doLogin.htm">
    	<div class="form">
        
            <table cellpadding="8" cellspacing="0" style="margin:20%;">
            	<tr>
                	<td colspan="2"><h2>Login</h2>${error}</td>
                </tr>
                <tr>
                	<td width="25%">Username<span class="required">*</span></td>
                    <td width="75%"><step:input path="username"/></td>
                </tr>
                <tr>
                	<td>Password<span class="required">*</span></td>
                    <td><step:password path="password"/></td>
                </tr>

                
                <tr>
                	<td></td>
                    <td><input name="" type="submit" value="Login"></td>
                </tr>
                
        	</table>
        </div>
    </step:form>
    
    </section>
    <!-- Form End -->
    
    

</body>
</html>