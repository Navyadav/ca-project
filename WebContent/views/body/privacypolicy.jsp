<%@ page language="java" contentType="text/html; charset=utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<!-- Set latest rendering mode for IE -->
<title>Privacy Policy</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
</head>
<body style="font-family: 'Times New Roman',Georgia,Serif; font-size: 18px;">
<h2>Privacy Policy</h2>
<h4>Privacy of Customer Data</h4>
<p>FMR is highly concerned in regards to the protection of the privacy of our customers.<br> Our privacy policy will make you feel comfortable and confident to share your personal information / documents in order to file your income tax returns with us.</p>
<h3>Scope</h3>
<p>This statement applies to the Filemyreturn.co.in Web site and services that display or link to this statement.</p>
<h3>Information Collection</h3>
<ul>
<li>When you avail our services, we ask you for information such as name, address, and telephone number that will help us confirming your payment details and in filing your Income Tax Return. Some of this information may be personal (information that can be uniquely identified with you, such as your full name, father&#8217;s name, address, date of birth, PAN, etc.).</li>
<li>Information is also gathered without you actively providing it, through the use of various technologies and methods such as Internet Protocol (IP) addresses and cookies. These methods do not collect or store personal information.</li>
<li>An IP address is a number assigned to your computer by your Internet Service Provider (ISP), so you can access the Internet. It is generally considered to be non-personally identifiable information, because in most cases an IP address is dynamic (changing each time you connect to the Internet), rather than static (unique to a particular user's computer).</li>
<li><b>We use your IP address to diagnose problems with our server, report aggregate information, and determine the fastest route for your computer to use in connecting to our site, and to administer and improve the site.</b></li>
<li><b>Cookies :</b>A cookie is small text files that websites put on your computer to store information about you and your preferences. On our website, we use cookies to track users' progress through the site, allowing us to make improvements based on usage data. The information in a cookie does not contain any personally identifiable information you submit to our site.</li>
<li>Once you close your browser, our access to the cookie terminates. You have the ability to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. If you choose not to accept the cookie, this will not affect your access to the majority of information available on our website. However, you will not be able to make full use of our online services.</li>
<li>In order to prepare your return, to file your return, to provide advice about tax matters, we collect information about your income, deductions, credits, dependents, etc. Put together, this information is known as tax return information.</li>
<li>When you visit our Web sites we may collect information about the pages you view, the links you select or actions you take using the standard information that your browser sends to each Web site you visit such as IP address, browser type, and the previously visited Web page.</li>
</ul>
<h3>Information Use</h3>
<ul>
<li>We do not sell or rent your personal or tax return information to anyone.</li>
<li>We do not share your personal or tax return information with anyone outside or inside of Filemyreturn  for their promotional or marketing use.</li>
<li>We do not share any information you provide via this site to any third parties or other govt departments except where:</li>
<ul>
<li>Such disclosures are necessary to fulfil our services obligations to you in which case we will require such third parties to agree to treat it in accordance with this Privacy Policy. </li>
<li>Required by applicable laws, court orders or government regulations.</li>
<li>You give us the permission to do so.</li>
</ul>
<li>Filemyreturn uses your personal information to contact you and to operate, improve, and deliver our services, including Web sites. We use your personal information to provide you with information and services you request. We use your personal information to charge you for the services you avail. We use the contact information you provide to communicate with you. We use your tax return information to prepare and file your return, and provide related assistance and services.</li>
<li>	We summarize information about your usage and combine it with that of others to learn about the use of Filemyreturn products to help us develop new products and services. This information is collected in such a way that it can not be used to identify an individual.</li>
<li>	We retain copies of your completed and filed tax returns in accordance with Filemyreturnâ€™s Records and Information Management Policy. This information may be used to provide you with a copy of your returns for your convenience.</li>
<li>	We may use service companies on our behalf to perform services for you but these companies are not allowed to use your information for their own purposes.</li>
<li>	We may access and/or disclose your information if it is necessary to comply with the law or legal process, to protect or defend Filemyreturn. For example, we may be required to cooperate with regulators or law enforcement action such as a court order, subpoena or search warrant.</li>
</ul>

<h3>Information Security</h3>
<p>Filemyreturn works to protect your personal information and tax return information from loss, misuse or unauthorized alteration by using industry-recognized security safeguards, coupled with carefully developed security procedures and practices. We maintain electronic and procedural safeguards of all tax return information. We use both internal and external resources to review our security procedures. Whenever we prompt you to transmit sensitive information, such as tax return or credit card information, we support the encryption of your information as it is transmitted to us. Our employees are trained and required to safeguard your information. We use technologies to safeguard your data and operate strict security standards to prevent any unauthorised access to it.</p>
<h5>Links to other websites</h5>
<p>This site contains links and references to other websites. Please be aware that this Privacy Policy does not apply to those websites and we encourage you to read the Privacy Policy of every website you visit.</p>
<h3>Changes to this Privacy Policy</h3>
<p>Please note that this Privacy Policy may change from time to time. We will post any Privacy Policy changes on this page and, if the changes are significant, we will provide a more prominent notice (including, for certain services, email notification of Privacy Policy changes).</p>
</body>
</html>