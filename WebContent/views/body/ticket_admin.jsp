<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="step"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html lang="en">
<head>
<meta charset="utf-8">
<%
	String path = request.getContextPath();
%>
<!-- Set latest rendering mode for IE -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Admin Tickets Registered</title>
<script src="<%=path%>/js/ticket.js"></script>
<style>
label,input {
	display: block;
}

input.text {
	margin-bottom: 12px;
	width: 95%;
	padding: .4em;
}

fieldset {
	padding: 0;
	border: 0;
	margin-top: 25px;
}

h1 {
	font-size: 1.2em;
	margin: .6em 0;
}
</style>
</head>

<body>
        <div class="ticket feedback discount">
        	<h2>Ticket Admin Staff</h2>
            	<h6 style="color: red;" id="error">${error}</h6>
            
            <br>
            <table cellpadding="10" cellspacing="0" width="100%" style="font-family:Arial, Helvetica, sans-serif;">
            
            	<tr align="left" bgcolor="#666666" style="color:#fff">
                	<th>Transaction ID </th>
                    <th>Date</th>
                    <th>PAN No. </th>
                    <th>Mobile</th>
                    <th>Email</th>
                    <th colspan="2">Remarks</th>
                </tr>
              <c:forEach items="${Tickets}" var="datafeed">
                <tr valign="top">
                	<td>${ datafeed.transactionId}</td>
                <td><fmt:formatDate value="${datafeed.dateTime}" pattern="MMM-dd-yyyy" /></td>
				<td>${datafeed.panNumber}</td>
				<td>${datafeed.mobile}</td>
				<td>${datafeed.email}</td>
				<td>${datafeed.remarks}</td>
                    <td><button class="create-user button" style="background: url('<%=path %>/img/green_btn.jpg') repeat scroll 0 0 rgba(0, 0, 0, 0);color:#fff;" onMouseOver="this.style.color='#000'" onMouseOut="this.style.color='#fff'" id="${datafeed.transactionId}-${datafeed.email}">Reply</button></td>
                </tr>
                </c:forEach>  
            </table>
                       
        </div>
   	<div id="dialog-form" title="Create new user">
		<p class="validateTips">Reply to Ticket</p>
		<h6 style="color: red;" id="replyerror">${error}</h6>
		<step:form modelAttribute="TicketsReply" action="ticketreply.htm" onsubmit="return replySubmit();">
			<fieldset>
				<label for="name">Transaction Id</label>
				<step:input type="text" readonly="true" id="id"
					class="text ui-widget-content ui-corner-all" path="transactionId" />
				<step:input type="hidden" path="email" id="email"
					class="text ui-widget-content ui-corner-all" />
				<label for="name">Remarks</label>
				<step:textarea path="remarks" class="text ui-widget-content ui-corner-all" id="remarks"></step:textarea>
				<input type="Submit" value="Reply" />
			</fieldset>
		</step:form>
	</div>
</body>
</html>
