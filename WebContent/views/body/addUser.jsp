<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html><html lang="en">
<head>
<meta charset="utf-8">
<%
	String path = request.getContextPath();
%>
<!-- Set latest rendering mode for IE -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Add User's</title>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script type="text/javascript">

function submitUser() {
	
	if($("#username").val()=="" || $("#username").val()==null) {
		$("#error").html("User name is Required");
		return false;
	}
	
	if($("#password").val()=="" || $("#password").val()==null) {
		$("#error").html("Password is Required");
		return false;
	}
	
	return true;
}

</script>
</head>

<body>
    	<div class="form">
    	<h4 style="color: red;" id="error">${error}</h4>
        	<form method="POST" action="submitUser.htm" onsubmit="return submitUser();" >
            <table cellpadding="8" cellspacing="0" style="margin:20%;">
            
            	<tr>
                	<td colspan="2"><h2>Add Staff</h2></td>
                </tr>
                <tr>
                	<td width="25%">Username<span class="required" >*</span></td>
                    <td width="75%"><input placeholder="username" name="username" type="text" id="username" onkeypress="return checkAlphaNumeric(event)"></td>
                </tr>
                <tr>
                	<td>Password<span class="required">*</span></td>
                    <td><input placeholder="password" name="password" type="password" id="password"></td>
                </tr>

                
                <tr>
                	<td></td>
                    <td><input type="submit" value="Add User"></td>
                </tr>
                
        	</table>
        	</form>
        </div>
    <div class="clearfix"></div>    
   

</body>
</html>
