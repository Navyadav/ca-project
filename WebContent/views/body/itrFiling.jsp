<%@ page language="java" contentType="text/html; charset=utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<!-- Set latest rendering mode for IE -->
<title>Guide to ITR Filing</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
</head>
<body  style="font-family: 'Times New Roman',Georgia,Serif; font-size: 16px;">
<h2>DOCUMENTS/INFORMATION REQUIRED FOR FILING</h2>
<p>
<ul>
<li>For Salaried Employees Form-16 (Part A & B) issued by ALL employers for the relevant Assessment Year.</li>
</ul>

<h3>Interest and Other Incomes</h3>
<ul>
<li>Total Interest Income on ALL savings accounts (Only figure).</li>
<li>Form 16A(Issued by Bank) on interest income from FD&#8217;s or provide 26AS Report.</li>
<li>Form 16A(Issued by Deductor against TDS deposit) on any other source of Income or provide 26AS Report.</li>
</ul>

<h3>Please upload the following if not considered in your Form 16</h3>
<ul>
<li>Section 80C - Investments made in LIC/ NSC/ PPF/ Tution Fees (Only Figures)</li>
<li>Section 80D - Receipt of Medical Policy (Self/spouse/children/dependant parents)</li>
<li>Section 80E - Education Loan Certificate </li>
<li>Section 80G -  receipt, name, PAN no. and address of Donee , Confirm exemption (50% /100%)</li>
<li>Home Loan Certificate.</li>
<li>Total amount of Capital Gain / Loss (Short Term or Long Term)</li>
</ul>
</p>

<h2 align="center">FILING WITH FMR</h2>
<p>
<h4>Step:-I (To be followed by You)</h4>
<ol>
<li>Fill client information page provided by us and upload the required documents.</li>
<li>Kindly save the receipt generated after paying the fee. This reference no. will be used to track the online status and for all further support.    </li>
</ol>

<h4>Step:-II (Followed by Us)</h4>
<ol>
<li>After downloading the documents, the same will be processed and checked at our back office. Then it will be uploaded on Income Tax site through software which is duly authorised by Income Tax department. NOTE:- All the ITR&#8217;s are uploaded without creating any User-id and password.</li>
</ol>

<h4>Step:-III (To be followed by Client)</h4>
<ol>
<li>After uploading of ITR online with Income Tax Department, an Acknowledgement copy i.e Income Tax Return “ITR-V” copy will come on your mail-id provided by you on client information page. The mail will come from donotreply@incometaxindiaefiling.gov.in and Subject as &#8216;&#8216;Confirmation message for Successful Efiling Return Reference: ITR Ack No: xxxxxxxxxxx for PAN XXXCH8029X of Assessment year 2013-2014&#8217;&#8217;. The password to open the file is your PAN No. in lower case followed by date of birth as per pan card in DDMMYYYY.<br><br>
<b>Note:- We request you to note that client who have uploaded the required documents/information related to filing of ITR before 7th July, 2014, will be receiving there Acknowledgement copy i.e Income Tax Return “ITR-V” within 15 days. And those who are submitting after 7th July,2014 will receive by 7th of August,2014. All the returns accepted till 30th  July,2014 will be filed by 31st July, 2014. (In case extension of date beyond 31st July, 2014 by Income Tax Authorities, the returns shall be duly filled within that extended time frame) </b>
</li><br>
<li>Kindly take two print outs of the same. Keep one copy in your record and the other needs to be signed and sent to the Income Tax Department –CPC Bangalore (Address:  
<br><b>NOTE</b>:- ITR-V is the full and final copy as it is now digitally signed and has BAR CODE so no STAMP OR SIGNATURES are there.
</li>
</ol>

<h4>Step:-IV (Followed by Income Tax Department)</h4>
<ol>
<li>After receiving the ITR-V (hard copy) CPC Bangalore, will be intimating through mail/sms sent by income tax department mail-id &#8216;&#8216;intimations@cpc.gov.in&#8217;&#8217;.<br> Note: - It is just a confirmation mail which may come or may not come. Still it can be cross checked from the link &#8216;&#8216;incometaxindiaefiling.gov.in&#8217;&#8217;. The final copy of ITR is ITR-V. </li>
</ol>
</p>
</body>
</html>