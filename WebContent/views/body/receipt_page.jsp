<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<!-- Set latest rendering mode for IE -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Client Detail</title>
<%
String path=request.getContextPath();
%>
<script>
function save() 
{ 
val = document.myform.text_area.value; 
mydoc = document.open(); 
mydoc.write(val); 
mydoc.execCommand("saveAs",true,".docx"); //you can change the .txt to your extention
history.go(-1);
}
</script>
</head>

<body>
    
    <!-- Form Start -->
    <section class="container">
    	<div class="form" style="min-height:500px;">
    	
        <c:if test="${OrderId!='' }">
        
            <table cellpadding="8" cellspacing="0">
            	<tr>
                	<td><h2>Success Condition</h2></td>
                </tr>
                <tr>
                	<td><h3>Payment success ${status}</h3></td>
                </tr>
                <tr>
                	<td>Your Receipt No. is</td>
                </tr>
                <tr>
                	<td><h3>${OrderId} </h3></td>
                </tr>
               
                <tr>
                	<td>Your return will be fill in a week and you contacted on Mobile No. / email if required.</td>
                </tr>
                <tr>
                	<td>You can laige  a ticket if you want any change in your details with 12 hours of Payment.</td>
                </tr>
                <tr>
                	<td><table cellpadding="5" cellspacing="0">
                    		<tr>
                            	<td><button onclick="window.print()">Print</button></td>
                                <td><button>Save</button></td>
                                <td><button onclick="save();">Copy</button></td>
                            </tr>
                    	</table>
                    </td>
                </tr>
                
        	</table>
            
            </c:if>
             <c:if test="${OrderId=='' }">
            <table cellpadding="8" cellspacing="0">
            	<tr>
                	<td><h2>Error </h2></td>
                </tr>
                <tr>
                	<td>Payment denied</td>
                </tr>
                <tr>
                	<td>${status}</td>
                </tr>
                <tr>
                	<td><table cellpadding="5" cellspacing="0">
                    		<tr>
                            	<td><button>Retry</button></td>
                                <td><button>Close</button></td>
                                <td><button onclick="window.location.href = 'ticket.htm'">Support</button></td>
                            </tr>
                    	</table>
                    </td>
                </tr>
                
        	</table>
            </c:if>
        </div>
    
    
    </section>
    <!-- Form End -->
    
    
    
   

</body>
</html>
