<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="admin_menu">
        	<ul>
                <li><a href="allFeedbacks.htm">Feedback</a></li>
                <li><a href="reportGenerate.htm">Reporting</a></li>
                <li><a href="alltickets.htm">Tickets </a></li>
                <li><a href="showdiscount.htm">Discounts </a></li>
                <c:if test="${username=='admin'}">
                	<li><a href="adminDashboard.htm">Admin Report</a></li>
                	<li><a href="addUser.htm">Add User</a></li>
                </c:if>
                <li><a href="logout.htm">Logout </a></li>
            </ul>
        </div>