<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="step"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<!-- Set latest rendering mode for IE -->
<%
	String path = request.getContextPath();
%>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Client Detail</title>
<script src="<%=path %>/js/jquery-1.9.1.js"></script>
<script src="<%=path %>/js/jquery-ui.js"></script> 
<script src="<%=path%>/js/validate.js"></script>
<link href="<%=path %>/css/style1.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<%=path %>/css/jquery-ui.css">
</head>
<body>
<tiles:insertAttribute name="header-content"></tiles:insertAttribute>
  <section class="container">
<tiles:insertAttribute name="menu-content"></tiles:insertAttribute>
<tiles:insertAttribute name="body-content"></tiles:insertAttribute>
 <div class="clearfix"></div> 
</section>
<tiles:insertAttribute name="footer-content"></tiles:insertAttribute>

</body>
</html>