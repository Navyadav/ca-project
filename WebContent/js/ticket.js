function replySubmit() {

	if ($("#remarks").val() == "" || $("#remarks").val() == null) {
		$("#replyerror").html("Remarks are Blank");
		return false;
	}
	return true;
}

function addTicket() {
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	
	if ($("#transid").val() == "" || $("#transid").val() == null || $("#transid").val()==0) {
		$("#error").html("Transaction id is blank or invalid");
		return false;
	}
	if ($("#pan").val() == "" || $("#pan").val() == null) {
		$("#error").html("Pan Number  is blank or invalid");
		return false;
	}
	if ((parseInt($("#mobile").val())) == 0 || $("#mobile").val() == null
			|| isNaN($("#mobile").val())) {
		$("#error").html("Mobile Number  is blank or invalid");
		return false;
	}
	if ($("#email").val() == "" || reg.test($("#email").val()) == false) {
		$("#error").html("Email is Invalid");
		return false;
	}
	if ($("#remarks").val() == "" || $("#remarks").val() == null) {
		$("#error").html("Remarks field is blank or invalid");
		return false;
	}
	return true;
}
$(function() {

	$("#dialog-form").dialog({
		autoOpen : false,
		height : 300,
		width : 450,
		modal : true,
		buttons : {}
	});

	$(".create-user").button().click(function() {
		var id = $(this).attr("id");
		var values = id.split('-');
		$("#id").val(values[0]);
		$("#email").val(values[1]);
		$("#replyerror").html("");
		$("#dialog-form").dialog("open");
	});

});


function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function checkAlphaNumeric(evt) {
	evt = (evt) ? evt : window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if ((charCode >= 48 && charCode <= 57) ||(charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122))
        	{
            return true;
        	}
        return false;
}