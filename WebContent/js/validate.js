function formSubmit() {
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    if ($("#fUserName").val() == "" || $("#fUserName").val() == null) {
        $("#error").html("User First Name is Required");
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
    if ($("#lUserName").val() == "" || $("#lUserName").val() == null) {
        $("#error").html("User Last Name is Required");
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
    if ($("#fFatherName").val() == "" || $("#fUserName").val() == null) {
        $("#error").html("Father First Name is Required");
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
    if ($("#lFatherName").val() == "" || $("#lUserName").val() == null) {
        $("#error").html("Father Last Name is Required");
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
    /*if ($("#panNumber").val() == "" || $("#panNumber").val() == null) {
        $("#error").html("Pan Card Number is Required");
        return false;
    }*/
    if (parseInt($("#mobileNumber").val()) == 0
            || isNaN(parseInt($("#mobileNumber").val()))) {
        $("#error").html("Mobile Number is Invalid");
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
    if ($("#email").val() == "" || reg.test($("#email").val()) == false) {
        $("#error").html("Email is Invalid");
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
    if ($("#address1").val() == "" || $("#address1").val() == null) {
        $("#error").html("Address is Invalid");
        return false;
    }
    if ($("#city").val() == "" || $("#city").val() == null) {
        $("#error").html("City is Invalid");
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
    if ($("#state").val() == "" || $("#state").val() == null) {
        $("#error").html("State is Invalid");
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
    if (parseInt($("#pinCode").val()) == 0
            || isNaN(parseInt($("#pinCode").val()))) {
        $("#error").html("Pin Code is Invalid");
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
    if (parseInt($("#accountNumber").val()) == 0
            || isNaN(parseInt($("#accountNumber").val()))) {
        $("#error").html("Account Number is Invalid");
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }
    if ($("#ifsc").val() == "" || $("#ifsc").val() == null) {
    	 
    	if ($("#bankName").val() == "" || $("#bankName").val() == null) {
    	        $("#error").html("Bank Name is Blank");
    	        $('html, body').animate({ scrollTop: 0 }, 'slow');
    	        return false;
    	    }
    	   if ($("#bankAddress").val() == "" || $("#bankAddress").val() == null) {
    	        $("#error").html("Bank Address is Blank");
    	        $('html, body').animate({ scrollTop: 0 }, 'slow');
    	        return false;
    	    }
    }
  
    return true;
}

function onSubmitSecond() {
    var iserror = false;
    if ($("#assessmentYear").val() == "" || $("#assessmentYear").val() == null) {
        $("#error").html("Assessement Year is invalid");
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    }

    //var ragx=/^(([a-zA-Z0-9_\.]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.doc|.docx|.DOC|.DOCX.txt|.TXT)$/;
   var ragx= /(\.DOC|\.DOCX|\.doc|\.docx|\.pdf|\.TXT|\.txt)$/;
    $('.file_browse')
            .each(
                    function(index, val) {
                        if (index == 0 ) {
                            if (!ragx.test($(this).val())) {
                                $("#error")
                                        .html(
                                                "File Part A must not be Blank or Invalid .doc, .docx, .txt and .pdf supported");

                                $('html, body').animate({ scrollTop: 0 }, 'slow');
                                iserror = true;
                                return false;
                            }
                        } else {
                            if ($(this).val() != ""
                                    && !ragx.test($(this).val())) {
                                $("#error")
                                        .html(
                                                "File Type Error .Doc,.Docx, .txt, .pdf supported");
                                $('html, body').animate({ scrollTop: 0 }, 'slow');
                                iserror = true;
                                return false;
                            }
                        }

                    });

    
   
    	
    	if(isNaN(parseInt($("#income").val()))) {
    		  $("#error").html("Income is not valid");
    	        $('html, body').animate({ scrollTop: 0 }, 'slow');
    		  iserror=true;
    		  return false;
    	}
    	if(isNaN(parseInt( $("#bankInterest").val())) || parseInt( $("#bankInterest").val())>100) {
  		  $("#error").html("Bank Interest is not valid");
          $('html, body').animate({ scrollTop: 0 }, 'slow');
  		  iserror=true;
  		  return false;
    	}
    	 if (!iserror) {
        
        if (!$('#checkbox1').is(":checked")) {
            $("#error").html("Accept Terms and Conditions");
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            return false;
        }
        if (!$('#checkbox2').is(":checked")) {
            $("#error").html("Confirmition of Terms and Conditions");
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            return false;
        }
    } else {
        return false;
    }
    return true;
}


function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function onlyAlphabets(e, t) {
            try {
                if (window.event) {
                    var charCode = window.event.keyCode;
                }
                else if (e) {
                    var charCode = e.which;
                }
                else { return true; }
                if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode==08))
                    return true;
                else
                    return false;
            }
            catch (err) {
                alert(err.Description);
            }
}

	function checkAlphaNumeric(evt) {
		evt = (evt) ? evt : window.event;
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if ((charCode >= 48 && charCode <= 57) ||(charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122))
            	{
                return true;
            	}
            return false;
}
	
	function onlyAlphabetsWithSpace(e, t) {
        try {
            if (window.event) {
                var charCode = window.event.keyCode;
            }
            else if (e) {
                var charCode = e.which;
            }
            else { return true; }
            if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode==08)|| (charCode==32))
                return true;
            else
                return false;
        }
        catch (err) {
            alert(err.Description);
        }
}