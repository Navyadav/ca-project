
function discountValidation() {
	
	if($("#dname").val()==null || $("#dname").val()=="") {
		$("#discounterror").html("Domain Name can not Blank");
		return false;
	}
	if($("#cname").val()==null || $("#cname").val()=="") {
		$("#discounterror").html("Company Name can not Blank");
		return false;
	}
	if($("#rate").val()==null || $("#rate").val()=="" || isNaN($("#rate").val()) || $("#rate").val()<=0) {
		$("#discounterror").html("Discount Rate can not Blank");
		return false;
	}
	return true;
}
$(function() {

	$("#dialog-form").dialog({
		autoOpen : false,
		height : 300,
		width : 450,
		modal : true,
		buttons : {}
	});

	$(".add-new").button().click(function() {
		
		$("#dialog-form").dialog("open");
		$("#discounterror").html("");
		$("#model-title").html("Add New Discount");
		$("#dname").val("");
		$("#cname").val("");
		$("#rate").val("");
		$("#submit").val("Add New Discount");
	});
$(".edit").button().click(function() {
		var id=$(this).attr("id");
		
		$("#discountId").val(id);
		$("#dialog-form").dialog("open");
		$("#discounterror").html("");
		$("#model-title").html("Update Discount");
		$("#dname").val($("#dname-"+id).html());
		$("#cname").val($("#cname-"+id).html());
		$("#rate").val($("#rate-"+id).html());
		$("#submit").val("Update");
	});

});